1. Users
   1. A superuser exists.
   2. Users can login.  A dashboard page exists for logged in users.
   3. The superuser can create new users.
   4. Users receive an email when they are added to the system.
   5. The superuser can find users.
   6. The superuser can delete users.
   7. Users can change their password.
   8. The superuser can change a user's name, email and password.
   9. Users can reset their password.
   10. A profile page exists for every user.
2. Courses
   1. The superuser can create a course.
   2. The superuser can edit a course's name.
   3. The superuser can see a list with all courses.
   4. The superuser can delete courses.
   5. The superuser can add students and teachers to a course.
   6. A course page exists for every course.
   7. The superuser can remove students and teachers from a course.
   8. Teachers can add students to their course.
   9. Teachers can remove students from their course.
   10. Students and teachers can see a list of courses they are in on their profile page.
3. Assignments
   1. Teachers can create new assignments.
   2. Teachers can edit assignments.
   3. Teachers can delete assignments.
   4. Teachers and students can see assignments.
   5. An assignment page exists for every assignment.
   6. When an assignment is updated, all submissions are pushed to the start of the pipeline.
4. Submissions
   1. Students can post submissions to assignments.
   2. Teachers can see an overview of all the latest submissions done by their students.
   3. Teachers can browse all the submissions a student made in a course.
   4. Students can see their own submissions.
   5. The status of a student's last submission is visible on the assignment page.
   6. Students and teachers can view individual submissions.
   7. Students and teachers can download the source code of individual submissions.
5. Testing
   1. Submissions can be reserved by a pipeline node.
   2. The preparation stage works for Python submissions.
   3. The preparation stage works for GCC submissions.
   4. Tests can be ran on a prepared Python submission.
   5. Tests can be ran on a prepared GCC submission.
   6. Students are notified when all example tests have been completed.
   7. The pipeline is correctly finished. (Data inserted to database and clean up)
   8. Teachers can see the results of the final tests in an overview on the assignment page.
\documentclass[11pt, a4paper]{article}
\usepackage{minted}
\usepackage{parskip}
\usepackage[utf8]{inputenc}
\usepackage[none]{hyphenat}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{biblatex}
\usepackage[hidelinks]{hyperref}
\frenchspacing
\setlength\parindent{0pt}
\addbibresource{bibliography.bib}

\title{Code Submission System: Setup Guide}
\author{Jeroen Bollen\\ 1642848\and Mih\'{a}ly Csonka\\ 1644219 \and Joey Wilmots\\ 1644850}
\date{}

\begin{document}
\maketitle
\newpage

\tableofcontents
\newpage

\section{Introduction}
This is the setup guide for the Code Submission System application. It covers the server-side setup and a short getting started section for the superuser. Section \ref{section:advanced} covers several advanced topics such as a Docker status dashboard and how to backup the database.

This application is targeted to run on Debian "Stretch" Linux x86\_64/amd64. As such, some steps might differ slightly on different host systems.

The system was tested to run on a Debian server environment, with 1 GiB of RAM and 10 GiB of storage space.

\section{Prerequisites}
A bit of preparation is required on host machine before the application can be deployed.

\subsection{XFS file system}
The Code submission System requires the host system to run on a XFS file system with the \texttt{pquota} flag set. Add the \texttt{rootflags=pquota} option to the kernel parameters to enable project quotas on the root file system.

On Debian this is done by opening \texttt{/etc/default/grub} and adding \\\texttt{rootflags=pquota} to the \texttt{GRUB\_CMDLINE\_LINUX\_DEFAULT} option. The line might end up looking as follows:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
GRUB_CMDLINE_LINUX_DEFAULT="quiet rootflags=pquota"
\end{minted}

After editing the configuration file, update the bootloader by running:
\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo cp /boot/grub/grub.cfg /boot/grub/grub.cfg.bk
sudo grub-mkconfig -o /boot/grub/grub.cfg
\end{minted}

Now restart the system. You can verify the change worked by running \\\texttt{mount | grep xfs}, and looking for the \texttt{prjquota} option. The output might look as follows:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
/dev/sda1 on / type xfs (rw,relatime,attr2,inode64,prjquota)
\end{minted}

\clearpage
\subsection{Docker}
Docker is the main dependency of the application. This section explains how to install Docker, check whether the install succeeded, and initiate Docker swarm mode. This section is heavily based on the original Docker installation guide for Debian \cite{docker_for_debian}.

\subsubsection{Installation}
Update the \texttt{apt} package index:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo apt-get update
\end{minted}

Now, install packages to allow \texttt{apt} to use repositories over HTTPS:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common
\end{minted}

Then, add Docker's official GPG key:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
curl -fsSL https://download.docker.com/linux/debian/gpg | \
    sudo apt-key add -
\end{minted}

And add the Docker repository to \texttt{apt}:
\begin{minted}[xleftmargin=15pt,linenos]{bash}
    sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"
\end{minted}

Lastly, install Docker and enable the Docker service:
\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo apt-get install docker-ce
sudo systemctl enable docker
\end{minted}

To ensure Docker has been successfully installed, run the following command:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo docker run hello-world | grep Hello
\end{minted}

Expected output is as follows:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
Hello from Docker!
\end{minted}

\section{Deployment}
Following sections explain how to deploy the application.

\subsection{Docker swarm: the master}\label{section:swarm_master}
Docker swarm facilitates scalability. It allows other machines to join the master host machine in managing the load of the application. Only 1 master host is allowed to exist. Initializing Docker swarm for the master host is easy. Run:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo docker swarm init
\end{minted}

\subsection{deploy.sh}\label{section:deploy}
If you want to use multiple nodes, this step must be performed after \ref{section:swarm_slave}.

This script is to be used on the machine which manages the Docker swarm. It takes your settings and starts the application with these settings applied. To get information about all possible options of the script run:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo ./deploy.sh -h
\end{minted}

The application is deployed once the \texttt{deploy.sh} script, either with \texttt{-i} or \texttt{-t}, finishes running. The web server can be visited through the external IP address of the machine it is running on.

\subsubsection{Interactive mode}
Interactive mode is recommended for first-time deployment. Simply run following command and follow the instructions to provide the application with your settings:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo ./deploy.sh -i
\end{minted}

This mode also creates an \texttt{interactive\_config.json} file with the settings you provided. Store it safely as it contains sensitive data! This file is useful for future re-deployments.

\subsubsection{File mode}
For easier re-deployment you can use file mode. This allows you to provide a \texttt{.json} file with all settings needed to deploy the application. File mode can be initiated using the \texttt{-f} flag.

\subsubsection{SSL}
Secure web servers require SSL protection. The SSL key and certificate can be provided during deployment using the the \texttt{-k} and \texttt{-c} flags respectively.

\subsection{Docker swarm: the slaves}\label{section:swarm_slave}
This section explains how to build the application on a different machine, and how to join the swarm as a slave. This step must be performed before the swarm master is deployed. This is not a requirement for the application to function! It can be skipped if you so wish.

The slave machine has to follow all steps except Sections \ref{section:swarm_master} and \ref{section:deploy}.

\subsubsection{build.sh}
This script builds the application but does not deploy it. Its options are similar to the \texttt{deploy.sh} script. Run the script with the same config files as you did \texttt{deploy.sh}.

Did you use interactive mode with the \texttt{deploy.sh} script? It generated an \texttt{interactive\_config.json} file. Use this file as \texttt{build.sh}'s config file.

\subsubsection{Joining the swarm}
On the master host machine, execute the command:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo docker swarm join-token worker
\end{minted}

Docker will respond with a command similar to this:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
docker swarm join --token LONG-TOKEN-HERE IP-ADDRESS:PORT
\end{minted}

Now simply execute the given command on the slave machine. The slave will successfully join the swarm if the token was correct.

\section{Getting started}\label{section:getting_started}
A superuser account is shipped with the application. They have the right to manage the whole system. The default username of the superuser is \texttt{admin@admin}. It is recommended to change this password immediately on the profile page. The password is \texttt{admin}. The admin panel, also known as super panel, can be accessed from the dashboard after logging in.

\clearpage
\section{Advanced}\label{section:advanced}
Following section discusses advanced topics such as database interaction and Docker management. These are quality of life topics and are not needed for the application to function.

\subsection{Docker swarm dashboard with Portainer}
Portainer is a dashboard tool for Docker. It allows you to get an overview of all the things Docker is doing on your machine. As well as manage all Docker images, containers, networks, etc.

To install Portainer run:

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo docker service create \
    --name portainer \
    --publish 9000:9000 \
    --replicas=1 \
    --constraint 'node.role == manager' \
    --mount type=bind,src=//var/run/docker.sock,dst=/run/docker.sock \
    --mount type=volume,src=portainer_data,dst=/data portainer/portainer \
    -H unix://run/docker.sock
\end{minted}

You can access Portainer on \texttt{localhost:9000}. The first time you access it you will be asked to create an admin account. This account is only for Portainer and only for the machine you are currently on.

\subsection{connect\_psql.sh}
This script facilitates connecting to the database directly. To use it you first need to install a dependency.

\begin{minted}[xleftmargin=15pt,linenos]{bash}
sudo apt-get update
sudo apt-get install postgresql-client
\end{minted}

Now simply run \texttt{connect\_psql.sh} and interactively provide the database credentials you chose during deployment.

\subsection{dump\_db.sh}
This script facilitates taking a back-up of the database. It works exactly the same as \texttt{connect\_psql.sh}. Install the dependency if you have not done so. Then execute the script and interactively provide your credentials.

\subsection{load\_db\_backup.sh}
If you make backups you should be able to load them back up again. This can be done using the \texttt{load\_db\_backup.sh} script.

Simply execute the script and interactively choose which backup you wish to load. Do not forget to re-deploy the application after loading the backup, as executing this script will halt the application.

\clearpage
\section{Adding Platforms}
New platforms must be added to both the testserver and the web server. Adding a platform to the web server allows submissions to be posted for that platform, and adding it to the test server allows those submissions to be tested.

\subsection{Adding a platform to the testserver}

To add a platform to the testserver, edit the testserver configuration. This can be found in \texttt{testserver/config.json}. The platform must be added to the \texttt{platforms} array. Every entry has several configuration options. All of them are mandatory.

\begin{itemize}
    \item \textbf{\texttt{id}}: The unique identifier of this platform. This can be any integer, as long as it's unique.
    \item \textbf{\texttt{prepare\_module}}: The directory of the runner that will handle the prepare stage.
    \item \textbf{\texttt{test\_module}}: The directory of the runner that will handle the example testing and final testing stages.
    \item \textbf{\texttt{prepare\_max\_memory}}: The amount of bytes of RAM the prepare stage can use.
    \item \textbf{\texttt{test\_max\_memory}}: The amount of bytes of RAM the example testing and final testing stages can use.
    \item \textbf{\texttt{working\_directory\_size\_limit}}: The maximum amount of bytes the runner can occupy on the file system.
\end{itemize}

Often when adding a new platform, it's also neccesary to add new runners to the testserver. Runners are docker containers code is prepared and tested in. They are added by creating a new directory in \texttt{testserver/runners/}. The name of the directory can only contain alphanumerical characters, as well as dashes (-).

There are two types of runners, and the type of a runner is defined by how it's used in the testserver configuration. If it appears as a \texttt{prepare\_module} it is a prepare runner, and if it appears as a \texttt{test\_module} it is a test runner. While a runner can both be a prepare and a test runner, by appearing as both, this is unlikely to prove useful.

Runners are automatically built when the testserver starts. When they are changed, the testserver needs to be rebuilt and restarted. The easiest way to do this is to redeploy the system as explained in \ref{section:swarm_master} and \ref{section:swarm_slave}.

\subsubsection{Prepare Runners}\label{section:prepare_runners}

A prepare runner is used to prepare submisions for testing. They are run once, as part of the preparation stage. Typically, they compile code.

Prepare runners receive the submitted submission as a file \\\texttt{/submission/submission}. When they are done, the insides \texttt{/submission/} will be copied to all test runners on the same submission. This is where for example compiled code must go. The testing stage is skipped if the prepare runner returns an exit code other than 0.

An example of a prepare runner for a GCC/GNU Make submission is provided below. It extracts a tar archive, and runs \texttt{make test}. Finally, it checks if the test file was indeed created, and if not, returns 1.

The Dockerfile is saved as \texttt{testserver/runners/testserver-prepare-gcc\\-tar/Dockerfile}. As every runner is a Docker container, every runner needs a Dockerfile.
\begin{minted}[xleftmargin=15pt,linenos]{text}
FROM ubuntu:16.04

RUN apt-get update
RUN apt-get install -y build-essential
RUN apt-get clean

COPY compile.sh .
CMD ./compile.sh
\end{minted}

\clearpage
We set our entrypoint in the Dockerfile to \texttt{compile.sh}, so we must also provide this file as \texttt{testserver/runners/testserver-prepare-gcc-tar/\\compile.sh}.
\begin{minted}[xleftmargin=15pt,linenos]{bash}
#!/bin/sh

# If one of the steps fail, propagate the error by stopping the script.
# e.g. No point in doing `make test` if we could not extract the archive.
set -e

cd /submission/

tar --no-same-owner -xvf submission
make test

if [ ! -f "test" ]
then
    echo "Running GNU Make did not create the file 'test'."
    exit 1
fi
\end{minted}

The standard output and standard error of the prepare runner is interpreted as the standard output and the standard error of the prepare stage. In this case, we add additional content to the standard output if the \texttt{test} file was not properly generated.

\subsubsection{Test Runners}

A test runner is used to prepare every test. The test runner does not test all tests at once, but is started up once per test. The \texttt{/submission/} directory of every test runner is equal to that same directory in the prepare runner of that submission after it finished.

A test runner receives the test input over the standard input stream, and the standard output and the standard error of the runner is interpreted as the standard output and standard error of the submission. This means, if the standard output of the test runner matches the expected output of the test, the test passes. Otherwise, it fails.

As an example, we'll provide a test runner for a GCC/GNU Make platform, compatible with the example runner defined in \ref{section:prepare_runners}.

\clearpage
The Dockerfile is saved as \texttt{testserver/runners/testserver-test-bin/Dockerfile}. As every runner is a Docker container, every runner needs a Dockerfile.
\begin{minted}[xleftmargin=15pt,linenos]{text}
FROM ubuntu:16.04

COPY run.sh .
ENTRYPOINT ./run.sh
\end{minted}

We defined \texttt{./run.sh} as our entrypoint, so we need to provide this file as \texttt{testserver/runners/testserver-test-bin/run.sh}.
\begin{minted}[xleftmargin=15pt,linenos]{bash}
#!/bin/sh

cd /submission/

./test <&0 1>&1 2>&2

return_code=$?
echo "Script stopped with return code $return_code." 1>&2
exit $return_code
\end{minted}

The run script simply runs the \texttt{test} binary previously generated by the prepare runner, with the standard input stream of the runner forwarded into the new process. Additionally, if the process stopped with a non-zero exit code, the exit code of the process is written in a helpful message to the standard error. It returns with the same exit code as the process.

\subsection{Adding a platform to the web server}

To add a platform to the web server, login to the web interface as a superuser. How to do this, is explained in section \ref{section:getting_started}. Navigate to the super panel, and click on 'Add new platform'.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{new_platform}
    \caption{The new platform page}
\end{figure}

Provide the platform unique identifier, title and a description. The unique identifier must match the unique identifier previously added to the testserver config. The description supports markdown.

Press "Save new platform" when you're done.

\clearpage
\section{Bibliography}\label{sec:bibliography}
\printbibliography[heading=none]

\end{document}
\documentclass[11pt, a4paper]{article}
    \usepackage{parskip}
    \usepackage{graphicx}
    \usepackage{tablefootnote}
    \usepackage[none]{hyphenat}
    \usepackage{biblatex}
    \usepackage{fontspec}
    \usepackage{listings}
    \usepackage{minted}
    \usepackage{xcolor}
    \usepackage[hidelinks]{hyperref}
    \synctex=1
    \setmainfont{DejaVu Sans}
    \emergencystretch=1em
    \setcounter{biburllcpenalty}{9000}
    \setcounter{biburlucpenalty}{9000}
    \sloppy
    \widowpenalties 1 10000
    \raggedbottom

    \makeatletter
    \newcommand*{\centerfloat}{%
      \parindent \z@
      \leftskip \z@ \@plus 1fil \@minus \textwidth
      \rightskip\leftskip
      \parfillskip \z@skip}
    \makeatother

    \addbibresource{analysis/bibliography.bib}
    \frenchspacing

    \def\arraystretch{1.25}

    \title{PSOPV: Analysis}
    \author{Jeroen Bollen\\ 1642848\and Mihály Csonka\\ 1644219 \and Joey Wilmots\\ 1644850}
    \date{March 15 2018}

\begin{document}

\begin{titlepage}
    \newcommand{\HRule}{\rule{\linewidth}{0.5mm}}
    \center
    \textsc{\LARGE University Hasselt}\\[1.3cm]
    \begin{figure}[H]
        \centering
        \includegraphics[scale=0.8]{./analysis/uhasselt_logo.png}\\[1.0cm]
    \end{figure}
    \textsc{\Large Project software ontwikkeling en professionele vaardigheden}\\[0.3cm]
    \textsc{\large Analysis report}\\[0.3cm]
    \HRule \\[0.4cm]
    { \huge \bfseries Code submission system}\\[0.4cm]
    \HRule \\[1.5cm]
    \begin{minipage}{0.5\textwidth}
        \begin{flushleft}
            \emph{Authors:}\\
            Jeroen Bollen \makebox[2.5cm]{1642848}\\
            Mihály Csonka \makebox[2cm]{1644219}\\
            Joey Wilmots \makebox[2.6cm]{1644850}\\
        \end{flushleft}
    \end{minipage}
    ~
    \begin{minipage}{0.4\textwidth}
        \begin{flushright}
            \emph{Client:} \\
            Prof. dr. Liesenborgs Jori\\
            \vspace{1em}
            \emph{Supervisor:} \\
            Drs. Hellings Jelle
        \end{flushright}
    \end{minipage}\\[4cm]
    {\large March 15, 2018}\\[3cm]
    \clearpage
\end{titlepage}
\newpage

\tableofcontents
\newpage

\clearpage
\section{Introduction}
In this report we introduce a new code submission system, present mockups of the planned web interface to said system, and iterate over the technical requirements. Further, we look at other, similar software to draw inspiration and potentially improve on them. We discuss the architecture of the planned system and finally we list all milestones with their respective work items.

\clearpage
\section{High-level overview}\label{sec:overview}
The goal of this project is to develop a code submission system which allows students to submit code for an assignment, which will then automatically be evaluated. The system runs on Debian Linux.

The system manages a set of users and courses. Users can be either unrelated to a course, a student of a course, or a teacher of a course.

Every course has a set of assignments created by the teachers, which the students have to complete.

We will present the system in more detail in the following subsections.

\subsection{Administration}

Every user has a name, email address, and password. Users relate to courses: they can either be unrelated to a course, a student of a course, or a teacher of a course.

The first user on the platform is the superuser. The superuser exists by default. They have the privilege of managing the entire system. This includes creating, modifying, and deleting users and courses. There can only be one superuser and their privileges are not transferable.

When the superuser creates a new user, the system will send an email to this user that contains a temporary link to set their password. The new user will not be able to login until they set a password. A new link can be requested by using the `forgot password' feature.

\clearpage

\subsection{Courses}
A course has a name, a set of students, a set of teachers, and a set of assignments.

Teachers can add and remove students, as well as create, edit, and delete assignments. The students can submit solutions to the assignments.

A superuser can do anything a teacher would be able to do.

\subsection{Assignments}\label{sec:assignments}

Every assignment has following properties:
\begin{itemize}
    \item title, description, and deadline;
    \item example tests;
    \item final tests;
    \item maximum compilation time and running time;
    \item platform\footnote{The platform describes the environment the code will run on. An example is 'GCC C++ on Ubuntu 16.04 LTS'.}.
\end{itemize}
All these properties are visible to the students, except for the final tests.

Students are able to submit solutions to assignments as long as the deadline hasn't been met. All submissions will be logged for future reference.

The only platforms included with this project are:
\begin{itemize}
    \item GCC C/C++ Ubuntu 16.04 LTS;
    \item Python 3 on Ubuntu 16.04 LTS.
\end{itemize}

\clearpage

\subsection{Testing}

Once a solution to an assignment has been submitted, it will go through the following pipeline as soon as possible:
\begin{enumerate}
    \item The solution will be prepared for execution.
    \item The solution will be tested against the example tests.
    \item The results of step 1 and 2 will be sent to the respective student by email.
    \item The solution will now be tested against the final tests.
\end{enumerate}

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{analysis/pipeline.pdf}
    \caption{The testing pipeline.}
    \label{fig:pipeline}
\end{figure}

The pipeline only notifies the user of the results of the first two steps. This is useful as documentation for the user. However, as requested by the client, it is the responsibility of the teachers to notify their students of the results of their final tests. The platform will not do this.

When an assignment has been modified by a teacher or a superuser, all solutions will be reset to the start of the pipeline. This ensures the results of the pipeline match the updated assignment.

\subsubsection{GCC C / C++}
All submissions must be in the tar format. The tar format makes sure file permissions are set correctly on both Windows and Linux machines.

During preparation the tarball will be extracted. GNU Make will be run in the root directory of the tarball.

\begin{minted}[xleftmargin=16pt,linenos]{bash}
    #!/bin/bash
    tar -xf submission.tar
    make solution
    \end{minted}

During testing the generated solution file will be run for every test. The test's input will be supplied through standard input, and output is expected through standard output.

\subsubsection{Python}
All submissions must be in the tar format. The tar format makes sure file permissions are set correctly on both Windows and Linux machines.

During preparation the tarball will be extracted.

For each test, Python 3 will be run on the file 'main.py'. The test's input will be supplied through standard input, and output is expected through standard output.

\subsection{Security}
The project is divided into modules. Each module will be run in a Docker container. These containers create an isolated environment in which code can be run without affecting critical systems like the database.

Passwords are hashed using a strong hashing algorithm.

A password reset is done through email. This is the standard method used by websites. Attention is paid to the implementation details to ensure maximum security.

\clearpage

\section{Mockups}

\subsection{Dashboard}

The dashboard, as seen in Figure \ref{fig:dashboard}, is full of shortcuts for the most common actions a user can take. Teachers are able to create new assignments. Students can view all their upcoming assignments and recent submissions. The superuser can access the super panel through this page. This is the first page users land on after logging in.

\subsection{Assignment Page}

The assignment page is different for teachers and students.

Teachers see, as shown in Figure \ref{fig:assignment_teacher}, a summary of the assignment, all latest submissions of the students, and all example and final tests.

Students see a summary of the assignment, their recent submissions, and example tests. This is shown in Figure \ref{fig:assignment_student}.

Figure \ref{fig:new_assignment} shows how teachers can create or edit an assignment. Tests can be added or deleted one by one.

\subsection{Course Page}

The course page, shown in Figure \ref{fig:course}, shows a course's info. Listed are all assignments of a course, and all the teachers and students assigned to a course.

Teachers have the ability to create new assignments, as well as manage the students of the course. The superuser is able to manage teacher and delete the course.

\subsection{Profile Page}

A profile page is shown in Figure \ref{fig:profile}. Every user has a profile page displaying the user's email address and a list of all courses the user is assigned to. A square academic cap is shown if the user is a student in the course. A necktie shows that the user is a teacher of the course.

Users are able to change their password and log out on the profile page.

Superusers are able to delete users via their profile page.

\subsection{Submission Page}

On the submission page users can view all the details of a submission. Figure \ref{fig:submission_pending} and Figure \ref{fig:submission_done} show the difference between a submission which is pending and a submission which is fully evaluated.

Students can only view submission details of their own submissions. Teachers can view submission details of all submissions on their courses.

This page also allows the user to download the source code for the submission.

The page shown in Figure \ref{fig:submission_hist} displays the submission history of a student to a specific assignment.

\subsection{Superuser Pages}

On the panel, shown in Figure \ref{fig:super_panel}, the superuser is able to add new users, find users, and download a list of all users. As well as navigating to, and creating new courses.

Users can be easily added in bulk using comma-separated values on the page shown in Figure \ref{fig:super_add}. Every user has a name and email address separated by a comma.

The superuser is able to search for other users based on name and email on the page shown in Figure \ref{fig:super_find}.

The superuser is responsible for creating all courses on the system. This can be seen in Figure \ref{fig:super_create}.

\clearpage

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/dashboard.png}
    \caption{The dashboard contains shortcuts for the most common actions.}
    \label{fig:dashboard}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/assignment_teacher.png}
    \caption{The assignment page allows teachers to manage the assignment and see their students' submissions.}
    \label{fig:assignment_teacher}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/assignment.png}
    \caption{Students see this page when viewing an assignment.}
    \label{fig:assignment_student}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/new_assignment.png}
    \caption{Teachers see the page above when creating or editing assignments.}
    \label{fig:new_assignment}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/course.png}
    \caption{The course page shows all information of a course.}
    \label{fig:course}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/profile.png}
    \caption{Users can view and manage their account info on their profile page.}
    \label{fig:profile}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/submission_pending.png}
    \caption{The submission page of a pending submission is quite bland.}
    \label{fig:submission_pending}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/submission_done.png}
    \caption{Once a submission has been evaluated, more detailed information is shown to the user.}
    \label{fig:submission_done}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/submission_history.png}
    \caption{The submission history page displays all submissions of a student on an assignment.}
    \label{fig:submission_hist}
\end{figure}


\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/super_panel.png}
    \caption{The superuser panel page allows the superuser to manage the system.}
    \label{fig:super_panel}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/super_add_users.png}
    \caption{The user add page allows the superuser to add new users in bulk.}
    \label{fig:super_add}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/super_find_users.png}
    \caption{Finding users is done on the find user page.}
    \label{fig:super_find}
\end{figure}

\begin{figure}[H]
    \centerfloat
    \includegraphics[width=\textwidth]{analysis/mockups/png/super_create_course.png}
    \caption{On the create course page the superuser is able to create new courses.}
    \label{fig:super_create}
\end{figure}

\clearpage

\section{Technical requirements}\label{sec:requirements}

In this section we will present every requirement of our project in detail.

\subsection{General}

\begin{itemize}
    \item The system runs on Debian.
\end{itemize}

\subsection{Accounts}

\begin{itemize}
    \item Users have a name, an email address, and a password.
    \item There is a superuser.
    \item The superuser is shipped with the application.
    \item The superuser can create new user accounts.
    \item The superuser can edit the properties of existing user accounts.
    \item The superuser can remove existing user accounts, except their own.
    \item New users receive an email to set their initial password. The account cannot be logged in with unless a password is set.
    \item Users are able to login using their email and their password.
    \item Users can change their own password by performing following steps.
          \begin{enumerate}
              \item Users click on a password reset link and supply their email address.
              \item A reset password link is sent to the email address.
              \item By following this link, the user can change their password.
          \end{enumerate}
    \clearpage\item Passwords are hashed.
    \item Password reset links are valid for a limited amount of time.
    \item Password reset links are immediately invalidated upon clicking them.
    \item Resetting a password logs the user out.
\end{itemize}

\subsection{Courses}

\begin{itemize}
    \item A course has a name, a set of teachers, a set of students, and a set of assignments.
    \item Teachers can edit the course name.
    \item Teachers can add students to the courses they teach.
    \item Teachers can remove students from the courses they teach.
    \item The superuser can do anything to a course a teacher would be able to do.
    \item The superuser can create new courses.
    \item The superuser can remove existing courses.
    \item The superuser can edit the properties of existing courses.
    \item The superuser can add and remove teachers from a course.
\end{itemize}

\subsection{Assignments}

\begin{itemize}
    \item Teachers can add new assignments to the courses they teach.
    \item Teachers can remove existing assignments from the courses they teach.
    \item Teachers can modify existing assignments in the courses they teach.
    \item Every assignment has following properties:
          \begin{itemize}
              \item title, description, and deadline;
              \item example tests and final tests;
              \item maximum compilation time and running time;
              \item language and platform.
          \end{itemize}
    \item Students can submit solutions to assignments in courses they are students of. The format depends on the assignment's presets.
    \item All submitted solutions are kept in the database.
    \item Assignments close after the deadline has expired. No new submissions are allowed.
    \item Teachers can always access all submitted source files of an assignment.
\end{itemize}

\subsection{Testing}

\begin{itemize}
    \item A test has an input and an output.
    \item A test succeeds when a submission, given the test inputs, generates the test outputs.
    \item All submissions go through the testing pipeline. See Figure \ref{fig:pipeline}.
          \begin{enumerate}
              \item Preparation: the submission is compiled and prepared for testing.
              \item Example testing: the prepared submission is tested against the example tests.
              \item Notification: the student is notified of the outcome of the preparation stage and example testing stage.
              \item Final testing: the prepared submission is tested against the final tests.
          \end{enumerate}
    \item Teachers can see the results of the pipeline for every submission to an assignment.
    \item Students can see the results of the preparation and example testing stage of the pipeline.
    \item The pipeline can test Python 3 scripts on Ubuntu 16.04.
    \item The pipeline can test Linux C programs on Ubuntu 16.04.
    \item The pipeline can test Linux C++ programs on Ubuntu 16.04.
    \item When an assignment is modified, every submission is pushed back to the start of the pipeline.
\end{itemize}

\clearpage
\section{Other software}\label{sec:software}
The code submission system we set to develop is not the first of its kind. Many alternatives exist, some of which are even open source. When presenting the alternatives, we are particularly interested in security.

\subsection{DOMJudge}
DOMJudge is a code submission system software package for coding competitions. Competitors can submit code, which similar to our case, will be tested against example inputs and against final inputs. It supports a wide variety of programming languages including Python, C, Java, and Rust.

Security in DOMJudge is achieved using Linux features e.g. cgroups to limit resource usage and chroot to limit file access. The official manual argues that merely warning people of the consequences of cheating the system might be sufficient to prevent it from happening. \cite{dom-judge}

While mere trust might be enough for a programming competition, it will likely not be an adequate security strategy for a code submission system that is targeted at students. While as previously mentioned DOMJudge does use chroot to try and isolate the code from the rest of the system, it should also be noted that using chroot as a security feature is generally advised against as there are many ways to break out of it. \cite{chroot-safety}

DOMJudge uses the UNIX 'diff' utility to test whether a submission is correct. If the diff utility does not report any differences between the test output and the submission output, the submission output is considered correct.

\subsection{Rust Playground}
The Rust Playground\footnote{https://play.rust-lang.org/} is a web application that allows you to experiment with Rust code without having to manually install the compiler. \cite{rust-playground}

While the system does not have any way of testing or grading submissions, it does allow people to submit code that will then be executed.

The Playground uses Docker for security. Docker makes sure submitted code does not have an internet connection and cannot use excessive amounts of memory. The 'timeout' utility is used to make sure the code does not run longer than is allowed.

\clearpage
\section{Project architecture}\label{sec:architecture}

We divide the application into different modules. The modules will be run and managed by Docker. The reason for choosing Docker is quite simple:

\begin{itemize}
    \item Every module can be run as an isolated component. One module failing does not directly impact another.
    \item It is more lightweight than using a Virtual Machine.
    \item Docker applications can be easily deployed.
    \item The assignment specifically mentioned Docker.
\end{itemize}

\subsection{Modules in Detail}
The application will be split up in different modules. The different modules are presented in Figure \ref{fig:containers}.

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{analysis/containers.pdf}
    \caption{Communication between the modules. Some modules, such as the web server and pipeline managers, are scalable i.e. they can be run multiple times to divide the workload. Every runner has at least one pipeline manager. The web servers and the pipeline managers communicate with the database. The runners communicate with their pipeline manager.}
    \label{fig:containers}
\end{figure}

The web server is what the students and the teachers will use to interact with the system. The pipeline managers manage the pipeline which every submission goes through. The runner allows code, submitted by the students, to be run in isolation. The database is responsible for storing all state.

Because all persistent state is stored in the database, all the other modules can be scaled up. Furthermore, when any other module crashes, it can simply restart without any data loss. Backups can be limited to only the database.

Communication between the modules will be done using user-defined Docker bridge networks. Containers can only communicate when connected to a common bridge network. Communication from within containers to the 'outside world' is only supported when explicitly defined. This setup ensures that malicious code will be unable to easily spread throughout the system.

\subsubsection{Database}

The database will be responsible for storing the persistent state of the application. It has to contain all the users, courses, assignments, submissions, and test results.

The three database software packages we have considered are PostgreSQL, SQLite, and Redis.\footnote{There are many other database tools available, but of course we could not possibly discuss all of them. We took into consideration these three because they have key differences. } A quick overview of the different features can be found in Table \ref{tab:databases}.

\begin{table}[h]
    \centering
    \begin{tabular}{l|ccc}
        Database              & Redis             & PostgreSQL & SQLite                                                                                                                               \\
        \hline
        Referential Integrity & ✗ \cite{redis-ri} & ✓          & ✓                                                                                                                                    \\
        Simultaneous Access   & ✓                 & ✓          & ✗\tablefootnote{While multiple clients can read at a time, when one client writes, all other clients have to wait. \cite{sqlitefaq}} \\
    \end{tabular}
    \caption {Comparison of databases considered.}
    \label{tab:databases}
\end{table}

The first requirement our database must meet is that it must support referential integrity. The data we will be storing is of a relational nature. For instance courses have a set of students, a set of teachers, and a set of assignments. A database with referential integrity ensures that these relations are always valid.

Our database must also support simultaneous access from multiple clients. We will have at least one web server and one pipeline manager. These servers will have to access the database simultaneously.

Taking these two requirements into consideration, it becomes clear that PostgreSQL is the best tool. It supports both referential integrity and access by multiple clients at once.

As Docker automatically removes any files written by a container, we will use Docker volumes to make sure data is stored even if the container is stopped or crashes. \cite{docker-volumes}

\paragraph{ER Diagram}

The entity-relation diagram in Figure \ref{fig:ER_diagram} shows an overview of the database design. It shows how every component relates to each other. We discuss the more complex elements below.

Normalized names are equal to the entry's default name stripped of all special characters. This is useful for reverse lookups of the entry when getting the name from an URL.

The submission table stores all data and the state of a submission. Notable are the last\_update and pipeline\_epoch columns. The last\_update column shows the last moment in which the submission was known to be worked on. This column will periodically be updated while the submission is going through the testing pipeline. However, in case no update is received for too long the pipeline will be considered faulty. The submission will be reset to the default state, last\_update will be set to NULL, and the pipeline\_epoch will be incremented.  The pipeline\_epoch is like a version number. When the pipeline\_epoch doesn't match the epoch used at the start, the submission update will be rejected.

\clearpage
\enlargethispage{10cm}
\thispagestyle{empty}
\addtolength{\topmargin}{-1.1in}
\begin{figure}[H]
    \centerfloat
    \includegraphics[height=1.2\textheight, width=1.1\textwidth]{analysis/er_diagram.pdf}
    \caption{ER diagram of the database.}
    \label{fig:ER_diagram}
\end{figure}
\clearpage
\addtolength{\topmargin}{1.1in}

\subsubsection{Web App}

The web module is the part of the project the user interacts with. It allows them to log in, submit assignments, and see their results.

The two main competitors are PHP and Node.js. Both have a big community and a lot of frameworks available. However, PHP is harder to set up as it also requires a back end web server. Node.js gets you started immediately, only requiring the software itself to be installed.

As previously mentioned, Node.js has a wide variety of frameworks available. A basic overview is provided in Table \ref{tab:webframeworks}. All three frameworks support the needed features. After experimentation, Express was the most comfortable to work with. Koa is a very minimalist framework, while Sails is very expansive. Express provides a good middle ground.

\begin{table}[h]
    \centering
    \begin{tabular}{l|ccc}
        Framework  & Express                   & Koa                   & Sails                   \\
        \hline
        Middleware & ✓                         & ✓                     & ✓                       \\
        PostgreSQL & ✓ \cite{express-pgsql}    & ✓ \cite{koa-pgsql}    & ✓ \cite{sails-pgsql}    \\
        Templating & ✓ \cite{express-template} & ✓ \cite{koa-template} & ✓ \cite{sails-template} \\
    \end{tabular}
    \caption {Comparison of web frameworks considered.}
    \label{tab:webframeworks}
\end{table}

\subsubsection{Pipeline Manager}
The responsibility of the pipeline manager is pushing an available submission through the testing pipeline, as well as cleaning test entries in the database that exceeded a given time limit.

The life cycle of the pipeline manager is as follows:\\

\begin{minted}[xleftmargin=16pt,linenos]{bash}
    while (true)
        if (garbage_data_found())
            clean_up();

        if (submission = needs_testing())
            database.mark_started(submission);
            result = start_pipeline(submission);
            database.push(result);
            database.mark_done(submission);
    \end{minted}

The pipeline manager will spawn a different container for the prepare stage, example testing stage, and the final testing stage. Each of these containers will return their result to the pipeline manager as shown in Figure \ref{fig:seq_uml}. This workflow has been inspired by the Rust Playground which does the same thing.

Internally the pipeline manager consists of several modules as seen in Figure \ref{fig:uml}:
\begin{itemize}
    \item WorkManager: queries the database for new submissions which have to be tested.
    \item Submission: an abstraction layer for database queries which gets submission data of a specific submission.
    \item Pipeline: this module manages the different stages of the pipeline itself.
    \item Runner: prepares and runs, given settings and data, a Docker container in which a submission is compiled or run.
    \item Test: a data structure used to efficiently manage testing in the pipeline.
\end{itemize}
\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{analysis/uml/uml.png}
    \caption{Minimalistic overview of the pipeline manager's components and the major functions these contain.}
    \label{fig:uml}
\end{figure}

\clearpage
\enlargethispage{10cm}
\thispagestyle{empty}
\addtolength{\topmargin}{-1.1in}
\begin{figure}[H]
    \centerfloat
    \includegraphics[height=1.2\textheight, width=1.4\textwidth]{analysis/UML.pdf}
    \caption{Sequence diagram of the pipeline.}
    \label{fig:seq_uml}
\end{figure}
\clearpage
\addtolength{\topmargin}{1.1in}

\paragraph{Language}

We considered several languages such as Java, Node.js, and C++ to write the pipeline manager in. Node.js was chosen to avoid complications, such as requiring different libraries and learning a different API, when switching between front end and back end development.

\paragraph{Database Interaction}

We chose pg-promise to interact between the pipeline manager and the database, as this is the same library we use for the web application.

\paragraph{Email Notification}

Following options to notify users via email were considered:
\begin{itemize}
    \item Adonis Mail
    \item smpt-mail
    \item Nodemailer
\end{itemize}

Adonis provides integration with platforms like Amazon. This is unneeded for our project.

smpt-mail offers a clean and simple abstraction of Nodemailer. However, the severe lack of documentation compared to Nodemailer is an issue.

In the end we decided to use Nodemailer.

\clearpage

\section{Backlog}
A list of all tasks of the project, what component they are a part of, their priority and their assignee is provided as Figure \ref{fig:backlog}.

\section{Milestones and work items}\label{sec:milestones}
A list of all work items for the project, what component they are a part of, their priority, and their assignee is provided as Figure \ref{fig:backlog}. Additionally, every week before a presentation, the team will come together and prepare for this presentation. These are not programming related tasks and are not included as work items.

There are several milestones in the project.

\paragraph{2018-04-01} All work items with a priority less than 20 should be completed. At this point a superuser will be able to create users and courses. Teachers will be able to create assignments, and students will be able to create submissions.

\paragraph{2018-04-20} All work items with a priority less than 100 should be completed. These mark the critical parts of the application. Submissions will be tested in the pipeline. This also marks the start of the interim presentations.

\paragraph{2018-05-16} All work items should be completed. The application should be finished. No more programming should be required at this point.

\paragraph{2018-05-31} A final report and a presentation should be prepared. This marks the end of the project.

\clearpage
\enlargethispage{10cm}
\thispagestyle{empty}
\addtolength{\topmargin}{-1.1in}
\begin{figure}[H]
    \centerfloat
    \includegraphics[height=1.2\textheight, width=1.2\textwidth]{analysis/backlog.pdf}
    \caption{Backlog containing all work items which have to be completed. Work items are sorted by priority.}
    \label{fig:backlog}
\end{figure}

\clearpage
\addtolength{\topmargin}{1.1in}

\clearpage
\section{Bibliography}\label{sec:bibliography}
\printbibliography[heading=none]

\end{document}
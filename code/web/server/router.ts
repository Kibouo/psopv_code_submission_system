import { User } from "./user";
import { Course } from "./course";
import { Assignment } from "./assignment";
import { Submission } from "./submission";

class PathSegment {
    name: string;
    path: string;
    is_real: boolean;

    constructor(name: string, path: string, is_real: boolean) {
        this.name = name;
        this.path = path;
        this.is_real = is_real;
    }
}

/**
 * A path is a collection of URL segments.
 */
export class Path {
    private segments: PathSegment[];

    constructor(segments: PathSegment[] = []) {
        this.segments = segments;
    }

    /**
     * Creates a new path based on this one, but with one extra segment at the end. This does not
     * modify the path it was called on.
     *
     * @param segment The segment to append to the newly created path.
     */
    append(segment: PathSegment): Path {
        const new_segments = this.segments;
        new_segments.push(segment);
        return new Path(new_segments);
    }

    /**
     * Creates a new `PathSegment` and appends it.
     *
     * @param name The name of the new path segment.
     * @param path The path of the new path segment.
     * @param is_real Whether this path leads to anywhere.
     */
    append_new(name: string, path: string, is_real: boolean) {
        return this.append(new PathSegment(name, path, is_real));
    }

    /**
     * Returns the URL this path represents.
     */
    get url(): string {
        const relative_url = this.segments
            .map(({ path, name }) => path)
            .filter(path => path.length > 0)
            .join("/");

        return `${root()}${relative_url}`;
    }

    /**
     * Gets the name of the page this path leads to.
     */
    get name(): string {
        const last_segment = this.last_segment;
        if (last_segment !== null)
            return last_segment.name;
        else
            return "";
    }

    /**
     * Gets whether this path leads to somewhere.
     */
    get is_real(): boolean {
        const last_segment = this.last_segment;
        if (last_segment !== null)
            return last_segment.is_real;
        else
            return false;
    }

    /**
     * The amount of segments in this path.
     */
    get length(): number {
        return this.segments.length;
    }

    /**
     * Returns an iterator over all subpaths of this path.
     */
    *subpaths(): Iterable<Path> {
        for (let index = 1; index <= this.segments.length; index += 1)
            yield this.subpath(index);
    }

    /**
     * Gets the first `amount` segments as a new path.
     * @param amount The amount of prefix-segments to include.
     */
    private subpath(amount: number): Path {
        return new Path(this.segments.slice(0, amount));
    }

    private get last_segment(): PathSegment | null {
        if (this.segments.length > 0)
            return this.segments[this.segments.length - 1];
        else
            return null;
    }
}

/**
 * This namespace contains path-generators that generate instances of `Path` to various pages.
 */
export namespace path {
    export function dashboard(): Path {
        return new Path().append_new("Dashboard", "dashboard", true);
    }

    export function login(): Path {
        return new Path().append_new("Login", "", true);
    }

    export function profile_page(target_user: User): Path {
        return new Path()
            .append_new("Profile Page", "profile_page", false)
            .append_new(target_user.name, `${target_user.name_normalized}-${target_user.id}`, true);
    }

    export function password_reset(): Path {
        return new Path().append_new("Password Reset", "password_reset", true);
    }

    export function password_reset_request(): Path {
        return new Path().append_new("Password Reset Request", "password_reset_request", true);
    }

    export function course(course: Course): Path {
        return new Path()
            .append_new("Courses", "courses", false)
            .append_new(course.name, `${course.name_normalized}-${course.id}`, true);
    }

    export function edit_course(course: Course): Path {
        return path.course(course).append_new("Edit", "edit", true);
    }

    export function new_assignment(course: Course): Path {
        return path.course(course).append_new("Create Assignment", "create_assignment", true);
    }

    export function edit_assignment(course: Course, assignment: Assignment): Path {
        return path.assignment(course, assignment).append_new("Edit Assignment", "edit_assignment", true);
    }

    export function assignment(course: Course, assignment: Assignment): Path {
        console.assert(course.id === assignment.course_id);

        return path.course(course)
            .append_new("Assignments", "assignments", false)
            .append_new(assignment.name, `${assignment.name_normalized}-${assignment.id}`, true);
    }

    export function add_submission(course: Course, assignment: Assignment) {
        return path.assignment(course, assignment)
            .append_new("New Submission", "new_submission", true);
    }

    export function submission_details(
        course: Course, assignment: Assignment, submission: Submission
    ) {
        return path.assignment(course, assignment)
            .append_new("Submissions", "submissions", false)
            .append_new(`${submission.id}`, `${submission.id}`, true);
    }

    export function delete_submission(
        course: Course, assignment: Assignment, submission: Submission
    ) {
        return path.submission_details(course, assignment, submission)
            .append_new("Delete", "delete", true);
    }

    export function submission_history(course: Course, assignment: Assignment, user: User) {
        return path.assignment(course, assignment)
            .append_new("Submissions", "submissions", false)
            .append_new("History", "history", false)
            .append_new(user.name, `${user.name_normalized}-${user.id}`, true);
    }

    export function super_panel(): Path {
        return new Path().append_new("Super Panel", "super", true);
    }

    export function super_add_users(): Path {
        return path.super_panel().append_new("Add Users", "add_users", true);
    }

    export function super_delete_users(): Path {
        return path.super_panel().append_new("Delete Users", "delete_users", true);
    }

    export function super_add_course(): Path {
        return path.super_panel().append_new("Add Course", "add_course", true);
    }

    export function super_all_courses(): Path {
        return path.super_panel().append_new("All Courses", "all_courses", true);
    }

    export function super_find_users(): Path {
        return path.super_panel().append_new("Find Users", "find_users", true);
    }

    export function platform_list(): Path {
        return new Path().append_new("Platforms", "platforms", true);
    }

    export function super_add_platform(): Path {
        return path.super_panel().append_new("Add Platform", "add_platform", true);
    }

    export function manage_user(course: Course, user_type: string): Path {
        return path.course(course).append_new("Manage " + user_type + "s", "manage_" + user_type + "s", true);
    }

    export function platform(platform: any): Path {
        return path.platform_list().append_new(platform.name, `${platform.id}`, true);
    }

    export function platform_edit(platform: any): Path {
        return path.platform(platform).append_new("Edit", "edit", true);
    }

    export function platform_delete(platform: any): Path {
        return path.platform(platform).append_new("Delete", "delete", true);
    }
}

/**
 * Returns the root-path of this installation.
 *
 * @returns {string}
 * The root-path.
 */
function root(): string { return "/"; }

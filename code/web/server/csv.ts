import Result from "./result";

export enum CsvError {
    EmptyRow, EmptyColumn, BadColumnCount
}

export function csv_error_to_string(error: CsvError): string {
    switch (error) {
        case CsvError.BadColumnCount:
            return "expected 2 columns";
        case CsvError.EmptyColumn:
            return "empty column";
        case CsvError.EmptyRow:
            return "empty row";
        default:
            throw new Error("Unknown CSV error");
    }
}

export function parse_csv(csv_string: string, column_names: string[]): Result<any, CsvError>[] {
    return csv_string
        .split("\n")
        .map(row => row
            .split(",")
            .map(field => field.trim()))
        .map(row => {
            if (row.length === 1 && row[0] === "")
                return Result.new_failure(CsvError.EmptyRow)
            else if (row.length !== column_names.length)
                return Result.new_failure(CsvError.BadColumnCount);
            else if (row.filter(field => field.length === 0).length > 0)
                return Result.new_failure(CsvError.EmptyColumn);
            else {
                let parsed_csv: any = {};
                for (let i = 0; i < column_names.length; i++) {
                    let column = column_names[i];
                    parsed_csv[column] = row[i];
                }
                return Result.new_success(parsed_csv);
            }
        });
}

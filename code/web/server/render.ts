import { Response } from "express";
import { User } from "./user";
import * as utils from "./utils.js";
import * as router from "./router";
import { Path, path } from "./router";
import { Course } from "./course";
import { Assignment } from "./assignment";
import { Submission, pipeline_status_to_string } from "./submission";

export class UserView {
    readonly name: string;
    readonly email: string;
    readonly profile_path: Path;

    constructor(user: User) {
        this.name = user.name;
        this.email = user.email;
        this.profile_path = path.profile_page(user);
    }
}

export class SimpleAssignmentView {
    readonly name: string;
    readonly overview_path: Path;
    readonly example_test_count: number;
    readonly final_test_count: number;
    readonly deadline: Date;

    private constructor(
        assignment: Assignment, course: Course,
        example_test_count: number, final_test_count: number
    ) {
        this.name = assignment.name;
        this.overview_path = path.assignment(course, assignment);
        this.example_test_count = example_test_count;
        this.final_test_count = final_test_count;
        this.deadline = assignment.deadline;
    }

    static async from_assignment(assignment: Assignment): Promise<SimpleAssignmentView> {
        return new SimpleAssignmentView(
            assignment, await assignment.course(),
            await assignment.fetch_example_test_count(), await assignment.fetch_final_test_count()
        );
    }
}

export class SubmissionStatusView {
    readonly student: UserView;
    readonly assignment: SimpleAssignmentView;
    readonly submission: SubmissionView | null;
    readonly history_path: Path;

    constructor(
        student: UserView,
        assignment: SimpleAssignmentView,
        submission: SubmissionView | null,
        history_path: Path
    ) {
        this.student = student;
        this.assignment = assignment;
        this.submission = submission;
        this.history_path = history_path;
    }

    static async from_user_assignment_submission(
        student: User,
        assignment: Assignment,
        submission: Submission | null
    ): Promise<SubmissionStatusView> {
        if (submission) {
            console.assert(submission.user_id === student.id, "Student ids did not match.");
            console.assert(
                submission.assignment_id === assignment.id,
                "Assignment ids did not match."
            );
        }

        return new SubmissionStatusView(
            new UserView(student),
            await SimpleAssignmentView.from_assignment(assignment),
            submission ? await SubmissionView.from_submission(submission) : null,
            path.submission_history(await assignment.course(), assignment, student)
        );
    }
}

export class SubmissionView {
    readonly details_path: Path;
    readonly pipeline_status: string;
    readonly is_prepare_success: boolean;
    readonly successful_example_test_count: number;
    readonly successful_final_test_count: number;

    private constructor(
        submission: Submission,
        details_path: Path,
        successful_example_test_count: number,
        successful_final_test_count: number
    ) {
        this.details_path = details_path;
        this.pipeline_status = pipeline_status_to_string(submission.pipeline_status);
        this.is_prepare_success = submission.is_prepare_success;
        this.successful_example_test_count = successful_example_test_count;
        this.successful_final_test_count = successful_final_test_count;
    }

    /**
     * Creates a renderable submission from a submission.
     * @param submission The submission to prepare for rendering.
     */
    static async from_submission(submission: Submission): Promise<SubmissionView> {
        const assignment_promise = submission.assignment();

        const [
            assignment, course,
            successful_example_test_count, successful_final_test_count
        ] = await Promise.all([
            assignment_promise,
            assignment_promise.then(assignment => assignment.course()),
            submission.fetch_successful_example_test_count(),
            submission.fetch_successful_final_test_count()
        ]);

        return new SubmissionView(
            submission, path.submission_details(course, assignment, submission),
            successful_example_test_count, successful_final_test_count
        );
    }

    /**
     * Creates a list of renderable submissions from a list of submissions. Order is preserved.
     * @param submissions A list of submissions to prepare for rendering.
     */
    static async all_from_submissions(submissions: Submission[]): Promise<SubmissionView[]> {
        return Promise.all(submissions.map(SubmissionView.from_submission));
    }
}

export function render_message(
    response: Response,
    user: User,
    title: string,
    content: string,
    path: router.Path
): void {
    response.render("message.pug", {
        header: { title },
        navigation: { page_stack: path },
        message: { title, content },
        utils: utils,
        router: router,
        user: user,
    });
}

export function render_message_with_redirect(
    response: Response,
    user: User,
    title: string,
    content: string,
    path: router.Path,
    redirect_to_message: string,
    redirect_to_url: string
): void {
    response.render("message.pug", {
        header: { title },
        navigation: { page_stack: path },
        message: {
            title,
            content,
            redirect: {
                url: redirect_to_url,
                message: redirect_to_message
            }
        },
        utils: utils,
        router: router,
        user: user
    });
}

/**
 * Renders the a validation page.
 */
export function render_validation_page(
    response: Response, 
    user: User,
    title: string,
    content: string,
    path: router.Path,
    validated_data: any,
    submit_message: string
) {
    return response.render("validate.pug", {
        header: {
            title: "Validate Course"
        },
        navigation: {
            page_stack: path,
        },
        page_title: title,
        message: content,
        validated_data: validated_data,
        submit_text: submit_message,
        utils: utils,
        router: router,
        user: user
    });
}
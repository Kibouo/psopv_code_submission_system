const UTILS = require("./utils");

module.exports = {
    /**
     * Gets all the platforms of an assignment.
     *
     * @param {pg-promise:Database} db The database to get the data from.
     * @param {Number} assignment_id The id of the assignment from which the
     * related platforms are being requested.
     * @return {Promise<Platform[]>} A promise resolving to an array
     * of platforms related to the requested assignment.
     */
    from_assignment_id(db, assignment_id) {
        return db.manyOrNone(
            `SELECT platform.id, platform.name
            FROM platform
                INNER JOIN assignment_platform_relation
                ON platform.id = assignment_platform_relation.platform_id
            WHERE assignment_platform_relation.assignment_id = $1
            ORDER BY name ASC`,
            [assignment_id]
        )
            .then(rows => rows.map(row => platform_from_row(db, row)));
    },

    /**
     * Gets the IDs and names of all existing platforms,
     * in alphabetic order based on the names.
     * The order comes in handy when displaying a list of the platforms
     *
     * @param {pg-promise:Database} db The database to get the data from.
     * @return {Promise<{id: number, name: string}[]>} A promise resolving to
     * an array of all platform IDs and names. Alphabetically ordered by name.
     */
    all_platforms_ordered: function (db) {
        return db.any("SELECT id, name FROM platform ORDER BY name ASC")
            .then(rows => rows.map(row => platform_from_row(db, row)));
    },

    /**
     * Retrieves a Platform from the database.
     *
     * @param {pg-promise:Database} db
     * The database to get the data from.
     *
     * @param {Number} id
     * The id of the platform to pull out of the database.
     *
     * @returns {extern:Promise.<Platform|null>}
     * A Platform, or null if the id does not match any platform.
     */
    from_id(db, id) {
        return db.oneOrNone("SELECT id, name FROM platform WHERE id = $1", [id])
            .then(row => row ? platform_from_row(db, row) : null);
    },

    /**
     * Retrieves a Platform from the database.
     *
     * @param {pg-promise:Database} db
     * The database to get the data from.
     *
     * @param {string} name
     * The name of the platform to pull out of the database.
     *
     * @returns {extern:Promise.<Platform|null>}
     * A Platform, or null if the name does not match any platform.
     */
    from_name(db, name) {
        return db.oneOrNone("SELECT id, name FROM platform WHERE name = $1", [name])
            .then(row => row ? platform_from_row(db, row) : null);
    },

    /**
     * Creates a new platform, pushing it to the database and returning a promise to it.
     *
     * @param {pg-promise:Database} db
     * A database handle.
     *
     * @param {number} id
     * The id of the platform to add. Must not conflict with already existing ids.
     *
     * @param {string} name
     * The name of the platform to add. Must not conflict with already existing names.
     *
     * @param {string} description
     * The description of the platform to add.
     *
     * @returns {exernal:Promise.<Platform>}
     * A promise resolving to a handle to the platform that was just added.
     */
    create_new(db, id, name, description) {
        return db.none(
            `
                INSERT INTO platform(id, name, description)
                VALUES ($(id), $(name), $(description))
            `,
            { id, name, description }
        )
            .then(() => new Platform(db, id, name, description));
    },

    /**
     * Validates the inputs for creating or editing a platform.
     * @param {*} properties A structure containing all the properties to check.
     */
    validate_inputs(
        { platform_id: inp_platform_id, name: inp_name, description: inp_description }
    ) {
        let errors = [];

        const platform_id = parseInt(inp_platform_id);
        const name = UTILS.ensure_string(inp_name).trim();
        const description = UTILS.ensure_string(inp_description).trim();

        if (isNaN(platform_id))
            errors.push("Platform identifier must be an integer.");
        else if (platform_id < 1 || platform_id > 65535)
            errors.push("Platform identifier must be in the range from 1 to 65535.");
        if (name.length < 1 || name.length > 100)
            errors.push("Title must be 1 to 100 characters long.");
        if (description.length > 5000)
            errors.push("Description must be at most 5000 characters long.");

        return {
            errors,
            safe_inputs: { platform_id, name, description },
        };
    }
}

function platform_from_row(db, { id, name }) {
    return new Platform(db, id, name);
}

function Platform(db, inp_id, inp_name) {
    this.id = inp_id;
    this.name = inp_name;

    let description = null;
    /**
     * Fetches the description of this platform from the database.
     */
    this.fetch_description = function () {
        return description !== null
            ? Promise.resolve(description)
            : db.one(
                `
                SELECT description
                FROM platform
                WHERE id=$(id)
            `,
                { id: this.id }
            ).then(({ description }) => description);
    }

    /**
     * Updates this platform with the new properties.
     */
    this.update = function (new_id, new_name, new_description) {
        return db.none(
            `
                UPDATE platform
                SET id = $(new_id), name = $(new_name), description = $(new_description)
                WHERE id=$(old_id)
            `,
            { old_id: this.id, new_id, new_name, new_description }
        ).then(() => {
            this.id = new_id;
            this.name = new_name;
            description = new_description;
        });
    }

    this.delete = function () {
        return db.none(
            `
                DELETE FROM platform
                WHERE id = $(id)
            `,
            { id: this.id }
        ).then(() => {
            // Make sure we don't use this ever again.
            this.id = null;
            this.name = null;
            this.delete = null;
            this.update = null;
            this.fetch_description = null;
            Object.freeze(this);
        })
    }
}


/**
 * A Result is a monad that can either be success or failure.
 */
export default class Result<T, F> {
    private readonly value: T | F;
    private readonly is_success_value: boolean;

    private constructor(value: T | F, is_success: boolean) {
        this.value = value;
        this.is_success_value = is_success;
    }

    /**
     * Creates a new Result in the success state, with the supplied inner value.
     * 
     * @param value
     * The inner success value of the Result to be created.
     * 
     * @returns
     * A Result in the success state, with `value` as its inner value.
     */
    static new_success<T, F>(value: T): Result<T, F> { return new Result(value, true) }

    /**
     * Creates a new Result in the failure state, with the supplied inner value.
     * 
     * @param value
     * The inner failure value of the Result to be created.
     * 
     * @returns
     * A Result in the failure state, with `value` as its inner value.
     */
    static new_failure<T, F>(value: F): Result<T, F> { return new Result(value, false) }

    /**
     * Returns whether the Result is a success.
     * 
     * This method is the opposite of `Result#is_failure`.
     * 
     * @returns
     * Whether the Result is a success.
     */
    get is_success(): boolean {
        return this.is_success_value;
    }

    /**
     * Returns whether the Result is a failure.
     * 
     * This method is the opposite of `Result#is_success`.
     * 
     * @returns {boolean}
     * Whether the Result is a failure.
     */
    get is_failure(): boolean {
        return !this.is_success_value;
    }

    /**
     * Returns the inner success value.
     * 
     * The Result's state must be success, otherwise the behaviour is undefined.
     * 
     * @returns
     * This Result's inner success value.
     */
    get_success(): T {
        console.assert(
            this.is_success,
            "Tried to `get_success` a Result that was actually a failure."
        );
        return <T>this.value;
    }

    /**
     * Returns the inner failure value.
     * 
     * The Result's state must be failure, otherwise the behaviour is undefined.
     * 
     * @returns
     * This Result's inner failure value.
     */
    get_failure(): F {
        console.assert(
            this.is_failure,
            "Tried to `get_success` a Result that was actually a failure."
        );
        return <F>this.value;
    }

    /**
     * Maps the success state to a different success state.
     * 
     * @param fn 
     * The function to run on the inner value, if this result is a success. The return value of
     * this method will be the inner value of the new Result returned by `fn`.
     * 
     * @returns
     * A Result in the success state, with its value set to the result of the passed in function.
     */
    map_success<U>(fn: (value: T) => U): Result<U, F> {
        if (this.is_success)
            return Result.new_success(fn(this.get_success()));
        else
            return Result.new_failure(this.get_failure());
    }

    /**
     * Maps the failure state to a different success state.
     * 
     * @param fn 
     * The method to run on the inner value, if this result is a failure. The return value of
     * this method will be the inner value of the new Result returned by `fn`.
     * 
     * @returns
     * A Result in the failure state, with its value set to the result of the passed in function.
     */
    map_failure<U>(fn: (value: F) => U): Result<T, U> {
        if (this.is_success)
            return Result.new_success(this.get_success());
        else
            return Result.new_failure(fn(this.get_failure()));
    }

    /**
     * If this Result is a success, this will run the supplied function on its inner value. The
     * result of the called function must be a new Result.
     * 
     * This method is similar to `Result#map_success`, but the passed in function is supposed to
     * return a new Result, not a new inner value.
     * 
     * @param fn 
     * If this Result is in the success state, `fn` will be run on this Result's inner
     * value. In this case, the return value of `fn` is also the return value of this method.
     * 
     * @returns
     * If this Result is in the success state, this will return the return value of `fn`. Otherwise,
     * this will return this Result.
     */
    success_then<U>(fn: (value: T) => Result<U, F>): Result<U, F> {
        if (this.is_success)
            return fn(<T>this.value);
        else
            return Result.new_failure(<F>this.value);
    }

    /**
    * If this Result is a success, this will run the supplied function on its inner value. The
    * result of the called function must be a new Result.
    * 
    * This method is similar to `Result#map_failure`, but the passed in function is supposed to
    * return a new Result, not a new inner value.
    * 
    * @param {function} fn 
    * If this Result is in the success state, `fn` will be run on this Result's inner
    * value. In this case, the return value of `fn` is also the return value of this method.
    * 
    * @returns {Result}
    * If this Result is in the success state, this will return the return value of `fn`. Otherwise,
    * this will return this Result.
    */
    failure_then<U>(fn: (value: F) => Result<T, U>): Result<T, U> {
        if (this.is_success)
            return Result.new_success(<T>this.value);
        else
            return fn(<F>this.value);
    }

    /**
     * If this result is in the success state, returns the success value. If not, returns a default
     * value.
     * 
     * @param default_value
     * The default value to use when the Result is in the failure state.
     * 
     * @returns The success value or a default value.
     */
    success_or_default(default_value: T): T {
        if (this.is_success)
            return <T>this.value;
        else
            return default_value;
    }

    /**
     * If this result is in the failure state, returns the failure value. If not, returns a default
     * value.
     * 
     * @param {*} default_value
     * The default value to use when the Result is in the success state.
     * 
     * @returns The failure value or a default value.
     */
    failure_or_default(default_value: F): F {
        if (this.is_success)
            return default_value;
        else
            return <F>this.value;
    }

    /**
     * Changes a `Result<Promise<A, E>, B>` into a `Promise<Result<A, B>, E>`.
     * 
     * @returns {extern:Promise.<Result>}
     */
    success_extract_promise<E>(this: Result<Promise<E>, F>): Promise<Result<E, F> | E> {
        if (this.is_success) {
            console.assert(this.value instanceof Promise);
            return (<Promise<E>>this.value)
                .then((inner_value: E) => Result.new_success<E, F>(inner_value));
        } else
            return Promise.resolve(Result.new_failure(<F>this.value));
    }

    /**
     * Changes a `Result<A, Promise<B, E>>` into a `Promise<Result<A, B>, E>`.
     * 
     * @returns {extern:Promise.<Result>}
     */
    failure_extract_promise<E>(this: Result<T, Promise<E>>): Promise<Result<T, E> | E> {
        if (this.is_success)
            return Promise.resolve(Result.new_success(<T>this.value));
        else {
            console.assert(this.value instanceof Promise);
            return (<Promise<E>>this.value)
                .then((inner_value: E) => Result.new_failure<T, E>(inner_value));
        }
    }
}

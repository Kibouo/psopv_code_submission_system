import { readFileSync } from "fs";
import session from "express-session";
import { Application } from "express";
import DB from "./database";
import { ssl_usable } from "./utils";

// Amount of milliseconds that a session will be valid.
const LIFETIME = 2 * 60 * 60 * 1000;
const COOKIE_SECRET = readFileSync("/run/secrets/cookie_secret");
const COOKIE_NAME = "code_sub";
const SESSION_TABLE = "user_session";
const pg_session = require("connect-pg-simple")(session);

export function init(express_app: Application) {
    express_app.use(session({
        // Where to store session data
        store: new pg_session({
            pgPromise: DB,
            tableName: SESSION_TABLE
        }),
        secret: `${COOKIE_SECRET}`,
        // Cookie settings
        cookie: {
            httpOnly: true,
            maxAge: LIFETIME,
            signed: true,
            sameSite: "strict",
            secure: ssl_usable()
        },
        name: COOKIE_NAME,
        // Update cookie expiration date
        rolling: true,
        // Update session expiration date
        // Not required as pg_session implements the touch method.
        resave: false,
        // Save uninitialized session
        // false: no session will be created until the user logs in.
        // No login? No cookie! >:(
        saveUninitialized: false,
        // Result of unsetting req.session
        unset: 'destroy'
    }));
}

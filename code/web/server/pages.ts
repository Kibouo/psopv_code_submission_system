export const page_handlers = {
    login: require("./pages/login").handle_page,
    password_reset_request: require("./pages/password_reset_request").handle_page,
    password_reset: require("./pages/password_reset").handle_page,
    dashboard: require("./pages/dashboard").handle_page,
    course: {
        page: require("./pages/course").handle_page,
        edit: require("./pages/edit_course").handle_page,
        manage_students: require("./pages/manage_users").handle_student_page,
        manage_teachers: require("./pages/manage_users").handle_teacher_page,
    },
    profile_page: require("./pages/profile").handle_page,
    superpanel: require("./pages/super").handle_page,
    add_submission: require("./pages/add_submission").handle_page,
    submission_details: require("./pages/submission_details").handle_page,
    delete_submission: require("./pages/delete_submission").handle_page,
    super: {
        add_users: require("./pages/super/add_users").handle_page,
        add_course: require("./pages/super/add_course").handle_page,
        all_courses: require("./pages/super/all_courses").handle_page,
        find_users: require("./pages/super/find_users").handle_page,
        delete_users: require("./pages/super/delete_users").handle_page,
    },
    assignment: {
        page: require("./pages/assignment").handle_page,
        create_assignment: require("./pages/create_assignment").handle_page,
        submission_history: require("./pages/submission_history").handle_page,
        edit_assignment: require("./pages/edit_assignment").handle_page,
    },
    platform: {
        list: require("./pages/platform/list").handle_page,
        show: require("./pages/platform/info").handle_page,
        edit: require("./pages/platform/edit").handle_page,
        new: require("./pages/platform/new").handle_page,
        delete: require("./pages/platform/delete").handle_page,
    }
}

export default page_handlers;
const UTILS = require("./utils");
const { path } = require("./router");
import DATABASE_CONNECTION from "./database";

module.exports = {
    /**
     * Add welcome email to the queued emails.
     *
     * @param {Number} user_id The user id of the recipient.
     * @param {String} username The username of the new user.
     */
    add_welcome_email: function (user_id, username) {
        const extra_data = {
            username: username,
            reset_request_link: path.password_reset_request().url
        };
        return DATABASE_CONNECTION.none(
            `INSERT INTO email (user_id, type, extra_data)
            VALUES ($(user_id), 'welcome', $(extra_data))`,
            { user_id, extra_data }
        );
    },

    /**
     * Add a password reset token email to the queued emails.
     *
     * @param {Number} user_id The user id of the recipient.
     * @param {String} token The password reset token.
     */
    add_reset_token_email: function (user_id, token) {
        const extra_data = {
            reset_link: path.password_reset().url,
            reset_token: token,
            expire_time: UTILS.lifetime_reset_token_hours(),
            reset_request_link: path.password_reset_request().url
        };
        return DATABASE_CONNECTION.none(
            `INSERT INTO email (user_id, type, extra_data)
            VALUES ($(user_id), 'reset_token', $(extra_data))`,
            { user_id, extra_data }
        );
    },

    /**
     * Add a "password has been reset" email to the queued emails.
     *
     * @param {Number} user_id The user id of the recipient.
     */
    add_password_reset_done_email: function (user_id) {
        return DATABASE_CONNECTION.none(
            `INSERT INTO email (user_id, type)
            VALUES ($(user_id), 'password_reset_done')`,
            { user_id }
        );
    },

    /**
     * Add a "submission received" email to the queued emails.
     *
     * @param {Number} user_id The user id of the recipient.
     * @param {String} assignment_name The name of the assignment to which has been submitted.
     * @param {String} submission_link The submission object that has been submitted.
     */
    add_submission_receive_email: function (user_id, assignment_name, submission_link) {
        const extra_data = {
            assignment_name: assignment_name,
            submission_link: submission_link
        };
        return DATABASE_CONNECTION.none(
            `INSERT INTO email (user_id, type, extra_data)
            VALUES ($(user_id), 'submission_receive', $(extra_data))`,
            { user_id, extra_data }
        );
    }
}
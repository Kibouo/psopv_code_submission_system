const { User } = require("./user");
const UTILS = require("./utils");
const router = require("./router");
const CSV_MODULE = require("./csv.js");
import Course from "./course";
import DB from "./database";

module.exports = {

    handle_page: async function (request, response, user_type) {
        const course_id = request.params.course_id;
        const user = await User.from_current_session(request);
        const course = await Course.from_id(course_id);

        if (!user)
            return response.redirect(router.path.login().url);
        else if (!course)
            return response.sendStatus(404);
        else if (!await user.can_manage_teachers(course_id))
            return response.sendStatus(403);
        else if (request.method === "GET")
            return render_page(response, course, user, [], [], [], DB, user_type);
        else {
            let errors = [];
            let results = [];
            let csv_errors = [];
            let input = { search_input: request.body.search_input, users_csv: request.body.users_csv };
            if (typeof request.body.search_btn === "string") {

                // search DB for given pattern
                let search_input = this.validate_search_input(request.body.search_input, errors);
                if (errors.length === 0) {
                    results = await User.search(search_input);
                }

                // if Ajax request
                if (request.get("Accept") === "json") {
                    return response.send(JSON.stringify(results));
                }

            } else if (request.body.add_btn) {

                // add teacher to csv
                const search_input = this.validate_search_input(request.body.search_input, errors);
                if (errors.length === 0) {
                    results = await User.search(search_input);
                }

                const user_to_add_id = UTILS.ensure_number(request.body.add_btn);
                // add the teacher to the csv
                input.users_csv = await this.add_user_to_csv_textarea(user_to_add_id, DB, input.users_csv);


            } else if (request.body.save) {
                // insert csv into DB as students of the course
                let csv_input = this.validate_csv_input(request.body.users_csv, errors);
                if (errors.length === 0) {
                    let validated_csv = await this.parse_and_validate_csv(request.body.users_csv, DB);
                    csv_errors = validated_csv.csv_errors;

                    if (csv_errors.length === 0) {
                        return this.save_csv(DB, course, validated_csv.users_to_add, user_type).then(() => {
                            response.redirect(router.path.course(course).url);
                        });
                    }
                }
            }

            return render_page(response, course, user, results,
                errors, csv_errors, DB, user_type, input);
        }
    },


    // TODO: add a notification/warning/etc. when a user is removed as student when made teacher or vice versa.
    validate_csv_input: function (users_csv, errors) {
        users_csv = UTILS.ensure_string(users_csv);

        if (users_csv.length > 5000)
            errors.push("The user csv must be no longer than 5000 characters.");

        return users_csv;
    },

    validate_search_input: function (search_input, errors) {

        search_input = UTILS.ensure_string(search_input);

        if (!(search_input.length > 0 && search_input.length < 255))
            errors.push("The search input must be between 0 and 256 characters long.");

        return search_input;

    },

    get_previous_search_results: async function (request, database) {
        // get the previous results so they don't clear after adding a user
        let results = await Promise.all(
            request.body.add_btn.map(user_id => {
                return User.from_id(user_id);
            }),
        );

        return results;
    },

    add_user_to_csv_textarea: async function (user_to_add_id, database, users_csv) {
        const user_to_add_index = UTILS.ensure_number(user_to_add_id);
        const user_to_add = await User.from_id(user_to_add_id);
        if (user_to_add) {
            users_csv = users_csv.trim();
            if (!(users_csv.endsWith("\n"))) {
                users_csv += "\n"
            }

            users_csv += (user_to_add.email + "\n");
        }

        return users_csv;

    },

    parse_and_validate_csv: async function (users_csv, database) {

        // Type: [{line: number, result: Result<{name: string, mail: string}, string>}];
        let lines = CSV_MODULE.parse_csv(users_csv, ['mail'])
            .map((result, index) => ({ result: result, line: index + 1 }))
            .filter( // filter out all empty rows
                ({ result }) => result
                    .map_failure(failure => failure !== CSV_MODULE.CsvError.EmptyRow)
                    .failure_or_default(true)
            )
            // refactor all csv errors into strings
            .map(({ line, result }) => ({ line, result: result.map_failure(CSV_MODULE.csv_error_to_string) }));


        // Collect any errors into an array.
        let csv_errors = lines
            // Add line numbers so the user can more easily find what we're complaining about.
            .map(({ result, line }) => result.map_failure(msg => `Line ${line}: ${msg}.`))
            // Keep only failures.
            .filter(result => result.is_failure)
            // Unwrap the failures.
            .map(result => result.get_failure());


        // get the students from the csv
        let users_to_add = lines
            // Remove line numbers.
            .map(({ line, result }) => result)
            // Keep only successes.
            .filter(result => result.is_success)
            // Unwrap the successes.
            .map(result => result.get_success());

        let registered_emails = [];
        // get a list of all users that exist in the database
        if (users_to_add.length > 0) {
            registered_emails = await database.any(
                "SELECT email FROM user_account WHERE email IN ($1:csv)",
                [users_to_add.map(user => user.mail)]
            );
            registered_emails = registered_emails.map(email_obj => {
                return email_obj.email;
            });
        }

        // push errors for every user that does not exist in the database
        for (let usr of users_to_add) {
            if (!(registered_emails.includes(usr.mail)))
                csv_errors.push("User does not exist: " + usr.mail);
        }

        users_to_add = (await database.task(t => {
            return Promise.all(users_to_add
                // create user objects (promises)s
                .map(user => User.from_email(user.mail))
            );
        })).filter(user => user !== null); // filter out non-existing users


        // If the same email was supplied twice, add errors.
        for (let duplicate_email of UTILS.find_duplicates(users_to_add.map(user => user.email)))
            csv_errors.push(`Trying to delete multiple users with email ${duplicate_email}.`);


        return { users_to_add, csv_errors };

    },

    save_csv: function (database, course, users_to_add, user_type) {

        return database.task(t => {
            return Promise.all([
                add_users_to_course(users_to_add, course, t, user_type),
                remove_users_from_course(users_to_add, course, t, user_type)
            ]);
        });

    },

}

async function render_page(response, course, user, results, errors, csv_errors, database, user_type, previous_input) {

    if (!previous_input) {

        let usr_csv = "";
        if (user_type === "student") {
            usr_csv = users_to_csv(await course.get_students())
        } else {
            usr_csv = users_to_csv(await course.get_teachers())
        }

        previous_input = {
            search_input: "",
            users_csv: usr_csv,
        };
    }

    return response.render("manage_users.pug", {
        header: {
            title: course.name_normalized
        },
        navigation: {
            page_stack: router.path.manage_user(course, user_type),
        },
        results: results,
        errors: {
            validation_errors: errors,
            csv_errors: csv_errors,
        },
        previous_input,
        user_type,
        utils: UTILS,
        user: user,
        router: router,
    });

}


function add_users_to_course(users_to_add, course, database, user_type) {
    return Promise.all(
        users_to_add.map(user => {
            return course.add_user(user, user_type);
        }),
    );
}

function remove_users_from_course(users_in_course, course, database, user_type) {
    return course.remove_users(users_in_course, user_type);
}

function users_to_csv(users) {
    let csv_users_map = users.map(user => {
        return user.email;
    });

    return csv_users_map.join("\n");
}

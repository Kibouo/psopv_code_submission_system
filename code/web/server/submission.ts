const UTILS_MODULE = require("./utils");
import DB from "./database";
import User from "./user";
import { Assignment } from "./assignment";

const SUBMISSION_DB_FIELDS: string =
    `
        submission.id,
        EXTRACT(epoch FROM submission.submitted_on) AS submitted_on,
        submission.platform_id,
        submission.user_id,
        submission.assignment_id,
        submission.is_prepare_success,
        submission.pipeline_status,
        submission.mime_type
    `;

interface SubmissionDbRow {
    id: number;
    submitted_on: number;
    platform_id: number;
    user_id: number;
    assignment_id: number;
    is_prepare_success: boolean;
    pipeline_status: string;
    mime_type: string;
};

export enum PipelineStatus {
    Wait, Prepare, Example, Final, Done
}

/**
 * Converts a string to a `PipelineStatus`.
 *
 * If the string is not a valid representation of a `PipelineStatus`, an error is thrown.
 *
 * @param string The string to convert to a `PipelineSatus`.
 */
export function string_to_pipeline_status(string: string): PipelineStatus {
    switch (string) {
        case "wait": return PipelineStatus.Wait;
        case "prepare": return PipelineStatus.Prepare;
        case "example": return PipelineStatus.Example;
        case "final": return PipelineStatus.Final;
        case "done": return PipelineStatus.Done;
        default: throw new Error("Invalid pipeline status string.");
    }
}

/**
 * Converts a `PipelineStatus` to a string.
 *
 * @param status The `PipelineStatus` to convert to a string.
 */
export function pipeline_status_to_string(status: PipelineStatus): string {
    switch (status) {
        case PipelineStatus.Wait: return "wait";
        case PipelineStatus.Prepare: return "prepare";
        case PipelineStatus.Example: return "example";
        case PipelineStatus.Final: return "final";
        case PipelineStatus.Done: return "done";
        default: throw new Error("Invalid pipeline status.");
    }
}

export class Submission {
    readonly id: number;
    readonly submitted_on: Date;
    readonly platform_id: number;
    readonly user_id: number;
    readonly assignment_id: number;
    readonly is_prepare_success: boolean;
    readonly pipeline_status: PipelineStatus;
    readonly mime_type: string;

    private constructor(
        id: number,
        submitted_on: number,
        platform_id: number,
        user_id: number,
        assignment_id: number,
        is_prepare_success: boolean,
        pipeline_status: PipelineStatus,
        mime_type: string
    ) {
        this.id = id;
        this.submitted_on = new Date(submitted_on * 1000);
        this.platform_id = platform_id;
        this.user_id = user_id;
        this.assignment_id = assignment_id;
        this.is_prepare_success = is_prepare_success;
        this.pipeline_status = pipeline_status;
        this.mime_type = mime_type;
    }

    /**
     * Fetches the source code.
     *
     * @return A promise resolving to a buffer of hex data.
     */
    async fetch_source_code(): Promise<Buffer> {
        return DB.one(`SELECT source_code FROM submission WHERE id = $(id)`, { id: this.id })
            .then(({ source_code }) => source_code);
    }

    /**
     * Fetches the stdout and stderr output of the prepare stage.
     *
     * @return A structure containing both the stdout and stderr output of the prepare stage.
     */
    async fetch_prepare_output(): Promise<{ stdout: string, stderr: string }> {
        const { prepare_stdout, prepare_stderr } = await DB.one(
            `SELECT prepare_stdout, prepare_stderr FROM submission WHERE id = $(id)`,
            { id: this.id }
        );

        return { stdout: prepare_stdout, stderr: prepare_stderr };
    }

    private static from_db_row(row: SubmissionDbRow): Submission {
        return new Submission(
            row.id,
            row.submitted_on,
            row.platform_id,
            row.user_id,
            row.assignment_id,
            row.is_prepare_success,
            string_to_pipeline_status(row.pipeline_status),
            row.mime_type
        );
    }

    private static from_db_row_or_null(row: SubmissionDbRow | null): Submission | null {
        return row ? Submission.from_db_row(row) : null;
    }

    private static from_db_rows(rows: SubmissionDbRow[]): Submission[] {
        return rows.map(Submission.from_db_row);
    }

    /**
     * Gets a handle to the submission with the given id.
     * @param id The id of the submission to get a handle to.
     */
    static async from_id(id: number): Promise<Submission | null> {
        return DB.oneOrNone(
            `SELECT ${SUBMISSION_DB_FIELDS} FROM submission WHERE id=$(id)`,
            { id }
        ).then(Submission.from_db_row_or_null)
    }

    /**
     * Gets all submissions of a specific assignment.
     * @param assignment_id The id of the assignment to get all submissions from.
     */
    static async all_from_assignment(assignment_id: number): Promise<Submission[]> {
        return DB.any(
            `
                SELECT ${SUBMISSION_DB_FIELDS}
                FROM submission
                WHERE assignment_id=$(assignment_id)
                ORDER BY submitted_on DESC
            `,
            { assignment_id }
        ).then(Submission.from_db_rows)
    }

    /**
     * Gets all submission made by a student for a specific assignment.
     * @param student_id The id of the student.
     * @param assignment_id The id of the assignment.
     */
    static async all_from_student_assignment(
        student_id: number, assignment_id: number
    ): Promise<Submission[]> {
        return DB.any(
            `
                SELECT ${SUBMISSION_DB_FIELDS}
                FROM submission
                WHERE assignment_id=$(assignment_id) AND user_id=$(student_id)
            `,
            { assignment_id, student_id }
        ).then(Submission.from_db_rows);
    }

    /**
     * Gets the most recent submission made by a student for a specific assignment.
     * @param student_id The id of the student.
     * @param assignment_id The id of the assignment.
     */
    static async most_recent_by_student_for_assignment(
        student_id: number,
        assignment_id: number
    ): Promise<Submission | null> {
        const recent_submissions = await Submission.all_recent_by_student_for_assignment(
            student_id, assignment_id, 1
        );
        return recent_submissions.length > 0 ? recent_submissions[0] : null;
    }

    /**
     * Fetches the most recent submissions made by a student from the database.
     *
     * @param student_id
     * The id of the student to get the recent submissions from.
     *
     * @param count
     * The maximum amount of submissions to fetch. The most recent ones are always fetched.
     */
    static async all_recent_by_student(student_id: number, count: number): Promise<Submission[]> {
        return DB.any(
            `
                SELECT ${SUBMISSION_DB_FIELDS}
                FROM submission
                WHERE user_id=$(student_id)
                ORDER BY submitted_on DESC
                LIMIT $(count)
            `,
            { student_id, count }
        ).then(Submission.from_db_rows);
    }

    /**
     * Fetches the most recent submissions made to a specific assignment by a student,
     * from the database.
     *
     * @param student_id
     * The id of the student to get the recent submissions from.
     *
     * @param assignment_id
     * The id of the assignment which we want specific info from.
     *
     * @param count
     * The maximum amount of submissions to fetch. The most recent ones are always fetched.
     */
    static async all_recent_by_student_for_assignment(
        student_id: number, assignment_id: number, count: number
    ): Promise<Submission[]> {
        return DB.any(
            `
                SELECT ${SUBMISSION_DB_FIELDS}
                FROM submission JOIN assignment ON submission.assignment_id=assignment.id
                WHERE submission.user_id=$(student_id) AND submission.assignment_id=$(assignment_id)
                ORDER BY submission.submitted_on DESC
                LIMIT $(count)
            `,
            { student_id, assignment_id, count }
        ).then(Submission.from_db_rows);
    }

    /**
     * Creates a new submission, and returns a handle to it.
     * @param source_code The source code of the new submission.
     * @param platform_id The id of the platform the submission was written for.
     * @param user_id The id of the user that posted the submission.
     * @param assignment_id The id of the assignment the submission was made in.
     * @param mime_type The MIME type of the uploaded file.
     */
    static async create_new(
        user_id: number, assignment_id: number, platform_id: number, source_code: string, mime_type: string
    ): Promise<Submission> {
        return DB.one(
            `
                INSERT INTO submission(source_code, platform_id, user_id, assignment_id, mime_type)
                VALUES ($(source_code), $(platform_id), $(user_id), $(assignment_id), $(mime_type))
                RETURNING ${SUBMISSION_DB_FIELDS}
            `,
            { user_id, assignment_id, platform_id, source_code, mime_type }
        ).then(Submission.from_db_row);
    }

    private cached_submittor: Promise<User> | null = null;
    /**
     * Returns a handle to the user that made this submission.
     */
    async submittor(): Promise<User> {
        if (!this.cached_submittor)
            this.cached_submittor = <Promise<User>>User.from_id(this.user_id);
        return this.cached_submittor;
    }

    private cached_assignment: Promise<Assignment> | null = null;
    /**
     * Returns a handle to the assignment this submission was made in.
     */
    async assignment(): Promise<Assignment> {
        if (!this.cached_assignment)
            this.cached_assignment = <Promise<Assignment>>Assignment.from_id(this.assignment_id);
        return this.cached_assignment;
    }

    async fetch_successful_example_test_count(): Promise<number> {
        return DB.one(
            `
                SELECT count(*) successful_test_count
                FROM test_result JOIN test ON test_result.test_id=test.id
                WHERE submission_id=$(id) AND test.type = 'example' AND test_result.is_success=true
            `,
            { id: this.id }
        ).then(({ successful_test_count }) => Number(successful_test_count));
    }

    async fetch_successful_final_test_count(): Promise<number> {
        return DB.one(
            `
                SELECT count(*) successful_test_count
                FROM test_result JOIN test ON test_result.test_id=test.id
                WHERE submission_id=$(id) AND test.type = 'final' AND test_result.is_success=true
            `,
            { id: this.id }
        ).then(({ successful_test_count }) => Number(successful_test_count));
    }


    async delete() {
        await DB.none(
            `
                DELETE FROM submission
                WHERE id = $(id)
            `,
            { id: this.id }
        );
    }
}

export default Submission;
import { readFileSync } from "fs";
const MARKDOWN = require("markdown-it")
    ('commonmark')
    .use(require('markdown-it-katex'))
    .use(require('markdown-it-highlightjs'));
const SCRYPT = require("scrypt");
const UUID_V4 = require("uuid/v4");
const SCRYPT_PARAMS = SCRYPT.paramsSync(0.2, 128000);
const RESET_LINK_HOURS = 2;
// Amount of milliseconds that a reset link will be valid.
const RESET_LINK_LIFETIME = RESET_LINK_HOURS * 60 * 60 * 1000;

interface DateStrings {
    utc_string: string,
    utc_day: string,
    utc_month: string,
    utc_iso: string,
    utc_hour_min: string,
}

/**
 * Check whether ssl key, cert, and password are available (does not check the actual content).
 *
 * @returns {boolean} whether cert and key are valid.
 */
export function ssl_usable(): boolean {
    try {
        const cert = readFileSync("/run/secrets/ssl_cert").toString();
        const key = readFileSync("/run/secrets/ssl_key").toString();
        const password = readFileSync("/run/secrets/ssl_password").toString();

        return cert !== "." && key !== ".";
    } catch (error) {
        console.log("Error: SSL private key, SSL certificate, and/or SSL password not found.");
        return false;
    }
}

export const SSL_CERT = ssl_usable() ? readFileSync("/run/secrets/ssl_cert").toString() : "";
export const SSL_KEY = ssl_usable() ? readFileSync("/run/secrets/ssl_key").toString() : "";
export const SSL_PASSWORD = ssl_usable() ? readFileSync("/run/secrets/ssl_password").toString() : "";

/**
 * Parse MarkDown and LaTeX maths to HTML.
 *
 * @param {String} text The text to parse.
 *
 * @returns {String} Text as HTML.
 */
export function parse_markdown(text: string): string {
    return MARKDOWN.render(text);
}

/**
 * Get the time at which a password reset token, created right now, would expire.
 *
 * @returns {Date} The time of expiration.
 */
export function get_new_token_expire_date(): Date {
    return new Date(Date.now() + RESET_LINK_LIFETIME);
}

/**
 * Amount of hours that a password reset token will be valid.
 *
 * @returns {Number} Amount of hours.
 */
export function lifetime_reset_token_hours(): number {
    return RESET_LINK_HOURS;
}

/**
 * Generates a crypto-random UUID, to be used as a password reset token.
 *
 * @returns {String} A string containing the generated token.
 */
export function gen_password_reset_token(): string {
    return UUID_V4();
}

/**
 * filters the string removing all non alphanumeric characters
 * and replacing every space by a dash
 *
 * @param {String} str The string to normalize.
 * @return {String} The normalized string
 */
export function normalize_string(str: string): string {
    const PATTERN = /[^a-z0-9 ]+/g
    return str.toLowerCase().replace(PATTERN, "").replace(/ +/g, "-");
}

/**
 * Hashes a given string using scrypt with the pre-defined parameters.
 *
 * @param {String} password The string to be hashed.
 * @returns {external:Promise<String>} A promise resolving to a
 * hex string resulting from the hashing.
 */
export function hash(password: string): Promise<string> {
    return SCRYPT
        .kdf(password, SCRYPT_PARAMS)
        .then((hash: any) => hash.toString("hex"));
}

/**
 * Checks whether the given string generates the given hash.
 *
 * @param {String} hash The hash to compare to.
 * @param {String} password The string to be hashed.
 * @returns {external:Promise<boolean>} A promise resolving to
 * whether the comparison was successful or not.
 */
export function match_hash_string(hash: string, password: string): Promise<boolean> {
    const hash_buffer = Buffer.from(hash, "hex");
    return SCRYPT
        .verifyKdf(hash_buffer, password)
        .catch((_: any) => { return false; });
}

/**
 * Given two array of strings containing tests input/output respectively,
 * this function will create an array of test objects with an input and output.
 *
 * @param {String[]} test_inputs The inputs of the tests.
 * @param {String[]} test_outputs The outputs of the tests.
 * @returns {{ input: string, output: string }[]} An array of objects representing the tests inputs and outputs
 */
export function test_io_array_to_object(test_inputs: string[], test_outputs: string[]): { input: string, output: string }[] {

    // We'll be nice. If the user tampered with the HTML we'll keep as many tests as we can.
    if (test_inputs.length > test_outputs.length)
        return test_outputs.map((output, index) => ({ input: test_inputs[index], output }));
    else
        return test_inputs.map((input, index) => ({ input, output: test_outputs[index] }));

}

/**
 * Converts a date object to a collection of strings that can be used to display
 * the date to the user.
 *
 * @param {extern:Date} date
 * The Date object to turn into strings.
 *
 * @returns The converted strings.
 */
export function date_to_strings(date: Date): DateStrings {
    console.assert(date instanceof Date);
    const full_year_nr = date.getUTCFullYear();
    const utc_full_year =
        full_year_nr < 10
            ? `000${full_year_nr}`
            : full_year_nr < 100
                ? `00${full_year_nr}`
                : full_year_nr < 1000
                    ? `00${full_year_nr}`
                    : new String(full_year_nr);
    const utc_month = date.getUTCMonth() + 1 < 10
        ? `0${date.getUTCMonth() + 1}`
        : new String(date.getUTCMonth() + 1);
    const utc_date = date.getUTCDate() < 10
        ? `0${date.getUTCDate() + 1}`
        : new String(date.getUTCDate());
    const utc_hours = date.getUTCHours() < 10
        ? `0${date.getUTCHours()}`
        : new String(date.getUTCHours());
    const utc_minutes = date.getUTCMinutes() < 10
        ? `0${date.getUTCMinutes()}`
        : new String(date.getUTCMinutes());

    return {
        utc_string: date.toUTCString(),
        utc_day: date.getUTCDate().toString(),
        utc_month: month_index_to_string(date.getUTCMonth()),
        // max precision is day thus using toISOString() is undesirable here
        utc_iso: utc_full_year + '-' + utc_month + '-' + utc_date,
        utc_hour_min: utc_hours + ':' + utc_minutes,
    }
}

/**
 * Makes sure a string is an integer.
 * @param {String} string The string to test.
 * @returns {Number} The value, or 0 when it does not represent an integer.
 */
export function ensure_number(string: string): number {
    const value = parseInt(string, 10);
    return isNaN(value) ? 0 : value;
}

/**
 * Makes sure a parsed string represents a time.
 * @param {String} string The string to test.
 * @returns {String|null} The string, or null when it does not represent a time.
 */
export function ensure_time(string: string): string | null {
    const REGEX = "([0-9]?[0-9]):([0-9]?[0-9])";
    const matches = ensure_string(string).match(REGEX);

    if (!matches) return null;
    else {
        const time = {
            hours: parseInt(matches[0], 10),
            minutes: parseInt(matches[1], 10),
        };
        if (time.hours < 0 || time.hours >= 24) return null;
        else if (time.minutes < 0 || time.minutes >= 60) return null;
        else return string;
    };
}

/**
 * Makes sure a parsed string represents a date.
 * @param {String} string The string to test.
 * @returns {String|null} The string, or null when it does not represent a date.
 */
export function ensure_date(string: string): string | null {
    const REGEX = new RegExp("([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])");
    string = ensure_string(string);
    if (!REGEX.test(string)) return null;
    const date = Date.parse(string);
    if (isNaN(date)) return null;
    else return string;
}

/**
 * Makes sure the passed value is a string.
 * @param {*} value The value to check.
 * @returns {string} The value if it was a string, or "" otherwise.
 */
export function ensure_string(value: any): string {
    return typeof value === "string" ? value : "";
}

/**
 * Makes sure the passed value is an array.
 * @param {*} value The value to check.
 * @returns {[*]} The value if it was a n array, or [] otherwise.
 */
export function ensure_array(value: any): any[] {
    return Array.isArray(value) ? value : [];
}

/**
 * Gives a month string based on a month index.
 *
 * @param {Number} utc_month_index A 0-based index, between 0 and 12,
 * for the month.
 * @return {String} A 3-letter long, all-caps month that is the
 * utc_month_index+1'th month of the year.
 */
function month_index_to_string(utc_month_index: number): string {
    const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
        'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

    return months[utc_month_index];
}

export function* find_duplicates<T>(array: T[]): Iterable<T> {
    const sorted_array = array.sort();
    let current_index = 1;
    while (current_index < sorted_array.length) {
        if (sorted_array[current_index] === sorted_array[current_index - 1]) {
            yield sorted_array[current_index];
            while (sorted_array[current_index] === sorted_array[current_index - 1])
                current_index += 1;
        } else
            current_index += 1;
    }
}

export function month_index_to_full_string(month_index: number): string {
    const months = ['January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'];

    return months[month_index];
}
import { normalize_string } from "./utils";
import User from "./user";
import DB from "./database";

const COURSE_DB_FIELDS: string = "id, name, name_normalized";

const NAME_MIN_LENGTH = 1;
const NAME_MAX_LENGTH = 200; // inclusive

interface CourseDbRow {
    id: number,
    name: string,
    name_normalized: string
};

export class Course {
    readonly id: number;
    name: string;
    name_normalized: string;

    private constructor(id: number, name: string, name_normalized: string) {
        this.id = id;
        this.name = name;
        this.name_normalized = name_normalized;
    }

    private static from_db_row_or_null(row: CourseDbRow | null): Course | null {
        return row ? Course.from_db_row(row) : null;
    }

    private static from_db_row(row: CourseDbRow): Course {
        return new Course(row.id, row.name, row.name_normalized);
    }

    private static from_db_rows(rows: CourseDbRow[]): Course[] {
        return rows.map(Course.from_db_row);
    }


    /**
     * Creates a Course object from a given id.
     *
     * @param id
     * The id of the requested course.
     *
     * @return
     * A promise resolving to a Course object when an entry is found for the given id. Else
     * resolving in null.
     */
    static async from_id(id: number): Promise<Course | null> {
        return DB.oneOrNone(
            `SELECT ${COURSE_DB_FIELDS} FROM course WHERE id = $(id)`,
            { id }
        ).then(Course.from_db_row_or_null);
    }


    /**
     * Get all existing courses, ordered alphabetically.
     *
     * @return A promise resolving to all courses the db has to offer, ordered alphabetically.
     */
    static async all_courses(): Promise<Course[]> {
        return DB.manyOrNone(`SELECT ${COURSE_DB_FIELDS} FROM course ORDER BY name ASC`).then(Course.from_db_rows);
    }

    /**
     * Get all courses where requested user is student of, ordered alphabetically.
     *
     * @param {number} user_id The id of the student user.
     * @return A promise resolving to all courses the db has to offer, ordered alphabetically.
     */
    static async all_courses_where_student(user_id: number): Promise<Course[]> {
        return DB.manyOrNone(
            `
                SELECT ${COURSE_DB_FIELDS}
                FROM course
                    INNER JOIN course_user_relation
                    ON course_user_relation.course_id = course.id
                WHERE course_user_relation.user_type = 'student'
                    AND course_user_relation.user_id = $(user_id)
                ORDER BY name ASC
            `,
            { user_id }
        ).then(Course.from_db_rows);
    }

    /**
     * Get all courses where requested user is teacher of, ordered alphabetically.
     *
     * @param {number} user_id The id of the teacher user.
     * @return A promise resolving to all courses the db has to offer, ordered alphabetically.
     */
    static async all_courses_where_teacher(user_id: number): Promise<Course[]> {
        return DB.manyOrNone(
            `
                SELECT ${COURSE_DB_FIELDS}
                FROM course
                    INNER JOIN course_user_relation
                    ON course_user_relation.course_id = course.id
                WHERE course_user_relation.user_type = 'teacher'
                    AND course_user_relation.user_id = $(user_id)
                ORDER BY name ASC
            `,
            { user_id }
        ).then(Course.from_db_rows);
    }

    async get_students(): Promise<User[]> {
        return User.all_students_of_course(this.id);
    }

    async get_teachers(): Promise<User[]> {
        return User.all_teachers_of_course(this.id);
    }

    async add_user(user: User, user_type: string): Promise<null> {
        console.assert(user_type === "student" || user_type === "teacher");

        return DB.none(
            `
                INSERT INTO course_user_relation (user_id, course_id, user_type)
                VALUES ($(user_id), $(course_id), $(user_type))
                ON CONFLICT (user_id, course_id) DO UPDATE SET
                    user_id=EXCLUDED.user_id,
                    course_id=EXCLUDED.course_id,
                    user_type=EXCLUDED.user_type
            `,
            { user_id: user.id, course_id: this.id, user_type }
        );
    }

    /**
     * Removes all users from a course that aren't specified in the whitelist.
     *
     * @param {Database} database The database to remove the users from.
     * @param {[User]} whitelist An array of users that may not be removed.
     * @param {String} user_type The type of the users to remove (either student or teacher).
     *
     * @returns {extern:Promise<null|DatabaseError>}
     * A promise resolving into null, and rejecting into a database error.
     */
    async remove_users(whitelist: User[], user_type: string) {
        console.assert(user_type === "student" || user_type === "teacher");

        if (whitelist.length === 0)
            return DB.none(
                `
                    DELETE FROM course_user_relation
                    WHERE course_id=$(course_id) AND user_type=$(user_type)
                `,
                { course_id: this.id, user_type }
            );
        else
            return DB.none(
                `
                    DELETE FROM course_user_relation
                    WHERE course_id=$(course_id) AND user_type=$(user_type)
                        AND user_id NOT IN ($(whitelist:csv))
                `,
                { course_id: this.id, user_type, whitelist: whitelist.map(user => user.id) }
            );
    }

    async update_name(new_name: string): Promise<void> {
        await DB.none(
            `
                UPDATE course
                    SET name = $(new_name), name_normalized=$(new_name_normalized)
                    WHERE id=$(id)
            `,
            { new_name, new_name_normalized: normalize_string(new_name), id: this.id }
        )
    }
}

/**
 * Places a course in the database.
 *
 * @param course_name The title of the course to add to the database
 * @returns A promise representing insertion success or failure.
 */
export function insert_course_into_database(course_name: string): Promise<null> {
    return DB.none(
        "INSERT INTO course (name, name_normalized) VALUES ($(name), $(name_normalized))",
        { name: course_name, name_normalized: normalize_string(course_name) }
    );
}

export function delete_from_db(course: Course): Promise<null> {
    return DB.none(
        "DELETE FROM course \
        WHERE id=${course_id}", 
        {course_id: course.id});
}

export default Course;

export function validate_course_name(name: string): string[] {
    if (typeof name !== "string")
        return ["A course name must be a string."];
    else if (name.length < NAME_MIN_LENGTH)
        return [`A course name must be at least ${NAME_MIN_LENGTH} character long.`]
    else if (name.length > NAME_MAX_LENGTH)
        return [`A course name must be at most ${NAME_MAX_LENGTH} characters long.`]
    else
        return [];
}
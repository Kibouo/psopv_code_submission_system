
const MAX_FILE_SIZE = 50 * 1024 * 1024;
const MAX_FILE_COUNT = 1;
const PORT = 80;
const SSL_PORT = 443;

type PageHandler = (request: Request, response: Response) => Promise<void>;

import express, { Request, Response } from "express";
const express_app = express();
import helmet, { IHelmetContentSecurityPolicyDirectives } from "helmet";
import DB from "./database";
import { page_handlers } from "./pages";
import { init as init_sessions } from "./session";
import { ssl_usable, SSL_CERT, SSL_KEY, SSL_PASSWORD } from "./utils";

const multer = require('multer')({
    dest: '/tmp/',
    limits: { fileSize: MAX_FILE_SIZE, files: MAX_FILE_COUNT },
});

// Jade, which is the default, has been deprecated in favour of Pug.
express_app.set("view engine", "pug");

express_app.use(helmet());
express_app.use(helmet.contentSecurityPolicy({
    directives: <IHelmetContentSecurityPolicyDirectives>{
        blockAllMixedContent: true,
        connectSrc: ["'self'"],
        defaultSrc: ["'self'"],
        formAction: ["'self'"],
        frameSrc: ["'self'"],
        imgSrc: ["'self'"],
        mediaSrc: ["'none'"],
        objectSrc: ["'none'"],
        scriptSrc: ["'self'"],
        styleSrc: ["'self'"],
        upgradeInsecureRequests: ssl_usable(),
    }
}));

const serve_static = require("serve-static");
express_app.use(serve_static("public"));
// highlight.js is a dependency of markdown-it-highlightjs.
// This enables us to use syntax highlighting css files.
express_app.use(serve_static("node_modules/highlight.js/styles/"));
express_app.use(require("body-parser").urlencoded({ extended: true }));
init_sessions(express_app);

function catch_errors(handler: PageHandler): (request: Request, response: Response) => void {
    return function (request: Request, response: Response) {
        handler(request, response).catch(error => {
            response.sendStatus(500);
            console.error("An internal server error occurred: ");
            console.error(error);
        });
    }
}

{
    // Setup routing.

    const COURSE = "courses/[A-Za-z0-9-]{0,}?:course_id([0-9]+)";
    const ASSIGNMENT = "assignments/[A-Za-z0-9-]{0,}?:assignment_id([0-9]+)";
    const SUBMISSION = "submissions/[A-Za-z0-9-]{0,}?:submission_id([0-9]+)";
    const PLATFORM = "platforms/:platform_id([0-9]+)";
    const USER = "[A-Za-z0-9-]{0,}?:user_id([0-9]+)";
    const RESET_TOKEN = ":reset_token([A-Za-z0-9-]{0,})";

    get("/", page_handlers.login);
    post("/", page_handlers.login);
    get("/password_reset_request", page_handlers.password_reset_request);
    post("/password_reset_request", page_handlers.password_reset_request);
    get(`/password_reset(/${RESET_TOKEN})?`, page_handlers.password_reset);
    post(`/password_reset(/${RESET_TOKEN})?`, page_handlers.password_reset);
    get("/dashboard", page_handlers.dashboard);
    post("/dashboard", page_handlers.dashboard);
    get(`/profile_page/${USER}`, page_handlers.profile_page);
    post(`/profile_page/${USER}`, page_handlers.profile_page);
    get("/super", page_handlers.superpanel);
    get("/super/add_course", page_handlers.super.add_course);
    post("/super/add_course", page_handlers.super.add_course);
    get("/super/all_courses", page_handlers.super.all_courses);
    get("/super/add_users", page_handlers.super.add_users);
    post("/super/add_users", page_handlers.super.add_users);
    get("/super/delete_users", page_handlers.super.delete_users);
    post("/super/delete_users", page_handlers.super.delete_users);
    get("/super/find_users", page_handlers.super.find_users);
    post("/super/find_users", page_handlers.super.find_users);
    get(`/super/add_platform`, page_handlers.platform.new);
    post(`/super/add_platform`, page_handlers.platform.new);
    get(`/${COURSE}`, page_handlers.course.page);
    post(`/${COURSE}`, page_handlers.course.page);
    get(`/${COURSE}/edit`, page_handlers.course.edit);
    post(`/${COURSE}/edit`, page_handlers.course.edit);
    get(`/${COURSE}/create_assignment`, page_handlers.assignment.create_assignment);
    get(`/${COURSE}/${ASSIGNMENT}`, page_handlers.assignment.page);
    post(`/${COURSE}/${ASSIGNMENT}`, page_handlers.assignment.page);
    get(`/${COURSE}/${ASSIGNMENT}/new_submission`, page_handlers.add_submission);
    get(`/${COURSE}/${ASSIGNMENT}/submissions/history/${USER}`, page_handlers.assignment.submission_history);
    get(`/${COURSE}/${ASSIGNMENT}/edit_assignment`, page_handlers.assignment.edit_assignment);
    get(`/${COURSE}/manage_students`, page_handlers.course.manage_students);
    post(`/${COURSE}/manage_students`, page_handlers.course.manage_students);
    get(`/${COURSE}/manage_teachers`, page_handlers.course.manage_teachers);
    post(`/${COURSE}/manage_teachers`, page_handlers.course.manage_teachers);
    get(`/${COURSE}/${ASSIGNMENT}/${SUBMISSION}`, page_handlers.submission_details);
    post(`/${COURSE}/${ASSIGNMENT}/${SUBMISSION}`, page_handlers.submission_details);
    get(`/${COURSE}/${ASSIGNMENT}/${SUBMISSION}/delete`, page_handlers.delete_submission);
    post(`/${COURSE}/${ASSIGNMENT}/${SUBMISSION}/delete`, page_handlers.delete_submission);
    get(`/platforms`, page_handlers.platform.list);
    get(`/${PLATFORM}`, page_handlers.platform.show);
    post(`/${PLATFORM}`, page_handlers.platform.show);
    get(`/${PLATFORM}/edit`, page_handlers.platform.edit);
    post(`/${PLATFORM}/edit`, page_handlers.platform.edit);
    get(`/${PLATFORM}/delete`, page_handlers.platform.delete);
    post(`/${PLATFORM}/delete`, page_handlers.platform.delete);
    // We cannot use the get or post convenience functions here because we need multer.
    express_app.post(
        `/${COURSE}/create_assignment`,
        multer.single("import_assignment"),
        catch_errors(page_handlers.assignment.create_assignment)
    );
    express_app.post(
        `/${COURSE}/${ASSIGNMENT}/edit_assignment`,
        multer.single("import_assignment"),
        catch_errors(page_handlers.assignment.edit_assignment)
    );
    express_app.post(
        `/${COURSE}/${ASSIGNMENT}/new_submission`,
        multer.single("submit_submission"),
        catch_errors(page_handlers.add_submission)
    );
}

if (ssl_usable()) {
    require('https')
        .createServer({
            key: SSL_KEY,
            cert: SSL_CERT,
            passphrase: SSL_PASSWORD,
        }, express_app)
        .listen(
            SSL_PORT,
            () => console.log(`Web application listening on port ${SSL_PORT}!`)
        );
    require('http')
        .createServer(function (request: Request, response: Response) {
            // remove old port nr
            const host = request.headers.host!.split(':')[0];

            // redirect http requests to https
            response.writeHead(
                301,
                { "Location": "https://" + host + `:${SSL_PORT}` + request.url }
            );
            response.end();
        }).listen(
            PORT,
            () => console.log(`Requests to port ${PORT} will be redirected.`)
        );
} else
    require('http')
        .createServer(express_app)
        .listen(
            PORT,
            () => console.log(`Web application listening on port ${PORT}!\nWARNING: SSL is not being used.`)
        );

/**
 * Convenience function around `express_app.get` that catches and logs any errors.
 * @param route The route to handle get request for.
 * @param handler The handler that will handle the get requests.
 */
function get(route: string, handler: PageHandler): void {
    express_app.get(route, catch_errors(handler));
}

/**
 * Convenience function around `express_app.post` that catches and logs any errors.
 * @param route The route to handle post request for.
 * @param handler The handler that will handle the post requests.
 */
function post(route: string, handler: PageHandler): void {
    express_app.post(route, catch_errors(handler));
}
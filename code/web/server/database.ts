import { readFileSync } from "fs";
import pgp_module = require("pg-promise");

const PG_OPTIONS = {
    error(error: any, error_context: pgp_module.IEventContext) {
        if (error_context.cn) {
            console.error("Database connection error occured.");
            console.error(error);
            console.error(error_context);
        }
    }
};

const pgp = pgp_module(PG_OPTIONS);


const username: Buffer = readFileSync("/run/secrets/database_username");
const password: Buffer = readFileSync("/run/secrets/database_password");
const name: Buffer = readFileSync("/run/secrets/database_name");

export const database = pgp(`postgres://${username}:${password}@database:5432/${name}`);
export default database;

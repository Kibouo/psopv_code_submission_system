import { ensure_string, ensure_array, ensure_time, ensure_date, ensure_number, test_io_array_to_object } from "./utils";
import Assignment from "./assignment";
import { readFileSync } from "fs";
import { render_message, render_message_with_redirect } from "./render";
import DB from "./database";
import { Response, Request } from "express";
import { Course } from "./course";
import { User } from "./user";
import { all_platforms_ordered } from "./platform";
import { path } from "./router";

interface AssignmentFormInputs {
    title: any,
    description: any,
    platforms: any,
    deadline_date: any,
    deadline_time: any,
    max_compilation_time: any,
    max_test_time: any,
    example_test_inputs: any,
    example_test_outputs: any,
    final_test_inputs: any,
    final_test_outputs: any
}

/**
 * Renders the edit or create assignment page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {Course} course The course of the current assignment
 * @param {Assignment} assignment The assignment to edit.
 * @param {User} user The currently logged in user.
 * @param {{id: number, name: string}[]} platforms An array containing all available platforms.
 * @param {String[]} validation_errors Any errors that occurred.
 * @param {any} previous_input Object containing all the data to fill the input fields with
 */
export async function render_page(
    response: Response, course: Course, user: User, platforms: { id: number, name: string }[],
    validation_errors: string[], previous_input: any | null = null, assignment: Assignment | null = null
) {
    if (previous_input === null) {
        if (assignment === null) {
            previous_input = {
                title: "", description: "", platforms: platforms.map(platform => platform.id),
                deadline_date: "", deadline_time: "",
                max_compilation_time: "", max_test_time: "",
                example_tests: [{ input: "", output: "" }],
                final_tests: [{ input: "", output: "" }],
            };
        } else {
            const assignment_info = JSON.parse(await assignment.to_json());
            const example_tests = test_io_array_to_object(assignment_info.example_test_inputs, assignment_info.example_test_outputs);
            const final_tests = test_io_array_to_object(assignment_info.final_test_inputs, assignment_info.final_test_outputs);
            previous_input = {
                title: assignment_info.title, description: assignment_info.description, platforms: assignment_info.platforms,
                deadline_date: assignment_info.deadline_date, deadline_time: assignment_info.deadline_time,
                max_compilation_time: assignment_info.max_compilation_time, max_test_time: assignment_info.max_test_time,
                example_tests: example_tests,
                final_tests: final_tests,
            };
        }
    }
    const page_stack = assignment === null ? path.new_assignment(course) : path.edit_assignment(course, assignment);
    return response.render("create_assignment.pug", {
        header: { title: assignment === null ? "Create new assignment" : "Edit assignment" },
        navigation: { page_stack: page_stack },
        platforms: platforms,
        validation_errors,
        new_assignment: assignment === null,
        router: require("./router"),
        utils: require("./utils"),
        user,
        previous_input,
    });
}

export async function handle_submit(
    request: Request, response: Response, course: Course, user: User, assignment_to_update: Assignment | null = null
) {
    const is_new_assignment = assignment_to_update === null;

    const input_data =
        request.file && request.file.mimetype === 'application/json' ?
            JSON.parse(readFileSync(request.file.path).toString()) :
            request.body;

    const { data: input, errors } = validate_input(input_data);

    const platforms = all_platforms_ordered(DB); // lazy (Promise)

    if (request.body.create_btn && errors.length === 0) {

        const assignment = await update_assignment_database(course.id, <any>input, assignment_to_update);

        const succes_title = is_new_assignment ? "Assignment Added!" : "Assignment Updated!";
        const succes_message = is_new_assignment ? "The new assignment has been successfully added." : "The assignment has been successfully updated.";
        const assignment_path = path.assignment(await assignment.course(), assignment);

        render_message_with_redirect(
            response, user,
            succes_title,
            succes_message,
            assignment_path,
            "Go to the assignment.",
            assignment_path.url
        );
    } else if (request.body.create_btn)
        return render_page(response, course, user, await platforms, errors, input, assignment_to_update);
    else if (request.body.import_btn) {
        let errors = [];

        if (!request.file)
            errors.push("No import file was provided.");
        else if (request.file.mimetype !== 'application/json')
            errors.push("Provided import file is not valid.");

        return render_page(response, course, user, await platforms, errors, input, assignment_to_update);
    } else {
        // Indexing starts at 1. 0 means do not remove any test.
        const example_test_to_remove = ensure_number(request.body.remove_example_test);
        const final_test_to_remove = ensure_number(request.body.remove_final_test);

        if (request.body.add_example_test)
            input.example_tests.push({ input: "", output: "" });
        else if (request.body.add_final_test)
            input.final_tests.push({ input: "", output: "" });
        else if (example_test_to_remove > 0)
            input.example_tests.splice(example_test_to_remove - 1, 1);
        else if (final_test_to_remove > 0)
            input.final_tests.splice(final_test_to_remove - 1, 1);
        else
            errors.push("No button was pressed.");

        return render_page(response, course, user, await platforms, errors, input, assignment_to_update);
    }
}

function validate_input(input: AssignmentFormInputs) {
    const title = ensure_string(input.title);
    const description = ensure_string(input.description);
    const platforms = ensure_array(input.platforms).map(ensure_number);
    const deadline_date = ensure_date(input.deadline_date);
    const deadline_time = ensure_time(input.deadline_time);
    const example_test_inputs = ensure_array(input.example_test_inputs).map(ensure_string);
    const example_test_outputs = ensure_array(input.example_test_outputs).map(ensure_string);
    const final_test_inputs = ensure_array(input.final_test_inputs).map(ensure_string);
    const final_test_outputs = ensure_array(input.final_test_outputs).map(ensure_string);
    const max_compilation_time = ensure_number(input.max_compilation_time);
    const max_test_time = ensure_number(input.max_test_time);

    let errors = [];

    // TODO: Maybe move these constants somewhere else.
    if (!(title.length > 0 && title.length < 255))
        errors.push("The title must be between 0 and 256 characters long.");
    if (description.length > 5000)
        errors.push("The description must be no longer than 5000 characters.");
    if (platforms.length < 1)
        errors.push("At least one platform must be allowed.");
    if (!deadline_date)
        errors.push("A deadline date must be provided in the YYYY-MM-DD format.");
    if (!deadline_time)
        errors.push("A deadline time must be provided in the HH-MM format.");
    if (ensure_string(input.max_compilation_time) === "")
        errors.push("A maximum compilation time must be provided in the minutes format.");
    if (ensure_string(input.max_test_time) === "")
        errors.push("A maximum time per test must be provided in the minutes format.");
    if (max_compilation_time > 120)
        errors.push("The maximum compilation time is 120 minutes.");
    if (max_test_time > 120)
        errors.push("The maximum testing time is 120 minutes.");

    const too_long_test_exists = (<number[]>[])
        .concat(example_test_inputs.map(v => v.length))
        .concat(example_test_outputs.map(v => v.length))
        .concat(final_test_inputs.map(v => v.length))
        .concat(final_test_outputs.map(v => v.length))
        .filter(length => length > 5000)
        .length !== 0;

    if (too_long_test_exists)
        errors.push("The test and outputs inputs must each be no longer than 5000 characters.");
    if (example_test_inputs.length + final_test_inputs.length > 100)
        errors.push("The maximum of 100 tests per assignment has been exceeded.");

    const example_tests = test_io_array_to_object(example_test_inputs, example_test_outputs);
    const final_tests = test_io_array_to_object(final_test_inputs, final_test_outputs);

    // We do not validate platforms -at all-. If the user tampered with the object, the database
    // will simply refuse the insert, the page will crash and the assignment might be unusable.
    // In such a scenario, the user should still be able to delete the assignment manually.
    //
    // Take note though, this can only happen of the user tampered with the HTML.

    return {
        data: {
            title, description,
            deadline_date, deadline_time,
            max_compilation_time, max_test_time,
            example_tests, final_tests, platforms
        },
        errors,
    }
};

async function update_assignment_database(
    course_id: number,
    input: {
        title: string, description: string, platforms: number[],
        deadline_date: string, deadline_time: number, max_compilation_time: number, max_test_time: number,
        example_tests: { input: string, output: string }[],
        final_tests: { input: string, output: string }[]
    },
    assignment_to_update: Assignment | null
): Promise<Assignment> {
    if (assignment_to_update === null)
        return Assignment.create_new(
            course_id, input.title, input.description,
            `${input.deadline_date} ${input.deadline_time}`,
            input.max_compilation_time, input.max_test_time,
            input.example_tests, input.final_tests, input.platforms,
        );
    else return assignment_to_update.update_assignment(
        input.title, input.description,
        `${input.deadline_date} ${input.deadline_time}`,
        input.max_compilation_time, input.max_test_time,
        input.example_tests, input.final_tests, input.platforms,
    );
};

import DB from "./database";
import { Request, Response } from "express";
import { PromiseAdapter } from "pg-promise";
const router = require("./router");

import {
    hash,
    match_hash_string,
    get_new_token_expire_date,
    gen_password_reset_token,
    normalize_string
} from "./utils";
import { Submission } from "./submission";

const USER_DB_FIELDS: string = "id, name, name_normalized, password_hash, email, is_super_user";

interface UserDbRow {
    id: number,
    name: string,
    name_normalized: string,
    password_hash: string,
    email: string,
    is_super_user: boolean
};

export class User {
    readonly id: number;
    readonly name: string;
    readonly name_normalized: string;
    readonly email: string;

    private password_hash: string;
    private is_super_user: boolean;

    private constructor(
        id: number,
        name: string,
        name_normalized: string,
        password_hash: string,
        email: string,
        is_super_user: boolean
    ) {
        this.id = id;
        this.name = name;
        this.name_normalized = name_normalized;
        this.email = email;

        this.password_hash = password_hash;
        this.is_super_user = is_super_user;
    }

    private static from_db_row(row: UserDbRow): User {
        return new User(
            row.id, row.name, row.name_normalized,
            row.password_hash, row.email, row.is_super_user
        );
    }

    private static from_db_row_or_null(row: UserDbRow | null): User | null {
        if (row !== null)
            return User.from_db_row(row);
        else
            return null;
    }

    private static from_db_rows(rows: UserDbRow[]): User[] {
        return rows.map(User.from_db_row);
    }

    private static async all_users_of_course_with_type(
        course_id: number,
        user_type: string
    ): Promise<User[]> {
        return DB.any(
            `SELECT id ${USER_DB_FIELDS}
            FROM user_account
                INNER JOIN course_user_relation
                ON user_account.id=course_user_relation.user_id
             WHERE course_id=$(course_id) AND user_type=$(user_type)`,
            { course_id, user_type }
        ).then(User.from_db_rows);
    }

    /**
     * Returns a list of all students of a course.
     * @param course_id The course id.
     */
    static async all_students_of_course(course_id: number): Promise<User[]> {
        return User.all_users_of_course_with_type(course_id, "student");
    }

    /**
     * Returns a list of all teachers of a course.
     * @param course_id The course id.
     */
    static async all_teachers_of_course(course_id: number): Promise<User[]> {
        return User.all_users_of_course_with_type(course_id, "teacher");
    }

    /**
     * Gets a handle to a user with the specified email address. Returns null when
     * there is no such user.
     * @param email The email address of the user.
     */
    static async from_email(email: string): Promise<User | null> {
        return DB.oneOrNone(
            `SELECT ${USER_DB_FIELDS}
             FROM user_account
             WHERE email = $(email)`,
            { email }
        ).then(User.from_db_row_or_null);
    }

    /**
     * Gets a handle to a user with the specified id.
     * @param id The id of the user to get.
     */
    static async from_id(id: number): Promise<User | null> {
        return DB.oneOrNone(
            `SELECT ${USER_DB_FIELDS}
            FROM user_account
            WHERE id = $(id)`,
            { id }
        ).then(User.from_db_row_or_null);
    }

    /**
     * Gets a handle to the currently logged in user.
     * @param request The expressjs request instance.
     */
    static async from_current_session(request: Request): Promise<User | null> {
        const logged_in = typeof request.session !== "undefined"
            && typeof request.session.user_id === "number"
            && request.session.logged_in;

        if (request.session)
            if (logged_in)
                return User.from_id(request.session.user_id);
            else
                return null;
        else return Promise.reject("No session existed");
    }

    /**
     * Gets handles to all users that match the given pattern.
     *
     * @param query The pattern to find users for.
     */
    static async search(query: string): Promise<User[]> {
        const pattern = `%${query}%`;
        return DB.any(
            `SELECT ${USER_DB_FIELDS}
             FROM user_account
             WHERE name ILIKE $(pattern) OR email ILIKE $(pattern)`,
            { pattern }
        ).then(User.from_db_rows);
    }

    /**
     * Checks whether the user is a student of the requested course.
     *
     * @param {Number} course_id The id of the requested course
     */
    async is_student(course_id: number): Promise<boolean> {
        return DB.oneOrNone(
            `SELECT user_id
            FROM course_user_relation
            WHERE course_id=$(course_id)
                AND user_id=$(user_id)
                AND user_type='student'`,
            { course_id, user_id: this.id }
        ).then(data => data !== null);
    }

    /**
     * Checks whether the user is a teacher of the requested course.
     *
     * @param {Number} course_id The id of the requested course
     */
    async is_teacher(course_id: number): Promise<boolean> {
        return DB.oneOrNone(
            `SELECT user_id
            FROM course_user_relation
            WHERE course_id=$(course_id)
                AND user_id=$(user_id)
                AND user_type='teacher'`,
            { course_id, user_id: this.id }
        ).then(data => data !== null);
    }

    /**
     * Log this user in by saving their session and creating a cookie for them.
     * Then redirect them to the dashboard. Redirecting is done here to make sure the session is
     * properly started before redirecting. This is necessary as session.save doesn't return
     * something we can wait on. Waiting is necessary, as otherwise the user could be redirected to
     * the dashboard page before the session is terminated, which would redirect them a 2nd time
     * back to the login page.
     *
     * @param request The request that contains the session.
     * @param response The response used to redirect.
     */
    async login(request: Request, response: Response): Promise<void> {
        if (request.session) {
            request.session.logged_in = true;
            request.session.user_id = this.id;
            request.session.save(
                function (error) {
                    if (error) Promise.reject(error);
                    else Promise.resolve(response.redirect(router.path.dashboard().url));
                });
        }
        else return Promise.reject("Error login: no session.");
    }

    /**
     * Logs out the current user.
     * Then redirects them to the login page. Redirecting is done here to make sure the session is
     * properly terminated before redirecting. This is necessary as session.destroy doesn't return
     * something we can wait on. Waiting is necessary, as otherwise the user could be redirected to
     * the login page before the session is terminated, which would redirect them a 2nd time back
     * to the dashboard.
     *
     * @param request The request that contains the session.
     * @param response The response used to redirect.
     */
    async logout(request: Request, response: Response): Promise<void> {
        if (request.session)
            return request.session.destroy(
                function (error) {
                    if (error) Promise.reject(error);
                    else Promise.resolve(response.redirect(router.path.login().url));
                }
            );
        else return Promise.reject("Error login: no session.");
    }

    /**
     * Deletes all sessions of this user.
     */
    async prune_sessions(): Promise<null> {
        return DB.none(
            `DELETE FROM user_session
            WHERE sess::jsonb @> '{"user_id": $(user_id)}'`,
            { user_id: this.id }
        );
    }

    /**
     * Checks whether the new_password the user wants matches the reset token they
     * have set during the password reset request.
     *
     * @param new_password The password to check.
     */
    async new_password_correct(new_password: string): Promise<boolean> {
        return DB.oneOrNone(
            `SELECT new_password_hash
            FROM password_reset_token
            WHERE user_id = $(id)`,
            { id: this.id })
            .then(row => row.new_password_hash
                ? match_hash_string(row.new_password_hash, new_password)
                : false
            );
    }

    /**
     * Checks whether the provided reset token is bound to this user and is still valid.
     *
     * @param token The token to check for.
     * @param {Date} date The date object to compare the expiration date with.
     */
    async check_reset_token_valid(token: string, date: any) {
        return DB.oneOrNone(
            `SELECT hash, expire_date
            FROM password_reset_token
            WHERE user_id = $(id)`,
            { id: this.id }
        ).then(row => date && row
            ? match_hash_string(row.hash, token)
                .then((hash_valid: boolean) => hash_valid && (date < row.expire_date))
            : false
        );
    }


    /**
     * Set a password reset token for the current user and save the new password hash
     *
     * @param new_password The new password that the user wishes to use.
     */
    async set_reset_token(new_password: string) {
        const reset_token_expire_date = get_new_token_expire_date();
        const reset_token = gen_password_reset_token();

        return Promise.all([
            hash(reset_token),
            hash(new_password)
        ]).then((result: any) => {
            return DB.none(
                `INSERT INTO password_reset_token(user_id, hash, expire_date, new_password_hash)
                VALUES ($(user_id), $(hash), $(reset_token_expire_date), $(new_password_hash))
                ON CONFLICT (user_id) DO
                    UPDATE SET hash = $(hash),
                        expire_date = $(reset_token_expire_date),
                        new_password_hash = $(new_password_hash)`,
                {
                    user_id: this.id,
                    hash: result[0],
                    reset_token_expire_date,
                    new_password_hash: result[1]
                }
            ).then(_ => reset_token);
        });
    }

    /**
     * Update the password reset token's expiry date for this user.
     *
     * @param {Date} date The new date to update the password reset token's expiry date to.
     */
    async update_password_reset_expiry_date(date: Date): Promise<null> {
        return DB.none(
            `UPDATE password_reset_token
            SET expire_date = $(date)
            WHERE user_id = $(id)`,
            { date, id: this.id }
        );
    }

    /**
     * Update the user's password with the during reset request set new password.
     */
    async update_password(): Promise<null> {
        return DB.none(
            `UPDATE user_account
            SET password_hash =
                (
                    SELECT new_password_hash
                    FROM password_reset_token
                    WHERE user_id = $(id)
                )
            WHERE id = $(id)`,
            { id: this.id }
        );
    }

    /**
     * Updates the user's password directly with the given string.
     */
    async update_password_direct(new_password: string): Promise<null> {
        const new_password_hash = await hash(new_password);
        return DB.none(
            `UPDATE user_account
            SET password_hash = $(new_password_hash)
            WHERE id = $(id)`,
            { new_password_hash, id: this.id }
        );
    }

    /**
     * Updates the user's email directly with the given string.
     */
    async update_email(new_email: string): Promise<null> {
        return DB.none(
            "UPDATE user_account \
            SET email = ${new_email} \
            WHERE id = ${id}",
            { new_email: new_email, id: this.id }
        );
    }

    /**
     * Updates the user's name directly with the given string.
     */
    async update_name(new_name: string): Promise<null> {
        return DB.none(
            "UPDATE user_account SET \
            name = ${new_name}, \
            name_normalized = ${name_norm}\
            WHERE id = ${id}",
            { new_name: new_name, name_norm: normalize_string(new_name), id: this.id }
        );
    }


    /**
     * Checks whether a password matches this user's password.
     *
     * @param {String} password
     * The password to evaluate.
     */
    password_correct(password: string): Promise<boolean> {
        return match_hash_string(this.password_hash, password);
    }

    // TODO: Check permissions for superfluous superuser permissions.
    get can_access_superpanel(): boolean { return this.is_super_user; }
    get can_add_users(): boolean { return this.is_super_user; }
    get can_add_course(): boolean { return this.is_super_user; }
    get can_create_all_assignments(): boolean { return this.is_super_user; }
    get can_see_all_courses(): boolean { return this.is_super_user; }
    get can_edit_platforms(): boolean { return this.is_super_user; }
    get can_find_users(): boolean { return this.is_super_user; }
    get can_delete_users(): boolean { return this.is_super_user; }
    get can_force_reset_password(): boolean { return this.is_super_user; }

    can_delete_course() { return this.is_super_user; }

    can_edit_user_info(): boolean { return this.is_super_user; }

    can_access_profile(target_user_id: number) {
        return target_user_id === this.id
            || this.is_super_user;
    }


    can_reset_password_of(target_user_id: number) {
        return target_user_id === this.id
            || this.is_super_user;
    }

    async can_edit_course(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_create_assignment(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_delete_assignment(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_edit_assignment(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_access_course(course_id: number): Promise<boolean> {
        return Promise.all(
            [
                this.is_teacher(course_id),
                this.is_student(course_id)
            ]
        ).then(result => result[0] || result[1] || this.is_super_user);
    }

    async can_see_final_tests(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async see_assignment_as_teacher(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async see_assignment_as_student(course_id: number): Promise<boolean> {
        return this.is_student(course_id);
    }

    async can_see_submission_details(submission: Submission): Promise<boolean> {
        const assignment = await submission.assignment();
        const course = await assignment.course();

        return this.is_super_user
            || await this.is_student(course.id)
            || await this.is_student(course.id);
    }

    async can_see_history(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_manage_students(course_id: number): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(course_id);
    }

    async can_manage_teachers(course_id: number): Promise<boolean> {
        return this.is_super_user;
    }

    async can_create_submission(course_id: number): Promise<boolean> {
        return this.is_student(course_id);
    }

    async can_delete_submission(submission: Submission): Promise<boolean> {
        return this.is_super_user || await this.is_teacher(
            (await submission.assignment()).course_id
        );
    }
}

/**
 * Creates a new user from its name and email address.
 * @param name The name of the new user.
 * @param email The email address of the new user. Must not already be used by another user.
 */
export async function create_new_user(name: string, email: string): Promise<null> {
    return DB.none(
        `INSERT INTO user_account (name, name_normalized, email)
             VALUES ($(name), $(name_normalized), $(email))`,
        { name, email, name_normalized: normalize_string(name) }
    );
}

export async function delete_user(user: User): Promise<null> {
    return DB.none(
        "DELETE FROM user_account \
        WHERE id=${user_id}",
        { user_id: user.id }
    );
}

export async function someone_has_email(email: string): Promise<boolean> {
    return DB.oneOrNone(
        `SELECT id
        FROM user_account
        WHERE email = $(email)`,
        { email }
    ).then(result => result !== null);
}

export default User;
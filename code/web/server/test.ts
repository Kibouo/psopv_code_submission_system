import DB from "./database";

const TEST_DB_FIELDS: string = "test.id, test.type, test.input, test.output, test.assignment_id";

interface TestDbRow {
    id: number;
    type: string;
    input: string;
    output: string;
    assignment_id: number;
};

export enum TestType {
    Final, Example
}

export class Test {
    id: number;
    type: TestType;
    input: string;
    output: string;
    assignment_id: number;

    private constructor(
        id: number,
        type: TestType,
        input: string,
        output: string,
        assignment_id: number
    ) {
        this.id = id;
        this.type = type;
        this.input = input;
        this.output = output;
        this.assignment_id = assignment_id;
    }

    /**
     * @internal
     */
    static from_db_row(row: TestDbRow): Test {
        console.assert(row.type === "example" || row.type === "final");
        const test_type = row.type === "example" ? TestType.Example : TestType.Final;
        return new Test(row.id, test_type, row.input, row.output, row.assignment_id);
    }

    private static from_db_rows(rows: TestDbRow[]): Test[] {
        return rows.map(Test.from_db_row);
    }

    /**
     * Adds a new test to an assignment.
     * @param assignment_id The id of the assignment to add a test to.
     * @param input The input of the test.
     * @param output The output of the test.
     * @param test_type The type of the test. Either "example" or "final".
     * @returns The newly created Test.
     */
    private static async create_new(assignment_id: number, input: string, output: string, test_type: string): Promise<Test> {
        console.assert(test_type === "example" || test_type === "final");

        return DB.one(
            `
                INSERT INTO test (type, input, output, assignment_id)
                VALUES ($(test_type), $(input), $(output), $(assignment_id))
                RETURNING ${TEST_DB_FIELDS}
            `,
            { test_type, input, output, assignment_id }
        ).then(Test.from_db_row);
    }

    /**
     * Adds a new example test to an assignment.
     * @param assignment_id The id of the assignment to add a test to.
     * @param input The input of the test.
     * @param output The output of the test.
     * @returns The newly created Test.
     */
    static async create_new_example(assignment_id: number, input: string, output: string): Promise<Test> {
        return Test.create_new(assignment_id, input, output, "example");
    }

    /**
     * Adds a new final test to an assignment.
     * @param assignment_id The id of the assignment to add a test to.
     * @param input The input of the test.
     * @param output The output of the test.
     * @returns The newly created Test.
     */
    static async create_new_final(assignment_id: number, input: string, output: string): Promise<Test> {
        return Test.create_new(assignment_id, input, output, "final");
    }

    /**
     * Gets all example tests of a specific assignment.
     *
     * @param assignment_id The id of the assignment from which all example tests are requested.
     * @return A promise resolving to an array of all example tests for the requested assignment.
     */
    static async all_example_from_assignment_id(assignment_id: number): Promise<Test[]> {
        return DB.manyOrNone(
            `
                SELECT ${TEST_DB_FIELDS}
                FROM test
                WHERE type = 'example' AND assignment_id = $(assignment_id)
            `,
            { assignment_id }
        ).then(Test.from_db_rows);
    }

    /**
     * Gets all final tests of a specific assignment.
     *
     * @param assignment_id The id of the assignment from which all final tests are requested.
     * @return A promise resolving to an array of all final tests for the requested assignment.
     */
    static async all_final_from_assignment_id(assignment_id: number): Promise<Test[]> {
        return DB.manyOrNone(
            `
                SELECT ${TEST_DB_FIELDS}
                FROM test
                WHERE type = 'final' AND assignment_id = $(assignment_id)
            `,
            { assignment_id }
        ).then(Test.from_db_rows);
    }
}

const TEST_RESULT_DB_FIELDS: string =
    `
        test_result.test_id, test_result.submission_id, test_result.is_success,
        test_result.stdout, test_result.stderr
    `;

interface TestResultDbRow {
    test_id: number;
    submission_id: number;
    is_success: boolean;
    stdout: string;
    stderr: string;
};

export class TestResult {
    readonly test_id: number;
    readonly submission_id: number;
    readonly is_success: boolean;
    readonly stdout: string;
    readonly stderr: string;

    private constructor(
        test_id: number,
        submission_id: number,
        is_success: boolean,
        stdout: string,
        stderr: string
    ) {
        this.test_id = test_id;
        this.submission_id = submission_id;
        this.is_success = is_success;
        this.stdout = stdout;
        this.stderr = stderr;
    }

    /**
     * @internal
     */
    static from_db_row(row: TestResultDbRow): TestResult {
        return new TestResult(
            row.test_id, row.submission_id, row.is_success,
            row.stdout, row.stderr
        );
    }
}

export function fetch_tests_results_from_submission(
    submission_id: number
): Promise<{ test: Test, result: TestResult }[]> {
    return DB.any(
        `
            SELECT ${TEST_DB_FIELDS}, ${TEST_RESULT_DB_FIELDS}
            FROM test_result INNER JOIN test
                ON test.id=test_result.test_id
            WHERE submission_id=$(submission_id)
        `,
        { submission_id }
    ).then(test_results => test_results.map(row => ({
        test: Test.from_db_row(row),
        result: TestResult.from_db_row(row)
    })));
}

const PLATFORM_MODULE = require("./platform.js");
import Course from "./course";
import { normalize_string } from "./utils";
import { Submission } from "./submission";
import DB from "./database";
import User from "./user";
const UTILS = require("./utils");
import { Test } from "./test";

const ASSIGNMENT_DB_FIELDS: string =
    `
        assignment.id,
        assignment.name,
        assignment.name_normalized,
        assignment.description,
        EXTRACT(epoch FROM assignment.deadline) AS deadline,
        EXTRACT(epoch FROM assignment.compile_time_max)/60 AS compile_time_max,
        EXTRACT(epoch FROM assignment.run_time_max)/60 AS run_time_max,
        assignment.course_id
    `;

interface AssignmentDbRow {
    id: number,
    name: string,
    name_normalized: string,
    description: string,
    deadline: number,
    compile_time_max: number,
    run_time_max: number,
    course_id: number
};

interface RawTest {
    input: string,
    output: string
}

export class Assignment {
    readonly id: number;
    readonly course_id: number;
    name: string;
    name_normalized: string;
    description: string;
    deadline: Date;
    deadline_unix: number;
    compile_time_max: number;
    run_time_max: number;

    private constructor(
        id: number,
        name: string,
        name_normalized: string,
        description: string,
        deadline: number,
        compile_time_max: number,
        run_time_max: number,
        course_id: number
    ) {

        this.id = id;
        this.name = name;
        this.name_normalized = name_normalized;
        this.description = description;
        this.deadline_unix = deadline;
        this.compile_time_max = Math.round(compile_time_max);
        this.run_time_max = Math.round(run_time_max);
        this.course_id = course_id;
        this.deadline = new Date(this.deadline_unix * 1000);
    }

    private static from_db_row(row: AssignmentDbRow): Assignment {
        return new Assignment(
            row.id, row.name, row.name_normalized, row.description, row.deadline,
            row.compile_time_max, row.run_time_max, row.course_id
        );
    }

    private static from_db_row_or_null(row: AssignmentDbRow | null): Assignment | null {
        return row ? Assignment.from_db_row(row) : null;
    }

    private static from_db_rows(rows: AssignmentDbRow[]): Assignment[] {
        return rows.map(Assignment.from_db_row);
    }

    /**
     * Gets all assignments from a given course, in reverse chronological order.
     *
     * @param course_id The id of the course for who we request data.
     * @return A promise resolving to an array of Assignment objects.
     */
    static async all_from_course(course_id: number): Promise<Assignment[]> {
        return DB.any(
            `SELECT ${ASSIGNMENT_DB_FIELDS}
            FROM assignment
            WHERE course_id=$(course_id)
            ORDER BY deadline DESC`,
            { course_id }
        ).then(Assignment.from_db_rows);
    }

    /**
    * Gets the assignment given an assignment's id.
    *
    * @param assignment_id
    * The id of the requested assignment.
    *
    * @return
    * A promise which resolves to an Assignment object if an entry is found, or null if no entry is
    * found.
    */
    static async from_id(id: number): Promise<Assignment | null> {
        return DB.oneOrNone(
            `SELECT ${ASSIGNMENT_DB_FIELDS} FROM assignment WHERE id = $(id)`,
            { id }
        ).then(Assignment.from_db_row_or_null);
    }

    /**
     * Gets n assignments which are coming up next (chronologically) for a given user.
     *
     * @param user_id The id of the user for which we request data.
     * @param count The amount of assignments to get.
     * @return A promise resolving to an array of max n Assignment objects.
     */
    static async upcoming_for_user(user_id: number, count: number): Promise<Assignment[]> {
        return DB.manyOrNone(
            `
                SELECT ${ASSIGNMENT_DB_FIELDS}
                FROM assignment JOIN course_user_relation
                    ON assignment.course_id=course_user_relation.course_id
                WHERE course_user_relation.user_id=$(user_id) AND deadline > now()::timestamp
                ORDER BY deadline ASC
                LIMIT $(count)
            `,
            { user_id, count }
        ).then(Assignment.from_db_rows);
    }

    /**
     * Gets n assignments which are coming up next (chronologically)
     * for a given user.
     *
     * @param user_id The id of the user for who we request data.
     * @param rows_to_get The amount of assignments to get.
     * @return A promise resolving to an array of max n Assignment objects.
     */
    static async next_n_from_user_id(id: number, rows_to_get: number): Promise<Assignment[]> {
        return DB.manyOrNone(
            `
                SELECT ${ASSIGNMENT_DB_FIELDS}
                FROM assignment
                    INNER JOIN course
                        INNER JOIN course_user_relation
                        ON course_user_relation.course_id = course.id
                    ON course.id = assignment.course_id
                WHERE (course_user_relation.user_type = 'student'
                        OR course_user_relation.user_type = 'teacher')
                    AND course_user_relation.user_id = $(id)
                    AND deadline > now()::timestamp
                ORDER BY deadline ASC
                LIMIT $(rows_to_get)
            `,
            { id, rows_to_get }
        ).then(Assignment.from_db_rows);
    }

    /**
     * Adds example tests to this assignment.
     * @param example_tests The example tests to add to this assignment.
     */
    private async add_example_tests(example_tests: RawTest[]): Promise<Test[]> {
        return Promise.all(example_tests.map(
            ({ input, output }) => Test.create_new_example(this.id, input, output)
        ));
    }

    /**
     * Adds final tests to this assignment.
     * @param final_tests The final tests to add to this assignment.
     */
    private async add_final_tests(final_tests: RawTest[]): Promise<Test[]> {
        return Promise.all(final_tests.map(
            ({ input, output }) => Test.create_new_final(this.id, input, output)
        ));
    }

    static async create_new(
        course_id: number,
        name: string,
        description: string,
        deadline: string,
        compile_time_max: number,
        run_time_max: number,
        example_tests: RawTest[],
        final_tests: RawTest[],
        platform_ids: number[]
    ): Promise<Assignment> {
        const assignment = await DB.one(
            `
                INSERT INTO assignment(
                    name, name_normalized, description, deadline,
                    compile_time_max,
                    run_time_max,
                    course_id
                )
                VALUES (
                    $(name), $(name_normalized), $(description), (TIMESTAMP $(deadline)),
                    concat ($(compile_time_max_), ' minutes')::interval,
                    concat ($(run_time_max_), ' minutes')::interval,
                    $(course_id)
                )
                RETURNING ${ASSIGNMENT_DB_FIELDS}
            `,
            {
                name, name_normalized: normalize_string(name), description,
                deadline, compile_time_max_: compile_time_max, run_time_max_: run_time_max, course_id
            }
        ).then(Assignment.from_db_row);


        await Promise.all([
            assignment.add_example_tests(example_tests),
            assignment.add_final_tests(final_tests),
            assignment.add_platforms(platform_ids)
        ]);

        await assignment.mark_ready_for_testing();

        return assignment;
    }

    /**
     * Returns an array of all tests this assignment has, both example and finals.
     */
    async get_all_tests(): Promise<{ example: Test[], final: Test[] }> {
        const [example, final] = await Promise.all([
            Test.all_example_from_assignment_id(this.id),
            Test.all_final_from_assignment_id(this.id),
        ]);

        return { example, final };
    }

    /**
     * Marks this assignment as ready for testing.
     */
    private async mark_ready_for_testing() {
        await DB.none(`
            UPDATE assignment SET tests_platforms_inserted = true WHERE id = $(id)`,
            { id: this.id }
        );
    }

    /**
     * Updates this assignment's tests.
     * @param examples All example tests of this assignment.
     * @param finals All final tests of this assignment.
     * @returns Whether any tests were actually updated. If so, the submissions must be re-evaluated.
     */
    private async update_tests(examples: RawTest[], finals: RawTest[]): Promise<boolean> {
        const { example: existing_examples, final: existing_finals } = await this.get_all_tests();

        let should_update = false;
        if (examples.length != existing_examples.length || finals.length != existing_finals.length)
            should_update = true;
        else {
            for (let index = 0; !should_update && index < examples.length; index += 1)
                if (examples[index].input !== existing_examples[index].input
                    || examples[index].output !== existing_examples[index].output)
                    should_update = true;
            for (let index = 0; !should_update && index < finals.length; index += 1)
                if (finals[index].input !== existing_finals[index].input
                    || finals[index].output !== existing_finals[index].output)
                    should_update = true;
        }

        // Only update if they actually changed.
        if (should_update) {
            const delete_tests = DB.none(
                `DELETE FROM test WHERE assignment_id=$(assignment_id)`,
                { assignment_id: this.id }
            );
            const add_example_tests = this.add_example_tests(examples);
            const add_final_tests = this.add_final_tests(finals);
            await Promise.all([delete_tests, add_example_tests, add_final_tests]);
        }

        return should_update;
    }

    private async update_platforms(new_platforms: number[]) {
        await DB.none(
            `DELETE FROM assignment_platform_relation WHERE
            assignment_id=$(assignment_id)`,
            { assignment_id: this.id }
        );

        await this.add_platforms(new_platforms);
    }

    async update_assignment(
        name: string,
        description: string,
        deadline: string,
        compile_time_max: number,
        run_time_max: number,
        example_tests: RawTest[],
        final_tests: RawTest[],
        platform_ids: number[]
    ): Promise<Assignment> {
        const compile_time_max_interval = `${compile_time_max}  minute`;
        const run_time_max_interval = `${run_time_max}  minute`;
        // update all assignment info
        const assignment_promise = DB.one(
            `UPDATE assignment SET
                name=$(name),
                name_normalized=$(name_normalized),
                description=$(description),
                deadline=(TIMESTAMP $(deadline)),
                compile_time_max=$(compile_time_max)::interval,
                run_time_max=$(run_time_max)::interval,
                tests_platforms_inserted=false
            WHERE id=$(assignment_id)
            RETURNING ${ASSIGNMENT_DB_FIELDS}`,
            {
                name: name, name_normalized: UTILS.normalize_string(name), description: description, deadline: deadline,
                compile_time_max: compile_time_max_interval, run_time_max: run_time_max_interval, assignment_id: this.id
            }
        ).then(Assignment.from_db_row);

        const rerun_tests = compile_time_max !== this.compile_time_max || run_time_max !== this.run_time_max;

        const [new_assignment] = await Promise.all([
            assignment_promise,
            this.update_tests(example_tests, final_tests)
                .then(did_update_tests => rerun_tests || did_update_tests
                    ? DB.none(
                        `
                            UPDATE submission SET
                                pipeline_epoch=pipeline_epoch+1,
                                pipeline_status='wait'
                            WHERE assignment_id=$(id)
                        `,
                        { id: this.id }
                    )
                    : Promise.resolve()),
            this.update_platforms(platform_ids)
        ]);

        await new_assignment.mark_ready_for_testing();

        return new_assignment;
    }

    private cached_platforms: Promise<any> | null = null;
    /**
     * Fetches a list of all allowed platforms on this assignment.
     *
     * @returns
     * A promise resolving into an array of platforms.
     */
    async platforms(): Promise<any[]> {
        if (!this.cached_platforms)
            this.cached_platforms = PLATFORM_MODULE.from_assignment_id(DB, this.id);
        return this.cached_platforms;
    }

    private cached_course: Promise<Course> | null = null;
    /**
     * Fetches a handle to the course this assignment is a part of.
     *
     * @returns
     * A handle to the course.
     */
    async course(): Promise<Course> {
        if (!this.cached_course)
            this.cached_course = <Promise<Course>>Course.from_id(this.course_id);
        return this.cached_course;
    }

    /**
     * Gets the most recent submission of a given user for the current assignment
     *
     * @param user
     * The user to look for a submission for.
     *
     * @returns
     * A Promise containing a handle to the most recent submission of the user,
     * or null when no submission was found for the given user.
     */
    async latest_submission_by_user(user: User): Promise<{} | null> {
        const submissions =
            await Submission.all_recent_by_student_for_assignment(user.id, this.id, 1);

        return submissions.length > 0 ? submissions[0] : null;
    }

    /**
     * Adds a test to this assignment.
     *
     * @param input The input of the test to be added.
     * @param output The output of the test to be added.
     * @param test_type The type of the test to be added (either example or final).
     * @returns A promise resolving into null, and rejecting into a database error.
     */
    async add_test(input: string, output: string, test_type: string): Promise<null> {
        return DB.none(
            `
                INSERT INTO test (type, input, output, assignment_id)
                VALUES ($(test_type), $(input), $(output), $(assignment_id))
            `,
            { test_type, input, output, assignment_id: this.id }
        );
    }

    /**
     * Adds an allowed platform to this assignment.
     *
     * @param platform_id The id of the platform to add to this assignment.
     * @returns A promise resolving into null.
     */
    private async add_platform(platform_id: number): Promise<null> {
        return DB.none(
            `
                INSERT INTO assignment_platform_relation (assignment_id, platform_id)
                VALUES ($(assignment_id), $(platform_id))
            `,
            { assignment_id: this.id, platform_id }
        );
    }

    /**
     * Adds allowed platforms to this assignment, in bulk.
     *
     * @param platform_ids The ids of the platforms to add to this assignment.
     * @returns A promise resolving into null.
     */
    private async add_platforms(platform_ids: number[]): Promise<null> {
        await Promise.all(platform_ids.map(id => this.add_platform(id)));
        return null;
    }

    /**
     * Checks if a platform is allowed on this assignment.
     *
     * @param platform_id The id of the platform to test.
     *
     * @returns A promise resolving to whether the platform is allowed.
     */
    async platform_is_allowed(platform_id: number): Promise<boolean> {
        return DB.oneOrNone(
            `
                SELECT platform_id FROM assignment_platform_relation
                WHERE assignment_id = $(assignment_id) AND platform_id = $(platform_id)
            `,
            { assignment_id: this.id, platform_id }
        ).then(platform_id => platform_id !== null);
    }

    async to_json(): Promise<string> {
        const [platform_objs,
            example_tests,
            final_tests] =
            await Promise.all([
                this.platforms(),
                Test.all_example_from_assignment_id(this.id),
                Test.all_final_from_assignment_id(this.id)
            ]);

        const platforms = platform_objs.map(platform => platform.id);
        const example_test_inputs = example_tests.map(test => test.input);
        const example_test_outputs = example_tests.map(test => test.output);
        const final_test_inputs = final_tests.map(test => test.input);
        const final_test_outputs = final_tests.map(test => test.output);

        const export_obj = {
            title: this.name,
            description: this.description,
            platforms: platforms,
            deadline_date: UTILS.date_to_strings(this.deadline).utc_iso,
            deadline_time: UTILS.date_to_strings(this.deadline).utc_hour_min,
            max_compilation_time: String(this.compile_time_max),
            max_test_time: String(this.run_time_max),
            example_test_inputs: example_test_inputs,
            example_test_outputs: example_test_outputs,
            final_test_inputs: final_test_inputs,
            final_test_outputs: final_test_outputs,
        }
        return JSON.stringify(export_obj);
    }

    async fetch_example_test_count(): Promise<number> {
        return DB.one(
            `SELECT count(*) count FROM test WHERE assignment_id=$(id) AND type='example'`,
            { id: this.id }
        ).then(({ count }) => Number(count));
    }

    async fetch_final_test_count(): Promise<number> {
        return DB.one(
            `SELECT count(*) count FROM test WHERE assignment_id=$(id) AND type='final'`,
            { id: this.id }
        ).then(({ count }) => Number(count));
    }

    /**
     * Gets whether the deadline on this assignment has expired.
     */
    deadline_expired(): boolean {
        return (this.deadline.getTime() - new Date().getTime()) < 0;
    }
}

export function delete_from_db(assignment: Assignment): Promise<null> {
    return DB.none(
        "DELETE FROM assignment \
        WHERE id=${assignment_id}",
        { assignment_id: assignment.id });
}

export default Assignment;
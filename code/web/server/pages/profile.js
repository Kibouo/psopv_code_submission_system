const { User } = require("../user");
const router = require("../router");
const { Course } = require("../course");
import { ensure_string, ensure_array, ensure_time, ensure_date, ensure_number, test_io_array_to_object } from "../utils";
const EMAIL_QUEUE = require("../email_queue.js");
import { render_message } from "../render";

export async function handle_page(request, response) {
    return User.from_current_session(request).then(user => {
        User.from_id(request.params.user_id)
            .then(target_user => {
                // If user logged in
                if (user) {
                    if (!target_user)
                        return response.sendStatus(404);
                    if (user.can_access_profile(target_user.id))
                        return handle_page_inner(request, response, user, target_user);
                    else
                        return response.sendStatus(403);
                }
                // Redirect to login
                else
                    response.redirect(router.path.login().url);
            });
    });
}

async function handle_page_inner(
    request,
    response,
    user,
    target_user
) {

    if (request.method === "POST") {

        // user logs out
        if (request.body.logout_button) {
            if (user.id === target_user.id)
                return user.logout(request, response);

            return render_page(response, user, target_user, ["You are not allowed to do this."]);
        }

        if (request.body.prune_sessions) {
            if (user.id === target_user.id) {
                await user.prune_sessions();
                return response.redirect(router.path.login().url);
            }
     
            return render_page(response, user, target_user, ["You are not allowed to do this."]);
        }


        // user tries to change password
        if (request.body.password_reset) {

            let reset_errs = [];
            if (typeof request.body.new_password !== "string")
                reset_errs.push("new password is a required field.");

            if (typeof request.body.new_password_repeat !== "string")
                reset_errs.push("new password is a required field.");

            if (!user.can_reset_password_of(target_user.id))
                return render_page(response, user, target_user, ["You are not allowed to do this."]);

            if (request.body.new_password.length < 8 || request.body.new_password_repeat.length < 8)
                reset_errs.push("The provided password is too short.");

            if (request.body.new_password !== request.body.new_password_repeat)
                reset_errs.push("The new passwords do not match.");

            if (!user.can_force_reset_password && typeof request.body.old_password !== "string")
                reset_errs.push("The provided current password is not correct.");

            if (!user.can_force_reset_password && !await user.password_correct(request.body.old_password))
                reset_errs.push("The provided current password is not correct.");

            if (reset_errs.length > 0)
                return render_page(response, user, target_user, reset_errs);

            await Promise.all([
                target_user.update_password_direct(request.body.new_password),
                EMAIL_QUEUE.add_password_reset_done_email(target_user.id)
            ]);

            let message;
            if (user.can_force_reset_password)
                message = "The password has been changed successfully."
            else
                message = "Your password has been changed and is usable the next time you log in.";

            return render_message(
                response, user,
                "Password changed.",
                message,
                router.path.profile_page(target_user),
            );

        }

        // admin tries editing user info
        if (request.body.user_edit) {

            let reset_errs = [];
            const email = ensure_string(request.body.new_email);
            const name = ensure_string(request.body.new_name);

            if (!(email.length > 0 && email.length < 255))
                reset_errs.push("The email must be between 0 and 256 characters long.");

            if (!(name.length > 0 && name.length < 255))
                reset_errs.push("The name must be between 0 and 256 characters long.");

            if (reset_errs.length > 0)
                return render_page(response, user, target_user, reset_errs);

            if (user.can_edit_user_info()) {

                const old_name = target_user.name;
                const old_email = target_user.email;
                await Promise.all([
                    target_user.update_email(email),
                    target_user.update_name(name)
                ]);

                const edited_user = await User.from_id(target_user.id);
                return render_message(
                    response, user,
                    "User info changed.",
                    "The user info for " + old_name + "<" + old_email + "> has been changed successfully to: " + edited_user.name + "<" + edited_user.email + ">",
                    router.path.profile_page(edited_user),
                );

            }


        }

    }

    // no data posted
    else
        return render_page(response, user, target_user, []);
}

async function render_page(
    response,
    user,
    target_user,
    errors
) {
    const [student_courses, teacher_courses] =
        await Promise.all([
            Course.all_courses_where_student(target_user.id),
            Course.all_courses_where_teacher(target_user.id),
        ]);

    return response.render("profile.pug", {
        header: {
            title: "Profile page"
        },
        navigation: {
            page_stack: router.path.profile_page(target_user),
        },
        profile_page: {
            target_user: target_user,
            student_courses: student_courses,
            teacher_courses: teacher_courses,
        },
        errors: errors,
        router: router,
        utils: require("../utils"),
        user: user,
    });
}

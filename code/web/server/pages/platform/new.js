const { User } = require("../../user");
const router = require("../../router");
const UTILS = require("../../utils");
const PLATFORM_MODULE = require("../../platform.js");
import DB from "../../database";
import { render_message_with_redirect } from "../../render";

export async function handle_page(request, response) {
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(router.path.login().url);
    else if (!user.can_edit_platforms)
        response.sendStatus(403);
    else if (!request.body.submit)
        render_page(response, user);
    else {
        const { errors, safe_inputs } = PLATFORM_MODULE.validate_inputs(request.body);

        {
            // Check for duplicate name & id.
            const [name_collision, id_collision] = await Promise.all([
                PLATFORM_MODULE.from_name(DB, safe_inputs.name),
                PLATFORM_MODULE.from_id(DB, safe_inputs.platform_id)
            ]);

            if (name_collision)
                errors.push("This name is already used by another platform.");
            if (id_collision)
                errors.push("The unique identifier is already used by another platform.");
        }

        if (errors.length !== 0)
            return render_page(response, user, request.body, errors);
        else {
            const new_platform = await PLATFORM_MODULE.create_new(
                DB, safe_inputs.platform_id, safe_inputs.name, safe_inputs.description
            );

            render_success_page(response, user, new_platform);
        }
    }
}

async function render_page(
    response, user,
    previous_input = { platform_id: "", name: "", description: "" },
    parse_errors = [],
    confirmation_required = false,
) {
    response.render("platform/new.pug", {
        header: {
            title: "Add platform"
        },
        navigation: {
            page_stack: router.path.super_add_platform(),
        },
        new_platform: {
            previous_input: previous_input,
            parse_errors: parse_errors,
        },
        utils: UTILS,
        router: router,
        user: user,
    });
}

async function render_success_page(response, user, new_platform) {
    render_message_with_redirect(
        response,
        user,
        "Platform added!",
        "You have successfully created a new platform.",
        router.path.super_add_platform(),
        "Go to the new platform.",
        router.path.platform(new_platform).url
    );
}
const PLATFORM_MODULE = require("../../platform.js");
import DB from "../../database";
import { ensure_number } from "../../utils";
import { path } from "../../router";
import { User } from "../../user";
import { Request, Response } from "express";

export async function handle_page(request: Request, response: Response) {
    const [user, platform] = await Promise.all([
        User.from_current_session(request),
        PLATFORM_MODULE.from_id(DB, request.params.platform_id)]
    );

    if (!user)
        response.redirect(path.login().url);
    else if (!user.can_edit_platforms)
        response.sendStatus(403);
    else if (!platform)
        response.sendStatus(404);
    else if (!request.body.save_edits)
        render_page(response, user, platform);
    else {
        const { errors, safe_inputs } = PLATFORM_MODULE.validate_inputs(request.body);
        const changing_name = errors.length === 0 && platform.name !== safe_inputs.name;
        const changing_id = errors.length === 0 && platform.id !== safe_inputs.platform_id;

        // Check for duplicate name & id.
        if (changing_name && await PLATFORM_MODULE.from_name(DB, safe_inputs.name))
            errors.push("This name is already used by another platform.");
        if (changing_id && await PLATFORM_MODULE.from_id(DB, safe_inputs.platform_id))
            errors.push("The unique identifier is already used by another platform.");

        if (
            errors.length === 0 // reverify because errors might have been added
            && changing_id
            && ensure_number(request.body.ignore_warning) !== safe_inputs.platform_id
        )
            return render_page(response, user, platform, request.body, errors, true);
        else if (errors.length !== 0)
            return render_page(response, user, platform, request.body, errors);
        else {
            await platform.update(
                safe_inputs.platform_id, safe_inputs.name, safe_inputs.description
            );
            return render_page(response, user, platform, request.body, [], false, true);
        }
    }
}

async function render_page(
    response: Response, user: User, platform: any,
    previous_input: null | { platform_id: number, name: string, description: string } = null,
    parse_errors = [],
    show_change_id_warning = false,
    did_perform_update = false
) {
    if (!previous_input)
        previous_input = {
            platform_id: platform.id,
            name: platform.name,
            description: await platform.fetch_description()
        };

    response.render("platform/edit.pug", {
        header: {
            title: "Edit platform"
        },
        navigation: {
            page_stack: path.platform_edit(platform),
        },
        edit_platform: {
            previous_input: previous_input,
            parse_errors: parse_errors,
            show_change_id_warning,
            did_perform_update,
            submit_url: path.platform_edit(platform).url
        },
        utils: require("../../utils"),
        router: require("../../router"),
        user: user,
    });
}

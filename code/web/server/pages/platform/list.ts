const PLATFORM_MODULE = require("../../platform.js");
import DB from "../../database";
import { path } from "../../router";
import { Request, Response } from "express";
import { User } from "../../user";

export async function handle_page(request: Request, response: Response) {
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(path.login().url);
    else
        render_page(response, user, await PLATFORM_MODULE.all_platforms_ordered(DB));
}

function render_page(response: Response, user: User, platforms: any[]) {
    response.render("platform/list.pug", {
        header: { title: "Platform List" },
        navigation: { page_stack: path.platform_list() },
        platform_list: {
            platforms,
            can_edit: user.can_edit_platforms,
        },
        utils: require("../../utils"),
        router: require("../../router"),
        user: user,
    });
}
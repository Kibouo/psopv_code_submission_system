import { Response, Request } from "express";
import { User } from "../../user";
import { path } from "../../router";
import DB from "../../database";
const PLATFORM_MODULE = require("../../platform.js");

export async function handle_page(request: Request, response: Response) {
    const [user, platform] = await Promise.all([
        User.from_current_session(request),
        PLATFORM_MODULE.from_id(DB, request.params.platform_id)]
    );

    if (!user)
        response.redirect(path.login().url);
    else if (!platform)
        response.sendStatus(404);
    else
        return render_page(response, user, platform);
}

async function render_page(response: Response, user: User, platform: any) {
    response.render("platform/info.pug", {
        header: {
            title: platform.name,
        },
        navigation: {
            page_stack: path.platform(platform),
        },
        show_platform: {
            name: platform.name,
            description: await platform.fetch_description(),
            can_edit: user.can_edit_platforms,
            edit_url: path.platform_edit(platform).url,
            delete_url: path.platform_delete(platform).url,
        },
        utils: require("../../utils"),
        router: require("../../router"),
        user: user
    });
}
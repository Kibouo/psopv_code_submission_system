const { User } = require("../../user");
const PLATFORM_MODULE = require("../../platform.js");
import { Request, Response } from "express";
import DB from "../../database";
import { path } from "../../router";
import { render_message_with_redirect } from "../../render";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const [user, platform] = await Promise.all([
        User.from_current_session(request),
        PLATFORM_MODULE.from_id(DB, request.params.platform_id)
    ]);

    if (!user)
        response.redirect(path.login().url);
    else if (!user.can_edit_platforms)
        response.sendStatus(403);
    else if (!platform)
        response.sendStatus(404);
    else if (!request.body.delete) {
        response.render("platform/delete.pug", {
            header: { title: "Delete Platform" },
            navigation: { page_stack: path.platform_delete(platform) },
            delete_platform: { platform, description: await platform.fetch_description() },
            utils: require("../../utils"),
            router: require("../../router"),
            user: user,
        });
    } else {
        const id = platform.id;
        await platform.delete();

        render_message_with_redirect(
            response, user, "Platform deleted",
            `The platform with identifier ${id} has been deleted.`,
            path.platform_list(),
            "Go to the platform list.",
            path.platform_list().url
        );
    }
}
const { User, someone_has_email } = require("../user");
const router = require("../router");
const UTILS = require("../utils");
const EMAIL_QUEUE = require("../email_queue.js");
const RENDER = require("../render.js");

export async function handle_page(request, response) {
    return User.from_current_session(request).then(user => {
        return handle_page_inner(request, response, user);
    });
};

async function handle_page_inner(request, response, user) {
    // Check for POST request
    if (request.method === "POST"
        && typeof request.body.new_password === "string"
        && typeof request.body.confirm_new_password === "string"
        && typeof request.body.email === "string") {

        if (request.body.new_password.length < 8)
            return render_page(response, user, ["The provided password is shorter than 8 characters."], request.body.email);

        if (request.body.new_password !== request.body.confirm_new_password)
            return render_page(response, user, ["The provided passwords do not match."], request.body.email);

        if (!await someone_has_email(request.body.email))
            // Although the provided email address doesn't exist in our database,
            // we don't want the end-user to know about this. So we pretend to
            // have sent an email.
            return RENDER.render_message(
                response, user,
                "Password Reset Request",
                "A confirmation link has been sent to the provided email address.", router.path.password_reset_request()
            );

        // Someone actually does have this mail address
        const requested_user = await User.from_email(request.body.email);
        const reset_token = await requested_user.set_reset_token(request.body.new_password);

        await EMAIL_QUEUE.add_reset_token_email(requested_user.id, reset_token);

        return RENDER.render_message(
            response, user,
            "Password Reset Request",
            "An email has been sent to the provided email address. Don't forget to check the spam folder.", router.path.password_reset_request()
        );
    }
    else
        return render_page(response, user, [], "");
}

function render_page(response, user, errors, email) {
    return response.render("password_reset_request.pug", {
        header: {
            title: "Password Reset Request"
        },
        navigation: {
            page_stack: router.path.password_reset_request(),
        },
        previous_input: {
            email: email
        },
        router: router,
        user: user,
        utils: UTILS,
        errors: errors,
    });
}

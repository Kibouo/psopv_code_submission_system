import { User } from "../user";
import { Request, Response } from "express";
import DB from "../database";
import { path } from "../router";
import { render_message_with_redirect } from "../render";
import { Submission } from "../submission"
import Result from "../result";
import { Assignment } from "../assignment";
import { Course } from "../course";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const [user, subasscrs] = await Promise.all([
        User.from_current_session(request),
        get_subasscrs(request.params.submission_id)
    ]);

    if (!user)
        response.redirect(path.login().url);
    else if (!subasscrs)
        response.sendStatus(404);
    else if (!user.can_delete_submission(subasscrs.submission))
        response.sendStatus(403);
    else if (!request.body.delete) {
        response.render("delete_submission.pug", {
            header: { title: "Delete Submission" },
            navigation: {
                page_stack: path.delete_submission(
                    subasscrs.course, subasscrs.assignment, subasscrs.submission
                )
            },
            submission_page: path.submission_details(
                subasscrs.course, subasscrs.assignment, subasscrs.submission
            ).url,
            utils: require("../utils"),
            router: require("../router"),
            user: user,
        });
    } else {
        await subasscrs.submission.delete();

        const assignment_path = path.assignment(
            subasscrs.course, subasscrs.assignment
        );

        render_message_with_redirect(
            response, user, "Submission deleted",
            `The submission has been deleted.`,
            assignment_path,
            "Go to the assignment page.",
            assignment_path.url
        );
    }
}

async function get_subasscrs(
    submission_id: number
): Promise<{
    submission: Submission, assignment: Assignment, course: Course
} | null> {
    const submission = await Submission.from_id(submission_id);

    if (submission !== null) {
        const assignment = await submission.assignment();
        const course = await assignment.course();
        return { submission, assignment, course };
    } else
        return null;
}
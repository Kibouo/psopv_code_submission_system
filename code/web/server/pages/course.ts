import { User } from "../user";
import { path } from '../router';
import Course from "../course";
import Submission from "../submission";
import Assignment from "../assignment";
import { Request, Response, Router } from "express";
import DB from "../database";
import { SimpleAssignmentView, render_message, render_validation_page } from '../render';
import { delete_from_db } from "../course";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const course_id = request.params.course_id;

    const [user, course] = await Promise.all([
        User.from_current_session(request),
        Course.from_id(course_id)
    ]);

    if (!user)
        response.redirect(path.login().url);
    else if (!course)
        response.sendStatus(404);
    else if (!await user.can_access_course(course.id))
        response.sendStatus(403);
    else if (request.body.delete_course) {
        
        if (!await user.can_delete_course()) 
            response.sendStatus(403);
        else
            return render_validation_page(
                response, 
                user,
                "Validate Course",
                "Are you sure you want to delete the course " + course.name + " ?",
                path.course(course),
                course_id,
                "Delete Course"
            );
    
    } else if (request.body.validate_submit) {

        delete_from_db(course).then(() => render_message(response, user, "Course Deleted", "The course '" + course.name + "' has been successfully deleted.", path.dashboard()));

    } else
        return render_page(response, course, user);
}

async function fetch_recent_submissions(assignments: Assignment[], user: User): Promise<any[]> {
    const recent_submissions_promises =
        assignments.map(assignment => {
            return Submission.all_recent_by_student_for_assignment(
                user.id, assignment.id, 1
            ).then((latest_sub: any[]) => latest_sub.length > 0 ? latest_sub[0] : null);
        });

    return await Promise.all(recent_submissions_promises);
}

async function render_page(response: Response, course: Course, user: User): Promise<void> {
    const [
        all_assignments,
        teachers, students,
        can_create_assignment, can_manage_teachers, can_manage_students, can_edit_course
    ] = await Promise.all([
        Assignment.all_from_course(course.id),
        course.get_teachers(),
        course.get_students(),
        user.can_create_assignment(course.id),
        user.can_manage_teachers(course.id),
        user.can_manage_students(course.id),
        user.can_edit_course(course.id),
    ]);

    const latest_submissions = await fetch_recent_submissions(all_assignments, user);
    const all_assignment_views = await Promise.all(
        all_assignments.map(SimpleAssignmentView.from_assignment)
    );

    response.render("course.pug", {
        header: { title: course.name },
        navigation: { page_stack: path.course(course) },
        course_info: {
            course, assignments: all_assignment_views,
            latest_submissions, teachers, students,
        },
        permissions: {
            can_create_assignment, can_manage_teachers, can_manage_students, can_edit_course
        },
        router: require("../router"),
        utils: require("../utils"),
        user: user,
    });
}
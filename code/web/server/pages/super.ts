import { Request, Response } from "express";
import { User } from "../user";
import { path } from "../router";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(path.login().url);
    else if (!user.can_access_superpanel)
        response.sendStatus(403);
    else
        render_page(response, user);
}

function render_page(response: Response, user: User) {
    return response.render("super.pug", {
        header: { title: "Superpanel" },
        navigation: { page_stack: path.super_panel() },
        router: require("../router"),
        utils: require("../utils"),
        user: user,
    });
}
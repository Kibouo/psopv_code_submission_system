import { Request, Response } from "express";
import { User } from "../user";
import { Course, validate_course_name } from "../course";
import { path } from "../router";
import { render_message_with_redirect } from "../render";
import { ensure_string } from "../utils";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const [user, course] = await Promise.all([
        User.from_current_session(request),
        Course.from_id(request.params.course_id)]
    );

    if (!user)
        response.redirect(path.login().url);
    else if (!course)
        response.sendStatus(404);
    else if (!await user.can_edit_course(course.id))
        response.sendStatus(403);
    else if (!request.body.submit)
        render_page(response, user, course);
    else {
        const new_name = ensure_string(request.body.course_name).trim();
        const validation_errors = validate_course_name(new_name);

        if (validation_errors.length > 0)
            render_page(response, user, course, validation_errors, { course_name: new_name });
        else {
            await course.update_name(new_name);
            render_message_with_redirect(
                response,
                user,
                "Course edited!",
                "The course has been edited successfully!",
                path.edit_course(course),
                "Go back to the course.",
                path.course(course).url
            )
        }
    }
}

function render_page(
    response: Response, user: User, course: Course,
    validation_errors: string[] = [],
    previous_input: { course_name: string } = { course_name: course.name }
) {
    response.render("edit_course.pug", {
        header: { title: "Edit Course" },
        navigation: { page_stack: path.dashboard() },
        router: require("../router"),
        utils: require("../utils"),
        user,
        validation_errors,
        previous_input,
    });
}
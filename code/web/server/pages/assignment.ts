import User from "../user";
import Course from "../course";
import Assignment from "../assignment";
import { Submission, pipeline_status_to_string } from "../submission";
import { Test } from "../test";
import { path, Path } from '../router';
import { Request, Response } from "express";
import { date_to_strings } from "../utils";
import DB from "../database";
import { SubmissionStatusView, UserView, SubmissionView, SimpleAssignmentView, render_validation_page, render_message } from "../render";
import { delete_from_db } from "../assignment";
const PLATFORM_MODULE = require("../platform");

export async function handle_page(request: Request, response: Response) {
    const [user, course, assignment] = await Promise.all([
        User.from_current_session(request),
        Course.from_id(request.params.course_id),
        Assignment.from_id(request.params.assignment_id),
    ]);

    if (!user)
        response.redirect(path.login().url);
    else if (!assignment || !course || assignment.course_id != course.id)
        response.sendStatus(404);
    else {
        const [see_as_teacher, see_as_student] = await Promise.all([
            user.see_assignment_as_teacher(course.id), user.see_assignment_as_student(course.id)
        ]);

        if (!(see_as_teacher || see_as_student))
            response.sendStatus(403);

        else if (see_as_teacher && request.method === "POST") {

            // download the assignment
            if (typeof request.body.download_assignment === "string") {
                const json_assignment = await assignment.to_json();
                response.writeHead(200, {
                    'Content-Type': 'application/json',
                    'Content-Disposition': 'attachment; filename=' + course.name_normalized + '_' + assignment.name_normalized + '_' + date_to_strings(assignment.deadline).utc_iso + '.json'
                });
                response.end(json_assignment);
            }

            // delete assignemnt
            else if (request.body.delete_assignment) {
                if (!await user.can_delete_assignment)
                    response.sendStatus(403);
                else
                    return render_validation_page(
                        response,
                        user,
                        "Delete Assignment",
                        "Are you sure you want to delete the assignment " + assignment.name + " ?",
                        path.assignment(course, assignment),
                        assignment.id,
                        "Delete Assignment"
                    );
            }

            // confirm deletion
            else if (request.body.validate_submit) {
                delete_from_db(assignment).then(() => render_message(response, user, "Assignment Deleted", "The assignment '" + assignment.name + "' has been successfully deleted.", path.dashboard()));
            }

        }

        else
            return render_page(response, course, assignment, user, see_as_teacher, see_as_student);
    }

}

// To help the user we check for weird data such as
// a compilation time of 0 minutes or an expired deadline.
function check_weird_data(assignment: Assignment) {
    const errors = [];
    const warnings = [];

    if (assignment.deadline_expired())
        errors.push("Deadline Expired!");
    if (assignment.compile_time_max === 0)
        warnings.push("Maximum preparation time is 0 minutes.")
    if (assignment.run_time_max === 0)
        warnings.push("Maximum running time per test is 0 minutes.")

    return { errors, warnings };
}

async function get_submission_statuses(
    assignment: Assignment
): Promise<SubmissionStatusView[]> {
    const course = await assignment.course();
    const students = await course.get_students();
    const submissions = students
        .map(student => Submission.most_recent_by_student_for_assignment(student.id, assignment.id)
            .then(submission => SubmissionStatusView.from_user_assignment_submission(
                student, assignment, submission
            ))
        );
    const statuses = await Promise.all(submissions);
    statuses.sort((a, b) => {
        if (a.student.name < b.student.name) return -1;
        else if (a.student.name > b.student.name) return 1;
        else return 0;
    });

    return statuses;
}

async function get_submissions_for_student(
    assignment: Assignment,
    student: User
): Promise<{ student: UserView, submission: SubmissionView, assignment: SimpleAssignmentView }[]> {
    const user_view = new UserView(student);
    const assignment_view = await SimpleAssignmentView.from_assignment(assignment);

    const submissions = await Submission.all_from_student_assignment(student.id, assignment.id);
    const submission_views = await Promise.all(submissions.map(SubmissionView.from_submission));

    return submission_views.map(submission_view => ({
        student: user_view,
        submission: submission_view,
        assignment: assignment_view
    }));
}

/**
 * Renders the assignment page.
 *
 * @param response The response to render the page to.
 * @param course The course this assignment belongs to.
 * @param assignment This assignment's data.
 * @param user The user whom requested this page.
 * @param is_teacher Whether the user is a teacher.
 * @param is_student Whether the user is a student.
 */
async function render_page(
    response: Response,
    course: Course,
    assignment: Assignment,
    user: User,
    is_teacher: boolean,
    is_student: boolean
): Promise<void> {
    const [
        platforms,
        example_tests,
        final_tests,
        can_edit_assignment,
        can_delete_assignment
    ] = await Promise.all([
        PLATFORM_MODULE.from_assignment_id(DB, assignment.id),
        Test.all_example_from_assignment_id(assignment.id),
        Test.all_final_from_assignment_id(assignment.id),
        user.can_edit_assignment(course.id),
        user.can_delete_assignment(course.id)
    ]);

    const notifications = check_weird_data(assignment);

    const [submission_statuses_for_teacher, submissions_for_student] = await Promise.all([
        is_teacher
            ? get_submission_statuses(assignment)
            : Promise.resolve(null),
        is_student
            ? get_submissions_for_student(assignment, user)
            : Promise.resolve(null),
    ]);

    const most_recent_submission_for_student =
        submissions_for_student && submissions_for_student.length > 0
            ? submissions_for_student[0].submission
            : null;

    return response.render("assignment.pug", {
        header: { title: assignment.name },
        navigation: { page_stack: path.assignment(course, assignment) },
        assignment_info: {
            course,
            assignment_view: await SimpleAssignmentView.from_assignment(assignment),
            assignment,
            platforms,
            submission_statuses_for_teacher,
            submissions_for_student,
            most_recent_submission_for_student,
            example_tests,
            final_tests,
        },
        permissions: {
            page_as_teacher: is_teacher,
            page_as_student: is_student,
            can_edit_assignment,
            can_delete_assignment
        },
        errors: notifications.errors,
        warnings: notifications.warnings,
        router: require("../router"),
        utils: require("../utils"),
        user: user
    });
}

import { User } from "../user";
import { path } from "../router";
const MIME_MODULE = require("mime-types");
import Submission from "../submission";
import Course from "../course";
import Assignment from "../assignment";
import { date_to_strings, ensure_number } from "../utils";
const PLATFORM_MODULE = require("../platform.js");
import { TestType, fetch_tests_results_from_submission, Test, TestResult } from "../test";
import { SubmissionView, UserView, SimpleAssignmentView } from "../render";
import DB from "../database";
import { Request, Response } from "express";

export async function handle_page(request: Request, response: Response) {
    const course_id = request.params.course_id;
    const assignment_id = request.params.assignment_id;
    const submission_id = request.params.submission_id;

    const [user, course, assignment, submission] = await Promise.all([
        User.from_current_session(request),
        Course.from_id(course_id),
        Assignment.from_id(assignment_id),
        Submission.from_id(submission_id)
    ]);

    if (!user)
        response.redirect(path.login().url);
    else if (!course || !assignment || !submission)
        return response.sendStatus(404)
    // Make sure these match. Otherwise the use can fake being a teacher by changing the course id.
    else if (course.id !== assignment.course_id
        || assignment.id !== submission.assignment_id)
        return response.sendStatus(404)
    else if (!await user.can_see_submission_details(submission))
        response.sendStatus(403);
    else
        return handle_page_inner(
            response,
            request,
            course,
            assignment,
            submission,
            user
        );

}

async function handle_page_inner(
    response: Response, request: Request,
    course: Course, assignment: Assignment, submission: Submission, user: User
) {

    // Check for POST/download request
    if (request.method === "POST" && typeof request.body.download_source === "string") {

        const source_code = await submission.fetch_source_code();
        // If MIME MODULE doesn't recognize the type, just add no extension.
        const extension = MIME_MODULE.extension(submission.mime_type) || '';

        response.writeHead(200, {
            'Content-Type': submission.mime_type,
            'Content-Disposition': 'attachment; filename=' + assignment.name_normalized + '_' + (await submission.submittor()).name_normalized + '_' + date_to_strings(submission.submitted_on).utc_iso + '.' + extension
        });
        response.end(source_code);


    } else {
        let [submitter, platform, tests_results] = await Promise.all([
            User.from_id(submission.user_id),
            PLATFORM_MODULE.from_id(DB, submission.platform_id),
            fetch_tests_results_from_submission(submission.id)
        ]);

        if (!submitter)
            throw new Error("Submission posted by non-existant user.");

        let show = false;
        let test_type = "";
        let test_id = -1;
        if (request.body.more_info)
            test_id = ensure_number(request.body.more_info);

        tests_results = tests_results.map(({ test, result }, index) => {
            return { test, result, show };
        });

        let prepare_output = await submission.fetch_prepare_output();

        let example_tests_results =
            tests_results.filter(({ test }) => test.type === TestType.Example);


        if (typeof request.body.test_type_example !== "undefined") {
            example_tests_results = example_tests_results.map(({ test, result }, index) => {
                show = (typeof request.body.show_value !== "undefined" && request.body.show_value[index] === "true") ? true : false;
                if (index === (test_id - 1))
                    show = !show;
                return { test, result, show };
            });
        }

        let final_tests_results =
            tests_results.filter(({ test }) => test.type === TestType.Final);
        const can_see_finals = await user.can_see_final_tests(course.id);

        if (typeof request.body.test_type_final !== "undefined") {
            final_tests_results = final_tests_results.map(({ test, result }, index) => {
                show = (typeof request.body.show_value !== "undefined" && request.body.show_value[index] === "true") ? true : false;
                if (index === (test_id - 1))
                    show = !show;
                return { test, result, show };
            });
        }

        return render_page(
            response, course, assignment, submission, user,
            submitter, platform, prepare_output,
            submission.is_prepare_success, example_tests_results, final_tests_results,
            can_see_finals
        );
    }
}


async function render_page(
    response: Response,
    course: Course, assignment: Assignment, submission: Submission,
    user: User, submitter: User, platform: any,
    prepare_output: { stdout: string, stderr: string }, compiled: boolean,
    example_tests_results: { test: Test, result: TestResult }[],
    final_tests_results: { test: Test, result: TestResult }[],
    can_see_finals: boolean
) {
    return response.render("submission_details.pug", {
        header: { title: "Submission details" },
        navigation: {
            page_stack: path.submission_details(course, assignment, submission),
        },
        assignment_info: { assignment: assignment, course: course },
        submission_info: {
            submission: await SubmissionView.from_submission(submission),
            assignment: await SimpleAssignmentView.from_assignment(assignment),
            submitted_on_normalized: date_to_strings(submission.submitted_on).utc_string,
            prepare_output,
            submitter: new UserView(submitter),
            delete_path: await user.can_delete_submission(submission)
                ? path.delete_submission(course, assignment, submission).url
                : null
        },
        example_tests_results, final_tests_results,
        platform: platform,
        compiled: compiled,
        can_see_finals: can_see_finals,
        router: require("../router"),
        utils: require("../utils"),
        user: user,
    });
}

const { User } = require("../user");
const router = require("../router");
import Assignment from "../assignment";
import Submission from "../submission";
import Course from "../course";
import { SimpleAssignmentView, SubmissionView } from "../render";
const UTILS = require("../utils");
const N_LATEST_ASSIGNMENTS = 5;
const N_LATEST_SUBMISSIONS = 3;

export async function handle_page(request, response) {
    return User.from_current_session(request).then(user => {
        // If user logged in
        if (user)
            return route_page(request, response, user);

        // Redirect to login
        else
            response.redirect(router.path.login().url);
    });
}

async function route_page(request, response, user) {
    if (request.method === "POST"
        && typeof Number(request.body.course_picker) === "number"
        && Number(request.body.course_picker) > 0) {
        const course = await Course.from_id(Number(request.body.course_picker));
        return response.redirect(router.path.new_assignment(course).url);
    }
    else if (request.method === "POST")
        return prepare_page(response, user, ["Select a valid course."]);
    else
        return prepare_page(response, user, []);
}

async function prepare_page(response, user, errors) {
    /**
     * Get next n upcoming assignments for current user
     *
     * A promise resolving to an array of the n upcoming assignments
     */
    const assignments = Assignment.upcoming_for_user(user.id, N_LATEST_ASSIGNMENTS)
        .then(assignments => Promise.all(assignments.map(async assignment => {
            const submission =
                await Submission.most_recent_by_student_for_assignment(user.id, assignment.id);
            return {
                assignment: await SimpleAssignmentView.from_assignment(assignment),
                course: await assignment.course(),
                submission: submission ? await SubmissionView.from_submission(submission) : null,
            };
        })));

    /**
     * Get n latest submissions from user
     *
     * @return {external:Promise<[Promise<{Submission, Assignment, Course}>]>}
     * A promise resolving to an array of the n latest submissions with
     * the corresponding assignment.
     */
    const latest_submissions = await Submission.all_recent_by_student(user.id, N_LATEST_SUBMISSIONS);
    const submission_extra =
        await Promise.all(
            latest_submissions.map(async submission => {
                const assignment = await Assignment.from_id(submission.assignment_id);
                const course = await Course.from_id(assignment.course_id);

                return {
                    submission: await SubmissionView.from_submission(submission),
                    assignment: await SimpleAssignmentView.from_assignment(assignment),
                    course: course,
                }
            }
            ));

    let courses;
    if (user.can_create_all_assignments)
        courses = Course.all_courses();
    else
        /**
         * Assume that non-superusers can only create assignments for
         *  courses they are teachers of.
         *
         * An alternate way to do this would be:
         * - get all courses
         * - check for each course whether the current user has permission
         *  to create an assignment (potentially in parallel).
         *
         * Drawbacks of doing the above would be:
         * - user.can_create_assignment makes a db request.
         *  This would result in amount_of_courses extra db requests and is
         *      thus not done here.
         */
        courses = Course.all_courses_where_teacher(user.id);

    return Promise
        .all([assignments, submission_extra, courses])
        .then(data => {
            return render_page(
                response,
                data[0],
                data[1],
                data[2],
                user,
                errors);
        });
}

function render_page(
    response,
    next_n_assignments,
    latest_submissions,
    courses,
    user,
    errors
) {
    response.render("dashboard.pug", {
        header: {
            title: "Dashboard"
        },
        navigation: {
            page_stack: router.path.dashboard(),
        },
        dashboard: {
            next_n_assignments: next_n_assignments,
            latest_submissions: latest_submissions,
            courses: courses,
            errors: errors,
        },
        router: router,
        utils: UTILS,
        user: user,
    });
}

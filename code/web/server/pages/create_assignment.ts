import { User } from "../user";
import { path } from "../router";
import { Course } from "../course";
import { all_platforms_ordered } from "../platform";
import DB from "../database";
import { Request, Response } from "express";
import { render_page, handle_submit } from "../assignment_form_handler";

export async function handle_page(request: Request, response: Response) {
    const course_id = request.params.course_id;
    const user = await User.from_current_session(request);
    const course = await Course.from_id(course_id);

    if (!user)
        return response.redirect(path.login().url);
    else if (!course)
        return response.sendStatus(404);
    else if (!await user.can_create_assignment(course_id))
        return response.sendStatus(403);
    else if (request.method === "GET")
        return render_page(response, course, user, await all_platforms_ordered(DB), []);
    else
        return handle_submit(request, response, course, user);

};

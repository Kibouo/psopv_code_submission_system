import { User } from "../user";
import { path } from "../router";
import Assignment from "../assignment";
import Course from "../course";
import { all_platforms_ordered } from "../platform";
import { render_page, handle_submit } from "../assignment_form_handler";
import DB from "../database";
import { Request, Response } from "express";

export async function handle_page(request: Request, response: Response) {
    const course_id = request.params.course_id;
    const assignment_id = request.params.assignment_id;

    const [user, course, assignment, platforms] = await Promise.all([
        User.from_current_session(request),
        Course.from_id(course_id),
        Assignment.from_id(assignment_id),
        all_platforms_ordered(DB),
    ]);

    if (!user)
        return response.redirect(path.login().url);
    else if (!course || !assignment)
        return response.sendStatus(404);
    else if (!await user.can_edit_assignment(course_id))
        return response.sendStatus(403);
    else if (request.method === "GET")
        return render_page(response, course, user, platforms, [], null, assignment);
    else
        return handle_submit(request, response, course, user, assignment);
};
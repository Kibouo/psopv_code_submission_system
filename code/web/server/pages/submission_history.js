const { User } = require("../user");
const router = require("../router");
import { SimpleAssignmentView, SubmissionView } from "../render";
import Submission from "../submission";
import Course from "../course";
import Assignment from "../assignment";
const UTILS = require("../utils");

export async function handle_page(request, response) {
    const user = await User.from_current_session(request)
    // If user logged in
    if (!user)
        return response.redirect(router.path.login().url);
    if (!await user.can_see_history(request.params.course_id))
        return response.sendStatus(403);

    const req_user = await User.from_id(request.params.user_id);
    if (!req_user || !await req_user.is_student(request.params.course_id))
        return response.sendStatus(404);

    return handle_page_inner(request, response, user);
};

function handle_page_inner(request, response, current_user) {
    const course_id = request.params.course_id;
    const assignment_id = request.params.assignment_id;
    const requested_user_id = request.params.user_id;

    return Promise
        .all([
            Course.from_id(course_id),
            Assignment.from_id(assignment_id),
            User.from_id(requested_user_id),
            Submission.all_from_student_assignment(requested_user_id, assignment_id),
            current_user.see_assignment_as_teacher(course_id)
        ])
        .then(result => {
            // Requested user doesn't exits
            if (!result[2])
                return response.sendStatus(404);
            else
                return render_page(
                    response,
                    result[0],
                    result[1],
                    result[2],
                    result[3],
                    current_user,
                    result[4]
                );
        });
}

// 'page_as_teacher' is passed since PUG doesn't support promises
async function render_page(
    response,
    course,
    assignment,
    requested_user,
    submissions,
    current_user,
    page_as_teacher) {
    return response.render("submission_history.pug", {
        header: {
            title: "Submission history: " + requested_user.name
        },
        navigation: {
            page_stack: router.path.submission_history(course, assignment, requested_user),
        },
        submission_history: {
            course: course,
            submissions: await Promise.all(submissions.map(SubmissionView.from_submission)),
            assignment: await SimpleAssignmentView.from_assignment(assignment),
            requested_user: requested_user,
        },
        permissions: {
            page_as_teacher: page_as_teacher
        },
        router: router,
        utils: UTILS,
        user: current_user,
    });
}

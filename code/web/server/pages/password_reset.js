const { User } = require("../user");
const router = require("../router");
const UTILS = require("../utils");
const EMAIL_QUEUE = require("../email_queue.js");

export async function handle_page(request, response) {
    const user = await User.from_current_session(request);
    return handle_page_inner(request, response, user);
}

async function handle_page_inner(request, response, current_user) {
    // Check for POST request
    if (request.method === "POST"
        && typeof request.body.new_password === "string"
        && typeof request.body.email === "string"
        && typeof request.body.token === "string") {
        const date_now = new Date(Date.now());

        const user = await User.from_email(request.body.email);
        if (!user)
            return render_page(
                response,
                current_user,
                ["The provided data is not valid or the reset token has expired."],
                request.body.email,
                request.body.token);

        const [reset_token_valid, password_correct] =
            await Promise.all([
                user.check_reset_token_valid(request.body.token, date_now),
                user.new_password_correct(request.body.new_password)
            ]);

        if (!reset_token_valid || !password_correct)
            return render_page(
                response,
                current_user,
                ["The provided data is not valid or the reset token has expired."],
                request.body.email, request.body.token);

        await Promise.all([
            user.update_password_reset_expiry_date(date_now),
            user.update_password(),
            EMAIL_QUEUE.add_password_reset_done_email(user.id)
        ]);
        return user.login(request, response);
    }
    else
        return render_page(response, current_user, [], "", request.params.reset_token);
}

function render_page(response, user, errors, email, token) {
    return response.render("password_reset.pug", {
        header: {
            title: "Password Reset"
        },
        navigation: {
            page_stack: router.path.password_reset(),
        },
        previous_input: {
            email: email,
            token: token
        },
        router: router,
        user: user,
        utils: UTILS,
        errors: errors,
    });
}

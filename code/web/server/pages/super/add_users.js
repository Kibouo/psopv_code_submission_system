const { User, create_new_user } = require("../../user");
const router = require("../../router");
const CSV_MODULE = require("../../csv.js");
const UTILS = require("../../utils");
const { render_message } = require("../../render.js");
const EMAIL_QUEUE = require("../../email_queue.js");
import DB from "../../database";

export async function handle_page(request, response) {
    return User.from_current_session(request)
        .then(user => {
            if (user && user.can_add_users)
                if (request.method === "POST" && typeof request.body.users === "string")
                    return handle_submit(response, request.body.users, user);
                else if (request.method === "POST" && typeof request.body.validated_data === "string")
                    return handle_validation(response, request.body.validated_data, user);
                else
                    return render_page(response, "", [], user);
            else if (user)
                response.sendStatus(403);
            else
                response.redirect(router.path.login().url);
        })
}

/**
 * Inserts the validated users into the database, and shows a response page to the user.
 *
 * @param {express:Response} response The response to render pages to.
 * @param {String} validated_data A user array as a string.
 * @param {User} user The currently logged in user.
 */
async function handle_validation(response, validated_data, user) {
    try {
        // We assume the user did not tamper with the data. To get here, they need superuser access
        // anyway, so they have plenty of other ways to do damage.
        const users = JSON.parse(validated_data);

        await insert_users_into_database(users);
        return queue_notification_mails(users).then(() => render_message(
            response,
            user,
            "Successfully added new users!",
            "All users have been successfully added.",
            router.path.super_add_users()
        ));
    } catch (ex) {
        // To get here, the superuser would have needed to tamper with the object.
        return Promise.reject(ex);
    }
}

/**
 *
 * @param {[{name, mail}]} users An array of users to whom we will have to send an email.
 * @returns {external:Promise} A promise representing insertion success or failure.
 */
async function queue_notification_mails(users) {
    const user_objects = await Promise.all(
        users.map(({ _, mail }) => User.from_email(mail))
    );
    return Promise.all(
        user_objects.map(user => EMAIL_QUEUE.add_welcome_email(user.id, user.name))
    );
}

/**
 *
 * @param {[{name, mail}]} users An array of users to add to the database.
 * @returns {external:Promise} A promise representing insertion success or failure.
 */
function insert_users_into_database(users) {
    return Promise.all(
        users.map(({ name, mail }) => create_new_user(name, mail))
    );
}

function handle_submit(response, users_input, user) {
    // Type: [{line: number, result: Result<{name: string, mail: string}, string>}];
    let lines = CSV_MODULE.parse_csv(users_input, ['name', 'mail'])
        .map((result, index) => ({ result: result, line: index + 1 }))
        .filter(
            ({ result }) => result
                .map_failure(failure => failure !== CSV_MODULE.CsvError.EmptyRow)
                .failure_or_default(true)
        )
        .map(({ line, result }) => ({ line, result: result.map_failure(CSV_MODULE.csv_error_to_string) }));

    // Collect any failures into an array.
    const users_to_add = lines
        // Remove line numbers.
        .map(({ line, result }) => result)
        // Keep only successes.
        .filter(result => result.is_success)
        // Unwrap the successes.
        .map(result => result.get_success());

    // Collect any errors into an array.
    let errors = lines
        // Add line numbers so the user can more easily find what we're complaining about.
        .map(({ result, line }) => result.map_failure(msg => `Line ${line}: ${msg}.`))
        // Keep only failures.
        .filter(result => result.is_failure)
        // Unwrap the failures.
        .map(result => result.get_failure());

    // If the same email was supplied twice, add errors.
    for (let duplicate_email of UTILS.find_duplicates(users_to_add.map(user => user.mail)))
        errors.push(`Trying to add multiple users with email ${duplicate_email}.`);


    // Only check for existing emails if the user managed to get even a single line right.
    let already_registered_emails_promise = Promise.resolve([]);

    if (users_to_add.length > 0)
        already_registered_emails_promise = DB.any(
            "SELECT email FROM user_account WHERE email IN ($1:csv)",
            [users_to_add.map(user => user.mail)]
        );
    else if (errors.length === 0)
        errors.push("You must provide at least one user to add.");

    // Check for duplicate users.
    return already_registered_emails_promise.then(already_registered_emails => {
        for (let registered_user of already_registered_emails)
            errors.push("Email was already registered: " + registered_user.email);

        if (errors.length === 0)
            return render_validation_page(response, users_to_add, user);
        else
            return render_page(response, users_input, errors, user);
    });
}

/**
 * Renders the add user page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {String} previous_input Previous input supplied by the user.
 * @param {[String]} parse_errors Any errors that occurred.
 * @param {User} user The currently logged in user.
 */
function render_page(response, previous_input, parse_errors, user) {
    response.render("super/add_users.pug", {
        header: {
            title: "Add Users"
        },
        navigation: {
            page_stack: router.path.super_add_users(),
        },
        add_users: {
            previous_input: previous_input,
            parse_errors: parse_errors,
        },
        utils: UTILS,
        router: router,
        user: user,
    })
}

/**
 * Renders the add user validation page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {[{name, mail}]} users An array of user objects that need to be validated.
 * @param {User} user The currently logged in user.
 */
function render_validation_page(response, users, user) {
    return response.render("super/add_users_validate.pug", {
        header: {
            title: "Validate Users"
        },
        navigation: {
            page_stack: router.path.super_add_users(),
        },
        add_users_validate: {
            users: users,
        },
        router: router,
        utils: UTILS,
        user, user
    });
}

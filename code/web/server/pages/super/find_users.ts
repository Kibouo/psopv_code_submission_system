import { Request, Response } from "express";
import { User } from "../../user";
import { path } from "../../router";
import { ensure_string } from "../../utils";

export async function handle_page(request: Request, response: Response): Promise<Response|void> {
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(path.login().url);
    else if (!user.can_find_users)
        response.sendStatus(403);
    else if (!(typeof request.body.search_btn === "string"))
        return render_page(response, user);
    else {
        const ERROR_MESSAGE =
            "The search query must be at least 1 character and a maximum of 100 characters long.";
        const query: string = ensure_string(request.body.search_input);

        if (query.length < 1 || query.length > 100)
            return render_page(response, user, query, [ERROR_MESSAGE])
        else {

            let users = await User.search(query);

            // if Ajax request
            if (request.get("Accept") === "json") {
                return response.send(JSON.stringify(users));
            }

            return render_page(response, user, query, [], users);
        }
    }
}

function render_page(
    response: Response,
    user: User,
    previous_query: string = "",
    errors: string[] = [],
    found_users: User[] = []
): void {
    response.render("super/find_users.pug", {
        header: { title: "Find users" },
        navigation: { page_stack: path.super_find_users() },
        utils: require("../../utils"),
        router: require("../../router"),
        user,
        find_users: {
            previous_query,
            errors,
            found_users
        }
    });
}
import { Request, Response } from "express";
import { User, delete_user } from "../../user";
import { path } from "../../router";
import { ensure_string, ensure_number } from "../../utils";
import { validate_search_input, add_user_to_csv_textarea, validate_csv_input, parse_and_validate_csv, save_csv } from "../../manage_user";
import { render_message } from "../../render";
import DB from "../..//database";

export async function handle_page(request: Request, response: Response): Promise<Response|void> {
    
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(path.login().url);
    else if (!user.can_delete_users)
        response.sendStatus(403);
    else if (request.method === "GET")
        return render_page(response, user, [], [], [], null);
    else {
        let errors:string[] = [];
        let results:User[] = [];
        let csv_errors:string[] = [];
        let input = { search_input: request.body.search_input, users_csv: request.body.users_csv };
        
        if (typeof request.body.search_btn === "string") {
            
            // search DB for given pattern
            let search_input = validate_search_input(request.body.search_input, errors);
            if (errors.length === 0) {
                results = await User.search(search_input);
            }

            // if Ajax request
            if (request.get("Accept") === "json") {
                return response.send(JSON.stringify(results));
            }

        } else if (request.body.add_btn) {
            
            // add user to csv

            const search_input = validate_search_input(request.body.search_input, errors);
            if (errors.length === 0) {
                results = await User.search(search_input);
            }

            const user_to_add_id = ensure_number(request.body.add_btn);
            // add the student to the csv
            input.users_csv = await add_user_to_csv_textarea(user_to_add_id, DB, input.users_csv);
            
        } else if (request.body.save) {
            
            // parse csv and validate deletion
            let csv_input = validate_csv_input(request.body.users_csv, errors);
            if (errors.length === 0) {
                let validated_csv = await parse_and_validate_csv(request.body.users_csv, DB);
                csv_errors = validated_csv.csv_errors;
                check_if_users_deletable(validated_csv.users_to_add, csv_errors, user);

                if (csv_errors.length === 0) {
                    return render_validation_page(response, validated_csv.users_to_add, user);
                }
            }

        } else if (request.body.delete_users) {
            // remove users from the database
            let users_to_delete :User[] = JSON.parse(request.body.validated_data); 
            await Promise.all(users_to_delete.map(usr => delete_user(usr)));
            return render_message(response, user, "Succesfully deleted users!", "Succesfully deleted " + users_to_delete.length + " users", path.super_delete_users());
        }
        

        return render_page(
            response, 
            user,
            results,
            errors,
            csv_errors,
            input
        );

    }

}

function check_if_users_deletable(users_to_delete: User[], csv_errors: string[], current_user: User) : void {
    
    users_to_delete.forEach(usr => {
        if (usr.id === current_user.id)
            csv_errors.push("Unable to delete yourself.");
        if (usr.id === 1)
            csv_errors.push("Unable to delete the default admin.");
    });

}

function render_page(
    response: Response,
    user: User,
    results: User[],
    errors: string[], 
    csv_errors: string[],
    previous_input: any
): void {

    if (!previous_input) {
        previous_input = {
            search_input: "",
            users_csv: "",
        };
    }

    return response.render("super/delete_users.pug", {
        header: { 
            title: "Delete users" 
        },
        navigation: { page_stack: path.super_delete_users() },
        results: results,
        errors: {
            validation_errors: errors,
            csv_errors: csv_errors,
        },
        previous_input,
        utils: require("../../utils"),
        router: require("../../router"),
        user,
    });
}

/**
 * Renders the delete user validation page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {[User]} users An array of user objects that need to be validated.
 * @param {User} user The currently logged in user.
 */
function render_validation_page(response: Response, users: User[], user: User) {
    return response.render("super/delete_users_validate.pug", {
        header: {
            title: "Validate Users"
        },
        navigation: {
            page_stack: path.super_delete_users(),
        },
        delete_users_validate: {
            users: users,
        },
        utils: require("../../utils"),
        router: require("../../router"),
        user,
    });
}
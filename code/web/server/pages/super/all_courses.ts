const router = require("../../router");
const UTILS = require("../../utils");
import Course from "../../course";
import { Request, Response } from "express";
import { User } from "../../user";

export async function handle_page(request: Request, response: Response): Promise<void> {
    const user = await User.from_current_session(request);

    if (!user)
        response.redirect(router.path.login().url);
    else if (!user.can_see_all_courses)
        response.sendStatus(403)
    else
        render_page(response, await Course.all_courses(), user);
};

function render_page(response: Response, courses: Course[], user: User) {
    return response.render("super/all_courses.pug", {
        header: { title: "All courses" },
        navigation: { page_stack: router.path.super_all_courses() },
        page: { courses: courses },
        router: router,
        utils: UTILS,
        user: user,
    });
}

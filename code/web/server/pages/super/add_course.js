const { User } = require("../../user");
const router = require("../../router");
import { insert_course_into_database } from "../../course";
const UTILS = require("../../utils");

export async function handle_page(request, response) {
    return User.from_current_session(request).then(user => {
        // If user logged in
        if (user && user.can_add_course)
            if (request.method === "POST" && typeof request.body.add_course === "string")
                return render_validation_page(response, request.body.add_course, user);
            else if (request.method === "POST" && typeof request.body.validated_data === "string")
                return handle_validation(response, request.body.validated_data, user);
            else
                return render_page(response, user);
        else if (user)
            return response.sendStatus(403);
        // Redirect to login
        else
            return response.redirect(router.path.login().url);
    });
}

/**
 * Renders the add course validation page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {String} course Course title that needs to be validated.
 * @param {User} user The currently logged in user.
 */
function render_validation_page(response, course_title, user) {
    return response.render("super/add_course_validate.pug", {
        header: {
            title: "Validate Course"
        },
        navigation: {
            page_stack: router.path.super_add_course(),
        },
        add_course_validate: {
            title: course_title,
        },
        router: router,
        utils: UTILS,
        user, user
    });
}

/**
 * Inserts the validated course into the database, and shows a response page to the user.
 *
 * @param {express:Response} response The response to render pages to.
 * @param {String} validated_course_title Title of the course.
 * @param {User} user The currently logged in user.
 */
function handle_validation(response, validated_course_title, user) {

    const course_name = validated_course_title;

    return insert_course_into_database(course_name)
        .then(() => response.render("message.pug", {
            header: { title: "Course Added" },
            navigation: { page_stack: router.path.super_add_course() },
            message: {
                title: "Added new course!",
                content: "The course '" + validated_course_title + "' has been successfully added.",
            },
            router: router,
            utils: UTILS,
            user: user,
        }));
}

/**
 * Renders the add course page.
 *
 * @param {express:Response} response The response to render the page to.
 * @param {User} user The currently logged in user.
 */
function render_page(response, user) {
    return response.render("super/add_course.pug", {
        header: { title: "Add Course" },
        navigation: { page_stack: router.path.super_add_course() },
        router: router,
        utils: UTILS,
        user: user,
    });
}

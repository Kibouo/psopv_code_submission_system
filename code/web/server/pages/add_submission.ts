import { User } from "../user";
import { Assignment } from "../assignment";
import { Submission } from "../submission";
import Result from "../result";
const FS = require('fs');
import { path } from "../router";
import { add_submission_receive_email } from "../email_queue";
import { render_message, render_message_with_redirect, SimpleAssignmentView } from "../render";
import { Request, Response } from "express";
import multer from "multer";

export async function handle_page(request: Request, response: Response) {
    const [user, assignment] = await Promise.all([
        User.from_current_session(request),
        Assignment.from_id(request.params.assignment_id)
    ]);

    if (!user)
        return response.redirect(path.login().url);
    else if (!assignment)
        response.sendStatus(404);
    else if (!await user.can_create_submission(assignment.course_id))
        response.sendStatus(403);
    else if (assignment.deadline_expired()) {
        response.status(403);

        render_message(
            response, user,
            "Deadline Expired!",
            "The deadline on this assignment has expired and no more submissions are accepted.",
            path.add_submission(await assignment.course(), assignment),
        );
    } else if (!request.file)
        return render_page(request, response, user, assignment, []);
    else {
        const platform_result = await platform_id_from_body(request.body, assignment);

        if (platform_result.is_failure)
            return render_page(request, response, user, assignment, [platform_result.get_failure()]);
        else {
            const code = FS.readFileSync(request.file.path);
            const submission_promise = Submission.create_new(
                user.id, assignment.id, platform_result.get_success(), code, request.file.mimetype
            );
            const [submission, course] = await Promise.all([submission_promise, assignment.course()]);

            await add_submission_receive_email(user.id, assignment.name, path.submission_details(course, assignment, submission).url);

            render_message_with_redirect(
                response, user,
                "Submission Added!",
                "Your submission has been posted. It will be tested soon.",
                path.add_submission(course, assignment),
                "View your new submission.",
                path.submission_details(course, assignment, submission).url
            );
        }
    }
}

/**
 * Extracts which platform the user selected.
 *
 * @returns {extern:Promise}
 * A promise resolving to a Result. The Result is a success if the platform is allowed by the
 * assignment. It's a failure when the user did not input a valid platform, or it is not allowed.
 *
 * The success result will contain the selected platform id as a number.
 * The failing result will contain a useful error string to display to the user.
 */
function platform_id_from_body(
    body: any, assignment: Assignment
): Promise<Result<number, string>> {
    return (
        body.platform
            ? Result.new_success<any, string>(body.platform)
            : Result.new_failure<any, string>("You must supply the platform your submission wil run on."))
        .map_success(platform_id => parseInt(platform_id, 10))
        .success_then(platform_id => !isNaN(platform_id)
            ? Result.new_success<number, string>(platform_id)
            : Result.new_failure<number, string>("The web browser sent an invalid platform."))
        .map_success(id => assignment.platform_is_allowed(id)
            .then(platform_allowed => ({ id, platform_allowed })))
        .success_extract_promise()
        .then(result => (<Result<{ id: number, platform_allowed: boolean }, string>>result)
            .success_then(({ id, platform_allowed }) => platform_allowed
                ? Result.new_success<number, string>(id)
                : Result.new_failure<number, string>("The selected platform does not exist.")));
}

async function render_page(
    request: Request, response: Response, user: User, assignment: Assignment, errors: string[]
) {
    const course = await assignment.course();

    return response.render("add_submission.pug", {
        header: { title: "Create Submission" },
        navigation: { page_stack: path.add_submission(course, assignment) },
        router: require("../router"),
        utils: require("../utils"),
        user: user,
        add_submission: {
            course,
            assignment: await SimpleAssignmentView.from_assignment(assignment),
            platforms: await assignment.platforms(),
            errors
        },
    });
}
import { Request, Response } from "express";
import { handle_page } from "../manage_user"

export async function handle_student_page(request: Request, response: Response) {
    return handle_page(request, response, "student");
}

export async function handle_teacher_page(request: Request, response: Response) {
    return handle_page(request, response, "teacher");
}
const { User } = require("../user");
const router = require("../router");
const UTILS = require("../utils");

export async function handle_page(request, response) {
    return User.from_current_session(request).then(user => {
        // If user not logged in
        if (!user)
            return handle_page_inner(request, response, user);

        // Redirect to dashboard
        else
            return response.redirect(router.path.dashboard().url);
    });
};

async function handle_page_inner(request, response, user) {
    const login_error = ["Incorrect email or password."];

    // Check for POST request
    if (request.method === "POST"
        && typeof request.body.login_email === "string"
        && typeof request.body.login_password === "string") {
        // Search for user requested by client
        const user = await User.from_email(request.body.login_email);

        if (user && await user.password_correct(request.body.login_password))
            // Login and enable session if email.password combo was correct
            return user.login(request, response);
        else
            return render_page(response, login_error, null, request.body.login_email);
    }
    else
        return render_page(response, [], null, "");
}

function render_page(response, errors, user, email) {
    return response.render("login.pug", {
        header: {
            title: "Login"
        },
        navigation: {
            page_stack: router.path.login(),
        },
        login: {
            login_errors: errors,
        },
        previous_input: {
            email: email
        },
        router: router,
        utils: UTILS,
        user: user,
    });
}

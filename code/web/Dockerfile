FROM node:9.8.0-alpine

EXPOSE 80
EXPOSE 443

WORKDIR /app
RUN apk --no-cache add python build-base postgresql-client openssl curl
COPY package.json package-lock.json ./
RUN npm install --only-production
RUN apk del python build-base

COPY public ./public

# Compile Stylesheets
COPY stylesheet ./stylesheet
RUN npx lessc stylesheet/style.less public/style.css
RUN rm -r stylesheet/

# Compile TypeScript
COPY tsconfig.json ./
COPY server ./server
RUN npx tsc
RUN rm -r server/ tsconfig.json

COPY views ./views
COPY start.sh ./start.sh
COPY healthcheck.sh ./healthcheck.sh
HEALTHCHECK --interval=10s --start-period=30s --timeout=2s --retries=1 CMD ./healthcheck.sh || exit 1
ENTRYPOINT ./start.sh
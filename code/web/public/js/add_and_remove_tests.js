// add event listener for the add example button
const add_example = document.getElementsByName("add_example_test")[0];
add_example.addEventListener("click", function (event) {
    event.preventDefault();
    add_test(true, event.target);
});

// add event listener for the add final button
const add_final = document.getElementsByName("add_final_test")[0];
add_final.addEventListener("click", function (event) {
    event.preventDefault();
    add_test(false, event.target);
});

// add event listeners to all remove example buttons
const remove_examples = document.getElementsByName("remove_example_test");
for(let i=0; i< remove_examples.length; i++) {
    remove_examples[i].addEventListener("click", function (event) {
        event.preventDefault();
        remove_test(true, event.target);
    });
}

// add event listeners to all remove final buttons
const remove_finals = document.getElementsByName("remove_final_test");
for(let i=0; i< remove_finals.length; i++) {
    remove_finals[i].addEventListener("click", function (event) {
        event.preventDefault();
        remove_test(false, event.target);
    });
}

function add_test(example_test, button) {
      
    let test_name = "final";
    if (example_test) {
        test_name = "example";
    }
    
    // creates new example test input
    let parent_container = document.getElementById(test_name + "_test_IO");
    let new_test_container = document.createElement("div");
    let new_test_input =  document.createElement("textarea");
    let new_test_output =  document.createElement("textarea");
    let new_test_button =  document.createElement("button");
      
    // set the attributes for the newly created elements
    new_test_container.setAttribute("class", "row framed");
      
    new_test_input.setAttribute("name", test_name + "_test_inputs[]");
    new_test_input.setAttribute("value", "");
    new_test_input.setAttribute("rows", "2");
    new_test_input.setAttribute("wrap", "off");

    new_test_output.setAttribute("name", test_name + "_test_outputs[]");
    new_test_output.setAttribute("value", "");
    new_test_output.setAttribute("rows", "2");
    new_test_output.setAttribute("wrap", "off");
    
    const btn_text = document.createTextNode("-");
    new_test_button.setAttribute("class", "small");
    new_test_button.setAttribute("name", "remove_example_test");
    new_test_button.setAttribute("type", "submit");
    new_test_button.setAttribute("value", button.id + 1);
    new_test_button.appendChild(btn_text);

    // add event listener to the new button
    new_test_button.addEventListener("click", function (event) {
        event.preventDefault();
        remove_test(example_test, event.target);
    });
    
    new_test_container.appendChild(new_test_input);
    new_test_container.appendChild(new_test_output);
    new_test_container.appendChild(new_test_button);

    parent_container.appendChild(new_test_container);
    
}

function remove_test(example_test, button) {
      
    let test_name = "final";
    if (example_test) {
        test_name = "example";
    }
    
    // remove all children from the test_container
    let parent_container = button.parentElement;
    while (parent_container.firstChild) {
        parent_container.removeChild(parent_container.firstChild);
    }
    
    let container = document.getElementById(test_name + "_test_IO");
    
    // remove test_container
    container.removeChild(parent_container);
  
}
// add event listener for the search button
const find_users = document.getElementsByName("search_btn")[0];
find_users.addEventListener("click", function (event) {
    event.preventDefault();
    const search_query = document.getElementsByName("search_input")[0].value;
    const search_bar = document.getElementById("search_bar");

    // if there is no search bar display in compact mode
    return (search_bar === null) ? loadUsersCompact(search_query) : loadUsers(search_query);
});

// add event listener for the add button
const add_user = document.getElementsByName("add_btn");
for (let i = 0; i < add_user.length; i++) {
    add_user[i].addEventListener("click", function (event) {
        event.preventDefault();
        add_user_to_textarea(event.target);
    });
}

function add_user_to_textarea(button) {
    let framed = button.previousSibling;
    const email_start_index = framed.innerHTML.indexOf("&lt;") + 4;
    const email_length = framed.innerHTML.indexOf("&gt;") - email_start_index;
    let user_email = framed.innerHTML.substr(email_start_index, email_length).trim();
    let textar = document.getElementsByName("users_csv")[0];
    let textarValue = textar.value;

    textarValue = textarValue.trim();
    if (!(textarValue.endsWith("\n"))) {
        textarValue += "\n"
    }
    textarValue += (user_email + "\n");
    textar.value = textarValue;
}

function setupXMLHttpRequest() {
    const http = new XMLHttpRequest();
    const url = window.location.href;
    http.open('POST', url, true);
    http.setRequestHeader("Accept", "json");
    //Send the proper header information along with the request
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    return http;
}

function encodeParamsObject(params) {
    let result = Object.keys(params).map(k => {
        return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
    }).join('&');

    return result;
}

function loadUsersCompact(data) {
    const http = setupXMLHttpRequest();
    let params = encodeParamsObject({ search_input: data, search_btn: "" });
    http.onreadystatechange = function () { //Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {

            const results = JSON.parse(http.responseText);
            let results_container = document.createElement("div");
            results_container.setAttribute("class", "framed");
            results_container.innerHTML = "Found " + results.length + " users.";

            let results_list = document.createElement("ul");

            for (let i = 0; i < results.length; i++) {

                let current_user = results[i];
                let list_element = document.createElement("li");
                let profileLink = document.createElement("a");
                profileLink.innerHTML = current_user.name + " &lt;" + current_user.email + "&gt;";
                let lineBreak = document.createElement("br");
                profileLink.setAttribute("href", "/profile_page/" + current_user.name_normalized + "-" + current_user.id);

                list_element.appendChild(profileLink);
                list_element.appendChild(lineBreak);

                results_list.appendChild(list_element);
            }

            // clear existing search if there is any
            let old_results_container = document.getElementsByClassName("framed")[0];
            if (typeof old_results_container !== 'undefined') {
                let old_results_list = old_results_container.firstChild;
                while (old_results_list.firstChild) {
                    let old_list_element = old_results_list.firstChild;
                    while (old_list_element.firstChild)
                        old_list_element.removeChild(old_list_element.firstChild);
                    old_results_list.removeChild(old_list_element);
                }
                old_results_container.removeChild(old_results_list);
                old_results_container.parentNode.removeChild(old_results_container);
            }

            // add new data
            results_container.appendChild(results_list);
            document.getElementById("main-content").appendChild(results_container);

        }
    }

    http.send(params);

}

function loadUsers(data) {

    const http = setupXMLHttpRequest();
    let params = encodeParamsObject({ search_input: data, search_btn: "" });

    http.onreadystatechange = function () { //Call a function when the state changes.
        if (http.readyState == 4 && http.status == 200) {
            // const results = http.responseText;
            const results = JSON.parse(http.responseText);

            const title = document.createElement("h1");
            let results_container = document.createElement("div");
            let column = document.createElement("div");
            column.setAttribute("class", "column");
            results_container.setAttribute("id", "results_container");
            title.innerHTML = "Results (" + results.length + ")";

            for (let i = 0; i < results.length; i++) {

                let row = document.createElement("div");
                let add_btn = document.createElement("button");
                let framed = document.createElement("div");

                // set the attributes for the newly created elements
                row.setAttribute("class", "row");
                framed.setAttribute("class", "framed most");
                add_btn.setAttribute("class", "least min_width_off");
                add_btn.setAttribute("name", "add_btn");
                add_btn.setAttribute("type", "submit");
                add_btn.setAttribute("value", i);

                add_btn.addEventListener("click", function (event) {
                    event.preventDefault();
                    add_user_to_textarea(event.target);
                });

                // set innerHTML's
                framed.innerHTML = results[i].name + " &lt;" + results[i].email + "&gt;";
                add_btn.innerHTML = "Add";

                row.appendChild(framed);
                row.appendChild(add_btn);

                column.appendChild(row);

            }
            const search_bar = document.getElementById("search_bar");
            results_container.appendChild(column);

            // clear existing search if there is any
            let old_results_container = document.getElementById("results_container");
            if (old_results_container !== null) {
                let old_title = old_results_container.previousSibling;
                let old_column = old_results_container.firstChild;
                while (old_column.firstChild) {
                    old_row = old_column.firstChild;
                    old_framed = old_row.firstChild;
                    old_framed.removeChild(old_framed.firstChild);
                    old_row.removeChild(old_framed);
                    old_column.removeChild(old_row);
                }
                old_results_container.removeChild(old_column);
                old_results_container.parentNode.removeChild(old_title);
                old_results_container.parentNode.removeChild(old_results_container);
            }

            // add new data
            search_bar.parentNode.insertBefore(results_container, search_bar.nextSibling);
            search_bar.parentNode.insertBefore(title, search_bar.nextSibling);
        }
    }

    http.send(params);

}
// add event listener for the collapse buttons
const collapse_toggle = document.getElementsByName("more_info");
for(let i=0; i < collapse_toggle.length; i++) {
    collapse_toggle[i].addEventListener("click", function (event) {
        event.preventDefault();
        let is_extended = event.target.previousSibling.previousSibling.previousSibling.previousSibling.value;
        return toggle_collapse(is_extended, event.target);
    });
}

function toggle_collapse(is_extended, button) {
    
    if (is_extended === "false") {
        
        // extend test
        let expected_output_label = document.createElement("label");
        let stderr_label =  document.createElement("label");
        expected_output_label.innerHTML = "Expected ouput"
        stderr_label.innerHTML = "Stderr"

        // add labels to html
        button.parentNode.insertBefore(expected_output_label, button.nextSibling);
        button.parentNode.insertBefore(stderr_label, button.nextSibling);

        let extended_container = document.createElement("div");
        extended_container.setAttribute("class", "row");

        // create expected output element
        let expected_output_container = document.createElement("div");
        expected_output_container.setAttribute("class", "framed");
        let expected_output = document.createElement("pre");
        expected_output.innerHTML = button.previousSibling.previousSibling.value;
        expected_output_container.appendChild(expected_output);

        // create stderr element
        let stderr_container = document.createElement("div");
        stderr_container.setAttribute("class", "framed");
        let stderr = document.createElement("pre");
        stderr.innerHTML = button.previousSibling.value;
        stderr_container.appendChild(stderr);

        // add expected output and stderr to extended_container
        extended_container.appendChild(stderr_container);
        extended_container.appendChild(expected_output_container);

        button.parentNode.parentNode.appendChild(extended_container);
        button.innerHTML = "v";

    } else {

        // collapse test

        // remove the labels
        button.parentNode.removeChild(button.nextSibling);
        button.parentNode.removeChild(button.nextSibling);

        // remove extended container
        let extended_container = button.parentNode.nextSibling;
        removeContainerRecursive(extended_container);
        extended_container.parentNode.removeChild(extended_container);

        button.innerHTML = ">";

    }

    button.previousSibling.previousSibling.previousSibling.previousSibling.value = (is_extended === "false") ? "true" : "false";

}

// removes all children of a DOM element recursivly
function removeContainerRecursive(container) {
    while (container.firstChild) {
        removeContainerRecursive(container.firstChild);
        container.removeChild(container.firstChild);
    }
}
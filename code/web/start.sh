#!/bin/sh

set -e

POSTGRES_USERNAME=`cat /run/secrets/database_username`
POSTGRES_PASSWORD=`cat /run/secrets/database_password`
POSTGRES_DATABASE=`cat /run/secrets/database_name`

until PGPASSWORD=$POSTGRES_PASSWORD psql -d $POSTGRES_DATABASE -h "database" -U $POSTGRES_USERNAME -c '\q'; do
  >&2 echo "Waiting for database to start up…"
  sleep 1
done

>&2 echo "Database is up. Starting web server…"
npm start


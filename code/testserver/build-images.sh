#/bin/sh

for runner in ./runners/*
do
    echo "Building runner $runner."
    docker build --tag `basename $runner` $runner
done
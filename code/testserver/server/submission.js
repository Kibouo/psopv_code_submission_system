const DB = require("./database.js");
const CONFIG = require("./config.json");

module.exports = {
    /**
     * Finds a submission in the waiting stage, sets it to preparing and returns it.
     *
     * @param {[Number]} platform_ids
     * Only submissions of these platforms will be searched.
     *
     * @returns {external:Promise.<Submission|null>}
     * Returns a submission if one was found, or null when there is no submissions of the requested
     * platforms that are in the waiting stage.
     */
    async retrieve_work(platform_ids) {
        console.assert(platform_ids.length > 0, "At least one platform must be supported.");

        // FOR UPDATE acquires a write-lock on every row that is SELECTed. If another query tries
        // to access said row, it will wait until this transactions is complete.
        //
        // However, if multiple of these queries run at once, they might both first select their
        // rows, and then both try to acquire write locks on them. The first one will update the
        // right row and end its transaction. The second one will then be able to take its lock, but
        // the row its trying to update has already been updated by the first row.
        //
        // SKIP LOCKED makes it so the the query will skip any rows for which it cannot immediately
        // acquire a lock. In the example above, the second query would therefore not wait on the
        // first query to complete, but rather move on and select a different row which hasn't been
        // locked yet.
        //
        // https://blog.2ndquadrant.com/what-is-select-skip-locked-for-in-postgresql-9-5/
        const row = await DB.oneOrNone(`
            UPDATE submission
            SET
                pipeline_status = 'prepare',
                last_update = CURRENT_TIMESTAMP
            WHERE id = (
                SELECT submission.id
                FROM submission
                    INNER JOIN assignment ON submission.assignment_id = assignment.id
                WHERE
                    submission.pipeline_status = 'wait' AND
                    submission.platform_id IN ($(platform_ids:csv)) AND
                    assignment.tests_platforms_inserted = true
                FOR UPDATE SKIP LOCKED
                LIMIT 1
            )
            RETURNING submission.id, submission.pipeline_epoch
        `,
            { platform_ids });

        if (!row)
            return null;
        else
            return initialise_submission(row.id, row.pipeline_epoch);
    }
}

async function initialise_submission(submission_id, pipeline_epoch) {
    const row = await DB.one(`
        SELECT
            submission.platform_id,
            assignment.id AS assignment_id,
            EXTRACT(epoch FROM assignment.compile_time_max) AS compile_time_max,
            EXTRACT(epoch FROM assignment.run_time_max) AS run_time_max
        FROM submission, assignment
        WHERE
            submission.assignment_id = assignment.id
            AND submission.id = $(submission_id)
            AND submission.pipeline_epoch = $(pipeline_epoch)
    `,
        { submission_id, pipeline_epoch });

    if (row === null)
        return null;

    const {
        assignment_id, compile_time_max, run_time_max, platform_id
    } = row;

    let last_poll = Promise.resolve();
    let should_poll = true;

    /**
     * Polls the database to check if we are still allowed to work on this submission.
     *
     * @returns {Promise}
     * A promise resolving if we are still allowed to work on this submission, rejecting otherwise.
     */
    const poll = function () {
        // This function is only ever called once at a time, because it only calls itself once all
        // promises inside have been resolved.
        if (should_poll) {
            last_poll = last_poll
                .then(() => console.info(`Updating lock for submission ${submission_id}.`))
                .then(() => DB.oneOrNone(`
                    UPDATE submission
                    SET last_update = CURRENT_TIMESTAMP
                    WHERE id = $(submission_id) AND pipeline_epoch = $(pipeline_epoch)
                    RETURNING id
                `,
                    { submission_id, pipeline_epoch }
                ))
                // We reject the promise if the lock on the database has been lost. This can happen
                // because the assignment was updated while we were testing, or we've not polling
                // at a high enough frequency causing other systems to think we died, and marked the
                // submission as available again.
                .then(row => row
                    ? Promise.resolve()
                    : Promise.reject("The lock on the submission has been lost.")
                )
                // We need to keep polling.
                .then(() => setTimeout(poll, CONFIG.poll_interval));
            // Node desperately wants us to handle the error in some way.
            last_poll.catch(console.error);
        }
    };

    // Start the polling. Calling this once will keep the polling going automatically.
    poll();

    /**
     * Constructs a new handle to a test. This handle allows to fetch the input and output of a
     * test, as well as set the test result.
     *
     * @param {number} id
     * The id of the test to construct a handle for.
     *
     * @returns {*}
     * A test handle.
     */
    function construct_test_handle({ id }) {
        return {
            /**
             * Gets the id of this test.
             *
             * @returns {number}
             * The id of this test.
             */
            get test_id() {
                return id;
            },

            /**
             * Fetches the input and the output of this test.
             *
             * @returns {external:Promise.<{input: string, output: string}>}
             * A promise resolving to this test's input and output.
             */
            fetch_input_output() {
                return last_poll.then(() => DB.one(
                    `
                        SELECT input, output
                        FROM test
                        WHERE id = $(id)
                    `,
                    { id }
                ));
            },

            /**
             * Inserts test results for this test into the database.
             *
             * @param {boolean} is_success
             * Whether the test was a success.
             *
             * @param {string} stdout
             * The standard output generated by this test.
             *
             * @param {string} stderr
             * The error output generated by this test.
             *
             * @returns {external:Promise}
             * A promise resolving if setting the results succeeded.
             */
            set_results(is_success, stdout, stderr) {
                return DB.none(
                    `
                        INSERT INTO test_result(
                            test_id, submission_id, pipeline_epoch, is_success, stdout, stderr
                        )
                        VALUES (
                            $(id), $(submission_id), $(pipeline_epoch),
                            $(is_success), $(stdout), $(stderr)
                        )
                        ON CONFLICT (test_id, submission_id) DO UPDATE
                            SET
                                stdout = $(stdout),
                                stderr = $(stderr),
                                is_success = $(is_success),
                                pipeline_epoch = $(pipeline_epoch)
                            WHERE test_result.pipeline_epoch < $(pipeline_epoch)
                    `,
                    { is_success, stdout, stderr, id, submission_id, pipeline_epoch }
                );
            }
        }
    }

    return {
        /**
         * Gets the platform id this submission was submitted as.
         *
         * @returns {Number}
         * The platform id.
         */
        get platform_id() { return platform_id },

        /**
         * Gets the id of this submission.
         *
         * @returns {Number}
         * The submission id.
         */
        get submission_id() { return submission_id },

        /**
         * Gets the maximum time this submission is allowed to compile.
         *
         * @returns {Number}
         * The maximum time this submission is allowed to compile, in milliseconds.
         */
        get max_compile_time() {
            return compile_time_max * 1000
        },

        /**
         * Gets the maximum time this submission is allowed to spend on a single test.
         *
         * @returns {Number}
         * The maximum time this submission is allowed to spend on a test, in milliseconds.
         */
        get max_run_time() {
            return run_time_max * 1000
        },


        /**
         * Fetches this submission's code from the database.
         *
         * @returns {external:Promise.<Buffer>}
         * A promise resolving to a buffer containing the submission.
         */
        fetch_code() {
            return last_poll.then(() => DB.one(`
                SELECT source_code
                FROM submission
                WHERE id = $(submission_id)
            `,
                { submission_id }
            ))
                .then(({ source_code }) => source_code);
        },

        /**
         * Fetches a list of example test handles from this submission.
         *
         * @returns {external:Promise.<[number]>}
         * An array containing handles to all example tests for this submission.
         */
        fetch_example_tests() {
            return last_poll.then(() => DB.any(
                `
                    SELECT id
                    FROM test
                    WHERE
                        assignment_id = $(assignment_id) AND
                        type = 'example'
                `,
                { assignment_id }
            )).then(ids => ids.map(construct_test_handle))
        },

        /**
         * Fetches a list of final test handles from this submission.
         *
         * @returns {external:Promise.<[number]>}
         * An array containing handles to all final tests for this submission.
         */
        fetch_final_tests() {
            return last_poll.then(() => DB.any(
                `
                    SELECT id
                    FROM test
                    WHERE
                        assignment_id = $(assignment_id) AND
                        type = 'final'
                `,
                { assignment_id }
            )).then(ids => ids.map(construct_test_handle))
        },

        /**
         * Sets the results of the prepare stage.
         *
         * @param {boolean} is_success Whether the prepare stage completed successfully.
         * @param {String} stdout The standard output of the prepare stage.
         * @param {String} stderr The standard error output of the prepare stage.
         * @returns {external:Promise} A promise resolving if the transaction was successful.
         */
        set_prepare_result(is_success, stdout, stderr) {
            return last_poll.then(() => DB.none(
                `
                    UPDATE submission
                    SET
                        prepare_stdout = $(stdout),
                        prepare_stderr = $(stderr),
                        is_prepare_success = $(is_success)
                    WHERE id = $(submission_id) AND pipeline_epoch = $(pipeline_epoch)
                `,
                { is_success, stdout, stderr, submission_id, pipeline_epoch }
            ))
        },

        /**
         * Sets the submission's stage in the pipeline to the example test stage.
         * @returns {external:Promise} A promise resolving if the transaction was successful.
         */
        start_example_test_stage() {
            return last_poll.then(() => DB.none(
                `
                    UPDATE submission
                    SET
                        pipeline_status = 'example'
                    WHERE id = $(submission_id) AND pipeline_epoch = $(pipeline_epoch)
                `,
                { submission_id, pipeline_epoch }
            ))
        },

        /**
         * Sets the submission's stage in the pipeline to the final test stage.
         * @returns {external:Promise} A promise resolving if the transaction was successful.
         */
        start_final_test_stage() {
            return last_poll.then(() => DB.none(
                `
                    UPDATE submission
                    SET
                        pipeline_status = 'final'
                    WHERE id = $(submission_id) AND pipeline_epoch = $(pipeline_epoch)
                `,
                { submission_id, pipeline_epoch }
            ))
        },

        /**
         * Sets the submission's stage to done.
         * @returns {external:Promise} A promise resolving if the transaction was successful.
         */
        end_pipeline() {
            return last_poll.then(() => DB.none(
                `
                    UPDATE submission
                    SET
                        pipeline_status = 'done'
                    WHERE id = $(submission_id) AND pipeline_epoch = $(pipeline_epoch)
                `,
                { submission_id, pipeline_epoch }
            ))
        },

        /**
         * Fetch the total amount of example tests to be done for the assignment to
         * which this submission belongs.
         * @returns {external:Promise<Number>} A promise resolving to the amount of example tests.
         */
        fetch_example_test_count() {
            return last_poll
                .then(() => DB.one(
                    `SELECT count(*) count
                    FROM test
                    WHERE assignment_id = $(assignment_id)
                        AND type='example'`,
                    { assignment_id }
                ))
                .then(({ count }) => Number(count));
        },

        /**
         * Fetch the amount of successfully completed example tests done by this submission.
         * @returns {external:Promise<Number>} A promise resolving to the amount of successfully
         * completed example tests.
         */
        fetch_successful_example_test_count() {
            return last_poll
                .then(() => DB.one(
                    `SELECT count(*) successful_test_count
                    FROM test_result
                        JOIN test
                        ON test_result.test_id = test.id
                    WHERE submission_id = $(submission_id) AND test.type = 'example' AND test_result.is_success=true`,
                    { submission_id }
                ))
                .then(({ successful_test_count }) => Number(successful_test_count));
        },

        /**
         * Clears the lock this submission keeps on the database.
         *
         * This function *MUST* be called before the object is destroyed, and the object cannot be
         * used after.
         *
         * @returns {external:Promise}
         * A promise resolving if we had the lock the entire time, rejecting if we did not.
         */
        unlock() {
            // We delete some of our functions just to make sure they are not used ever again
            // beyond this point.
            delete this.fetch_code;
            delete this.unlock;

            // No point in checking the lock beyond this point.
            should_poll = false;

            // Return last poll, because why not. We don't really need to. If the poll failed, it
            // doesn't matter because we were already releasing the lock anyway. If it didn't fail,
            // that works too.
            return last_poll;
        }
    };
}
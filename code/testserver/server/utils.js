module.exports = { sleep };

/**
 * Returns a promise resolving after a given amount of miliseconds.
 * 
 * @param {Number} miliseconds
 * The amount of miliseconds to sleep for.
 * 
 * @returns {Promise}
 * A promise resolving after a given amount of miliseconds.
 */
function sleep(miliseconds) {
    return new Promise(resolve => setTimeout(resolve, miliseconds));
}
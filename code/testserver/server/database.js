const FS = require("fs");
const PGP = require("pg-promise")();

const USERNAME = FS.readFileSync("/run/secrets/database_username");
const PASSWORD = FS.readFileSync("/run/secrets/database_password");
const NAME = FS.readFileSync("/run/secrets/database_name");

const DB = PGP(`postgres://${USERNAME}:${PASSWORD}@database:5432/${NAME}`);

module.exports = DB;
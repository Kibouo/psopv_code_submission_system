const DB = require("./database.js");
const SUBMISSION_MODULE = require("./submission.js");
const FS = require("fs-extra");
const UTILS = require("./utils.js");
const CONFIG = require("./config.json");

module.exports = { run }

async function run(submission) {
    console.info(`Starting pipeline for submission ${submission.submission_id}.`);
    const workdir = await FS.mkdtemp(`${CONFIG.work_directory}/`);
    const platform_config = search_config_for_platform(submission);

    try {
        console.info("Entering prepare stage.");

        if (await prepare(submission, workdir, platform_config)) {
            console.info("Entering example testing stage.");
            await submission.start_example_test_stage();
            await test_examples(submission, workdir, platform_config);
            console.info("Queueing student notification.");
            await queue_test_done_mail(submission);

            console.info("Entering final testing stage.");
            await submission.start_final_test_stage();
            await test_finals(submission, workdir, platform_config);
        }

        await submission.end_pipeline();
        console.info(`Pipeline for submission ${submission.submission_id} completed successfully.`);
    } finally {
        // We want to always clean up, even if the pipeline failed somewhere.
        await FS.remove(workdir)
            // Swallow up any errors during cleanup.
            .catch(console.error);
    }
}

/**
 * Runs the prepare stage.
 *
 * @param {submission:Submission} submission
 * The submission to prepare.
 *
 * @param {string} testing_dir
 * The directory in which to put the results of the prepare stage.
 *
 * @param {*} platform_config
 * The platform configuration for this submission.
 *
 * @returns {external:Promise}
 * A promise resolving to whether the prepare stage ran successfully..
 */
async function prepare(submission, testing_dir, platform_config) {
    const prepare_dir = await FS.mkdtemp(`${CONFIG.work_directory}/`);

    try {
        await FS.writeFile(`${prepare_dir}/submission`, await submission.fetch_code());

        const { exit_code, stdout, stderr } = await spawn_runner(
            platform_config.prepare_module, platform_config.prepare_max_memory,
            submission.max_compile_time, platform_config.working_directory_size_limit,
            prepare_dir, testing_dir
        );

        const prepare_success = exit_code === 0;
        submission.set_prepare_result(prepare_success, stdout, stderr);
        return prepare_success;
    } finally {
        // These is not much we can do with this error other than report it. After all,
        // there might already have been a previous error, which we don't want to
        // interfere with.
        await FS.remove(prepare_dir).catch(console.error);
    }
}

/**
 * Runs the example testing stage on a submission.
 *
 * @param {submission:Submission} submission
 * The submission to run the example testing stage for.
 *
 * @param {string} workdir
 * The directory the submission was prepared in.
 *
 * @param {*} platform_config
 * The platform configuration for this submission.
 *
 * @returns {external:Promise}
 * A promise resolving if this stage was ran successfully, rejecting otherwise.
 */
async function test_examples(submission, workdir, platform_config) {
    const example_tests = await submission.fetch_example_tests();

    for (test of example_tests)
        await run_test(submission, workdir, platform_config, test);
}

/**
 * Runs the final testing stage on a submission.
 *
 * @param {submission:Submission} submission
 * The submission to run the final testing stage for.
 *
 * @param {string} workdir
 * The directory the submission was prepared in.
 *
 * @param {*} platform_config
 * The platform configuration for this submission.
 *
 * @returns {external:Promise}
 * A promise resolving if this stage was ran successfully, rejecting otherwise.
 */
async function test_finals(submission, workdir, platform_config) {
    const final_tests = await submission.fetch_final_tests();

    for (test of final_tests)
        await run_test(submission, workdir, platform_config, test);
}

/**
 * Runs a test, and commits the results to the database.
 *
 * @param {submission:Submission} submission
 * The submission the test is for.
 *
 * @param {string} workdir
 * The directory the submission was prepared in.
 *
 * @param {*} platform_config
 * The configuration settings for the platform of the submission.
 *
 * @param {submission:Test} test
 * A handle to the test to run.
 *
 * @returns {external:Promise}
 * A promise that resolves if the test could be tested, rejecting otherwise.
 */
async function run_test(submission, workdir, platform_config, test) {
    console.info(`Starting test ${test.test_id}.`);

    const { input, output } = await test.fetch_input_output();

    const { exit_code, stdout, stderr } = await spawn_runner(
        platform_config.test_module, platform_config.test_max_memory, submission.max_run_time,
        platform_config.working_directory_size_limit, workdir, null, input
    );

    const test_was_success = compare_test_outputs(output, stdout) && exit_code === 0;
    await test.set_results(test_was_success, stdout, stderr);
    console.info(`Finished running test ${test.test_id}.`);
}

/**
 * Runs a runner.
 *
 * @param {string} image The name of the image of the runner.
 * @param {number} memory How much memory in bytes the runner is allowed.
 * @param {number} run_time For how long the runner is allowed to run, in miliseconds.
 * @param {number} max_size The maximum size of the working directory.
 * @param {string} mount_directory The directory to mount to /submission/ in the runner.
 * @param {string} output_directory A directory to put the contents of /submission/ after the runner has stopped.
 * @param {string} stdin What to send the runner over stdin. Defaults to an empty string.
 * @returns {external:Promise.<{exit_code, stdout, stderr}>}
 * A promise resolving when the container could be started, rejecting otherwise.
 */
async function spawn_runner(image, memory, run_time, max_size, mount_directory, output_directory = null, stdin = "") {
    console.info(`Starting runner for max ${run_time} milliseconds.`);

    const { stdout: container_id_untrimmed } = await exec(
        "docker",
        [
            "create",
            "--cap-drop=ALL",
            "--security-opt=no-new-privileges",
            "--cpu-quota", CONFIG.cpu_quota,
            "--memory", memory,
            "--network=none",
            "--memory-swap=0",
            "--interactive",
            "--storage-opt", `size=${max_size}`,
            image
        ]);

    const container_id = container_id_untrimmed.trim();
    await exec("docker", ["cp", mount_directory, `${container_id}:/submission`])
        .catch(({ stderr }) => console.error(stderr));

    const runner_promise = exec(
        "docker",
        ["start", "--attach", "--interactive", container_id],
        {},
        stdin
    );
    console.info(`Runner started: ${container_id}.`);

    const timeout = setTimeout(() => {
        console.info("Runner ran out of time. Stopping it.");
        exec("docker", ["stop", "--time=3", container_id], {})
            .catch(({ exit_code, stdout, stderr }) => {
                console.warn("Failed to kill runner. Dumping stdout and stderr.");
                console.warn(stdout);
                console.warn(stderr);
            })
    },
        run_time);

    runner_promise
        .then(() => clearTimeout(timeout), () => clearTimeout(timeout))
        .then(() => console.info(`Runner stopped: ${container_id}.`));

    const runner_results = await runner_promise.catch(error => error);
    if (output_directory !== null) {
        FS.remove(output_directory);
        await exec("docker", ["cp", `${container_id}:/submission`, output_directory])
            .catch(({ stderr }) => runner_results.stderr = `${runner_results.stderr}\n${stderr}`);
    }

    // Clean up the container.
    await exec("docker", ["container", "rm", container_id]);

    return runner_results;
}

/**
 * Runs a process, returning a promise to resolving when it's done.
 *
 * @param {string} program
 * The name of the program to run.
 *
 * @param {[string]} arguments
 * An array of arguments to pass to the program.
 *
 * @param {{*}} options
 * An option object to send to execFile.
 *
 * @returns {external:Promise.<{exit_code, stdout, stderr}>}
 * A promise resolving when the program stopped with an exit code of 0, rejecting otherwise.
 *
 * Either way, the promise will yield an exit_code, the stdout and the stderr of the process.
 */
function exec(program, arguments, options = {}, stdin = "") {
    const { execFile } = require('child_process');

    return new Promise((resolve, reject) => {
        const runner = execFile(program, arguments, options, (error, stdout, stderr) => {
            const exit_code = error ? error.code : 0;

            if (exit_code === 0)
                resolve({ exit_code, stdout, stderr });
            else
                reject({ exit_code, stdout, stderr })
        });
        runner.stdin.write(stdin);
        runner.stdin.destroy();
    })
}

/**
 * Tests whether a submission's output matches the expected output.
 *
 * Newlines at the end of either string are ignored when testing if they are equal.
 *
 * @param {string} reference_output
 * The expected output.
 *
 * @param {string} submission_output
 * The output generated by the submission.
 *
 * @returns {boolean} Whether the output matches the expected output.
 */
function compare_test_outputs(reference_output, submission_output) {
    return remove_trailing_newlines(reference_output) === remove_trailing_newlines(submission_output);
}

/**
 * Removes all trailing newlines of a string.
 *
 * @param {string} string
 * The string to remove trailing newlines from.
 *
 * @returns {string}
 * A copy of the supplied string, with all trailing newlines removed.
 */
function remove_trailing_newlines(string) {
    return string.replace(/[\n\r]+$/, "");
}

/**
 * Add a "test done" email to the queued emails. The recipient will be the owner of the submission.
 *
 * @param {Submission} submission The submission for which a notification email will be queued.
 */
async function queue_test_done_mail(submission) {
    const submission_id_query = DB.one(
        `SELECT submission.user_id AS user_id,
            assignment.name AS assignment_name
        FROM submission
            INNER JOIN assignment
            ON submission.assignment_id = assignment.id
        WHERE submission.id = $(submission_id)`,
        { submission_id: submission.submission_id }
    );
    const [total_example_tests, total_example_success, row] =
        await Promise.all([
            submission.fetch_example_test_count(),
            submission.fetch_successful_example_test_count(),
            submission_id_query
        ]);

    const extra_data = {
        assignment_name: row.assignment_name,
        total_example_tests: total_example_tests,
        total_example_success: total_example_success
    };
    return DB.none(
        `INSERT INTO email (user_id, type, extra_data)
        VALUES ($(user_id), 'test_done', $(extra_data))`,
        { user_id: row.user_id, extra_data }
    );
}

/**
 * Gets the config of a submission, and returns it.
 *
 * @param {Submission} submission The submission to find the config for.
 * @returns {*} The config of the platform.
 */
function search_config_for_platform(submission) {
    const matches = CONFIG.platforms.filter(({ id }) => id === submission.platform_id);
    console.assert(
        matches.length === 1,
        `Multiple platforms with id ${submission.platform_id} in config.`
    );
    return matches[0];
}

const PIPELINE = require("./pipeline.js");
const SUBMISSION_MODULE = require("./submission.js");
const UTILS = require("./utils.js");
const CONFIG = require("./config.json");
const SUPPORTED_PLATFORMS = CONFIG.platforms.map(({ id }) => id);

main()
    .then(() => console.log("STOPPED"))
    .catch(error => {
        console.info("CRASHED");
        console.error(error);
        process.exit(1);
    });

async function main() {
    let running = true;
    process.on('SIGINT', () => running = false);

    while (running) {
        const submission = await SUBMISSION_MODULE.retrieve_work(SUPPORTED_PLATFORMS);
        if (!submission)
            await UTILS.sleep(3000);
        else {
            await PIPELINE.run(submission)
                // We need to make sure the submission is unlocked!    
                .catch(error => {
                    submission.unlock();
                    return Promise.reject(error);
                });
            await submission.unlock();
        }
    }
}

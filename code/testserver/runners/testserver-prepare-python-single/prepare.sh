#!/bin/sh

# Propagate errors.
set -e

cd /submission/

mv submission main.py

if [ ! -f "main.py" ]
then
	echo "The submission doesn't contain a 'main.py'."
	exit 1
fi

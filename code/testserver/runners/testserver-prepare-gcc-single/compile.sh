#!/bin/sh

# If one of the steps fail, propagate the error by stopping the script.
# e.g. No point in doing `make test` if we could not extract the archive.
set -e

cd /submission/

mv submission main.c
gcc main.c -o test -Wall

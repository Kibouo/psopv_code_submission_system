#!/bin/sh

cd /submission/

./test <&0 1>&1 2>&2

return_code=$?
echo "Script stopped with return code $return_code." 1>&2
exit $return_code
#!/bin/sh

# Propagate errors.
set -e

cd /submission/

tar --no-same-owner -xvf submission

if [ ! -f "main.py" ]
then
	echo "The submission doesn't contain a 'main.py'."
	exit 1
fi

#!/bin/sh

cd /submission/

python3 ./main.py 0<&0 1>&1 2>&2

return_code=$?
echo "Script stopped with return code $return_code." 1>&2
exit $return_code
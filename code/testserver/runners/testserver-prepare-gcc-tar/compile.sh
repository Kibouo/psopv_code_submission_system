#!/bin/sh

# If one of the steps fail, propagate the error by stopping the script.
# e.g. No point in doing `make test` if we could not extract the archive.
set -e

cd /submission/

tar --no-same-owner -xvf submission
make test

if [ ! -f "test" ]
then
	echo "Running GNU Make did not create the file 'test'."
	exit 1
fi
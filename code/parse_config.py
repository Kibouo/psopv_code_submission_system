#! /usr/bin/env python3

import sys
import json


def parse_json(config_json, path):
    if type(config_json) is dict:
        for key in config_json.keys():
            parse_json(config_json[key], path + '/' + key)
    else:
        if config_json == "":
            # Docker complains about 0byte sized files if we put no data in a secret
            config_json = "."
        with open(path, 'w') as write_file:
            write_file.write(str(config_json))


def main():
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'r') as file:
            config_json = json.loads(file.read())
        parse_json(config_json, '.')
    else:
        print("Usage: parse_config.py JSON_FILE\n\
\n\
Script which generates Code Submission System's config files given a json")


main()

# Project Structure

Every module has its own directory in the root. Every module contains a Dockerfile.

```
cron/
database/
testserver/
web/
```

The root also contains various scripts to deploy the platform. See the manual for more details.

## Cron

The cron module contains various cron jobs.

## Database

The database module has a list of database scripts in the `scripts/` folder.

## Testserver

- `runners/`: The configuration of every runner.
- `server/`: The testserver itself, written in Javascript.
- `build-images.sh`: Script that builds or updates all runners on the system. Is ran automatically by the testserver on startup.
- `config.json`: Configuration options for the testserver.
- `package-lock.json`: List with versions of every NPM dependency.
- `package.json`: The Node package configuration. Includes information like dependencies.
- `start.sh`: The script that starts the testserver.

## Web

- `public/`: Contains images and javascript directly accessible by a web browser.
- `server/`: Contains Typescript and Javascript files that run the server.
  - `pages/`:  Contains page handlers for every page.
- `stylesheet/`: Contains LESS stylesheets.
- `views/`: Contains page templates that are compiled into HTML at runtime.
  - `includes/`: Contains reusable templates for different pages to use.
- `healthcheck.sh`: Script that is ran to determine whether the web server still functions, or crashed.
- `package-lock.json`: List with versions of every NPM dependency.
- `package.json`: The Node package configuration. Includes information like dependencies.
- `start.sh`: The script that starts the web server.
- `tsconfig.json`: Typescript compilation options.


# Test setup

For the setup we request you to follow the instructions in the Setup Guide. Following files are present and provide example data:

- `example_logins.txt`: A few test accounts.
- `example_config.json`: The example application settings. To be used as input file for `./deploy.sh` with the `-f` flag.
- `example_cert.pem`: An example, self-signed, SSL certification. To be used as input file for `./deploy.sh` with the `-c` flag.
- `example_key.pem`: The private key for the example SSL certificate. To be used as input file for `./deploy.sh` with the `-k` flag.
- `database/scripts/example.sql`: The database with example data. To use this database, select it as the active database using the `load_db_backup.sh` script. This is preferred to be done before the _Deployment_ step in the Setup Guide. **Watch out!** This database is only usable with the database credentials used in the file `example_config.json`.
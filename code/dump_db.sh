#!/bin/bash

if [[ $(/usr/bin/id -u) -ne 0 ]]
then
    echo "Not running as root"
    exit 1
fi

read -rp "Database name: " DB_NAME
read -rp "Database username: " DB_USER
read -rsp "Database password: " DB_PASS
echo

PGPASSWORD=${DB_PASS} pg_dump -h 127.0.0.1 -p 5432 -U $DB_USER $DB_NAME -o > database/scripts/DB_BACKUP_$(date -u -I).sql

if [ $? -eq 0 ]
then
    echo
    echo "Succesfully dumped the database."
fi

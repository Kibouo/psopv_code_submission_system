#!/bin/sh

DB_HOST=$PGHOST

DB_NAME=`cat /run/secrets/database_name`
DB_USER=`cat /run/secrets/database_username`
# TODO: epoch +1
# TODO: pipeline_status op wait zetten
# TODO: al eventuele tests verwijderen
# Clearn dirty submission data
echo "clean database - submissions"
PGPASSWORD=`cat /run/secrets/database_password` psql -h "database" -p 5432 -U $DB_USER $DB_NAME -c "UPDATE submission SET pipeline_epoch=pipeline_epoch+1, pipeline_status='wait' where pipeline_status<>'done' AND (age(LOCALTIMESTAMP, last_update) > '1 minute')"

# Clean us  ed or expired reset tokens
echo "clean database - reset tokens"
PGPASSWORD=`cat /run/secrets/database_password` psql -h "database" -p 5432 -U $DB_USER $DB_NAME -c "DELETE FROM password_reset_token WHERE expire_date < LOCALTIMESTAMP"

const FS = require("fs");

const DB_USERNAME = FS.readFileSync("/run/secrets/database_username");
const DB_PASSWORD = FS.readFileSync("/run/secrets/database_password");
const DB_NAME = FS.readFileSync("/run/secrets/database_name");
const DB_CONNECTION = require("pg-promise")()(`postgres://${DB_USERNAME}:${DB_PASSWORD}@database:5432/${DB_NAME}`);

/**
 * Check whether ssl key, cert, and password are available (does not check the actual content).
 *
 * @returns {boolean} whether cert and key are valid.
 */
function ssl_usable() {
    try {
        const cert = FS.readFileSync("/run/secrets/ssl_cert").toString();
        const key = FS.readFileSync("/run/secrets/ssl_key").toString();
        const password = FS.readFileSync("/run/secrets/ssl_password").toString();

        return cert !== "." && key !== ".";
    } catch (error) {
        console.log("Error: SSL private key, SSL certificate, and/or SSL password not found.");
        return false;
    }
}

const EMAIL_ACCOUNT = FS.readFileSync("/run/secrets/email_account");
const TRANSPORTER = require("nodemailer").createTransport({
    host: FS.readFileSync("/run/secrets/email_smtp_url").toString(),
    port: new Number(FS.readFileSync("/run/secrets/email_port").toString()),
    secure: (FS.readFileSync("/run/secrets/email_use_ssl").toString() === '1'),
    auth: {
        user: EMAIL_ACCOUNT,
        pass: FS.readFileSync("/run/secrets/email_password"),
    }
});

/**
 * Get all queued emails which have not been sent yet.
 *
 * @returns {external:Promise<[Email]>} A promise with all queued emails to be sent.
 */
function get_queued_mails() {
    return DB_CONNECTION.manyOrNone(
        `SELECT email.id AS id,
            user_account.email AS account,
            email.type AS type,
            email.extra_data AS extra_data
        FROM email
        INNER JOIN user_account
            ON user_account.id = email.user_id
        LIMIT 20`
    );
}

/**
 * Delete the queued mail with the given id.
 *
 * @param {Number} id The id of the email to be deleted from the queue.
 */
function pop_mail(id) {
    return DB_CONNECTION.none(
        `DELETE FROM email
        WHERE id = $(id)`,
        { id }
    );
}

/**
 * Returns the host name which has been set during setup.
 */
function host_name() {
    return FS.readFileSync("/home/send_mails/domain_name").toString();
}

/**
 * Determine the type of mail to be sent, and perform the sending.
 *
 * @param {Object} mail_object An object with all data of an email to be sent.
 *
 * @returns {Number} The id of the mail if the sending succeeded, or -1 if it failed.
 */
const EMAIL_RENDERER = require('pug');
async function send_mail(mail_object) {
    const subject_prefix = "Code:Sub - ";
    const url = ssl_usable() ? "https://" + host_name() : "http://" + host_name();
    let email_subject;
    let message_html;
    switch (mail_object.type) {
        case "welcome":
            email_subject = "Welcome!";
            message_html = await EMAIL_RENDERER.renderFile("emails/welcome.pug",
                {
                    username: mail_object.extra_data.username,
                    reset_request_link: url + mail_object.extra_data.reset_request_link
                });
            break;
        case "reset_token":
            email_subject = "Password Reset";
            message_html = await EMAIL_RENDERER.renderFile("emails/reset_token.pug",
                {
                    reset_link: url + mail_object.extra_data.reset_link + "/" + mail_object.extra_data.reset_token,
                    expire_time: mail_object.extra_data.expire_time,
                    reset_request_link: url + mail_object.extra_data.reset_request_link,
                    reset_token: mail_object.extra_data.reset_token
                });
            break;
        case "password_reset_done":
            email_subject = "Password Reset Done";
            message_html = await EMAIL_RENDERER.renderFile("emails/password_reset_done.pug", {});
            break;
        case "submission_receive":
            email_subject = "Submission Received";
            message_html = await EMAIL_RENDERER.renderFile("emails/submission_receive.pug",
                {
                    assignment_name: mail_object.extra_data.assignment_name,
                    submission_link: url + mail_object.extra_data.submission_link
                });
            break;
        case "test_done":
            email_subject = "Tests Done";
            message_html = await EMAIL_RENDERER.renderFile("emails/test_done.pug",
                {
                    assignment_name: mail_object.extra_data.assignment_name,
                    total_example_tests: mail_object.extra_data.total_example_tests,
                    total_example_success: mail_object.extra_data.total_example_success
                });
            break;
        default:
            email_subject = null;
            message_html = null;
            console.log("Invalid email type " + mail_object.type);
    }

    if (message_html && email_subject) {
        const options = {
            from: EMAIL_ACCOUNT,
            to: mail_object.account,
            subject: subject_prefix + email_subject,
            text: "",
            html: message_html
        };

        const success = await TRANSPORTER
            .sendMail(options)
            .then(email_info => {
                console.log(mail_object.type + " email sent to " + mail_object.account);
                return true;
            }, error => {
                console.log("email error! Failed to send to : " + mail_object.account);
                console.log(error);
                return false;
            });

        return success ? mail_object.id : -1;
    }

    // Something was wrong with the email's data
    return -1;
}

async function main() {
    get_queued_mails().then(async mails => {
        console.log("Got " + mails.length + " mails queued to send.");
        const sent_mail_ids = await Promise.all(mails.map(mail => { return send_mail(mail); }));

        for (let i = 0; i < sent_mail_ids.length; i += 1)
            if (sent_mail_ids[i] !== -1)
                pop_mail(sent_mail_ids[i]);
    });
}

main();
#!/bin/bash

RED="\033[0;31m"
YELLOW="\033[1;33m"
NO_COLOR="\033[0m"

function print_help {
    echo "Usage: build.sh COMMAND

Build script for the Code Submission System project. The information provided has to match the Docker swarm manager's information

Options:
  -c    Path to SSL certificate
  -f    Path to JSON file with all information needed for deployment
  -h    Print this message
  -k    Path to SSL private key"
}

function parse_json {
    ./parse_config.py $1
    if [ $? -gt 0 ]
    then
        exit 1
    fi
}

function build {
    COMPOSE_PROJECT_NAME=codesubsys
    chmod 744 ./database/scripts/*

    docker build -t ${COMPOSE_PROJECT_NAME}-cron cron/
    docker build -t ${COMPOSE_PROJECT_NAME}-database database/
    docker build -t ${COMPOSE_PROJECT_NAME}-testserver testserver/
    docker build -t ${COMPOSE_PROJECT_NAME}-web web/
}

function clean_sensitive_data {
    # cleans generated sensitive data
    rm -rf ./secrets/
    rm ./cron/send_mails/domain_name

    echo -e "[${YELLOW}WARNING${NO_COLOR}]: The files used for this setup contain sensitive data. Be sure to store them safely!"
}

function check_args {
    FLAGS_PASSED=0
    K_FLAG_USED=0
    C_FLAG_USED=0

    # parse flags passed to program
    while getopts ":hf:c:k:" FLAG; do
        FLAGS_PASSED=1
        case $FLAG in
            f)
                if [ -f ${OPTARG} ]
                then
                    mkdir ./secrets
                    parse_json ${OPTARG}
                else
                    echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -f does not exist!"
                    exit 1
                fi
                ;;
            h)
                print_help
                exit 0
                ;;
            c)
                if [ -f ${OPTARG} ]
                then
                    C_FLAG_USED=1
                    cp ${OPTARG} ./secrets/cert.pem
                else
                    echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -c does not exist!"
                    exit 1
                fi
                ;;
            k)
                if [ -f ${OPTARG} ]
                then
                    K_FLAG_USED=1
                    cp ${OPTARG} ./secrets/key.pem
                else
                    echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -k does not exist!"
                    exit 1
                fi
                ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    # no flags passed
    if [ ${FLAGS_PASSED} -eq 0 ]
    then
        print_help
        exit 1
    fi

    # if user didn't supply key/cert
    # make dummy files to prevent Docker of complaining about no secrets available
    # fill it with "." to prevent Docker of complaining about a 0byte file
    if [ ${K_FLAG_USED} -eq 0 ]
    then
        touch ./secrets/key.pem
        echo -n "." > ./secrets/key.pem
    fi
    if [ ${C_FLAG_USED} -eq 0 ]
    then
        touch ./secrets/cert.pem
        echo -n "." > ./secrets/cert.pem
    fi

    build
    clean_sensitive_data

    exit 0
}

if [[ $(/usr/bin/id -u) -ne 0 ]]
then
    echo "Not running as root"
    exit 1
fi
check_args ${@}

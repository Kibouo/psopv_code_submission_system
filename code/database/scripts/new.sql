CREATE TABLE user_account (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    name_normalized TEXT NOT NULL,
    password_hash CHARACTER(192) NOT NULL DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
    email TEXT NOT NULL UNIQUE,
    is_super_user BOOLEAN NOT NULL DEFAULT false
);
/*
--
*/
CREATE TABLE password_reset_token (
    user_id INTEGER NOT NULL REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE UNIQUE,
    hash CHARACTER(192) NOT NULL,
    expire_date TIMESTAMP NOT NULL DEFAULT localtimestamp,
    new_password_hash CHARACTER(192) NOT NULL,
    PRIMARY KEY (user_id)
);
/*
--
*/
CREATE TABLE user_session (
    sid TEXT NOT NULL PRIMARY KEY,
    sess JSON NOT NULL,
    expire TIMESTAMP(6) NOT NULL DEFAULT localtimestamp
);
/*
--
*/
CREATE TABLE course (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    name_normalized TEXT NOT NULL
);
/*
--
*/
CREATE TABLE platform (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    description TEXT NOT NULL
);
/*
--
*/
CREATE TABLE assignment (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    name_normalized TEXT NOT NULL,
    description TEXT,
    deadline TIMESTAMP NOT NULL,
    compile_time_max INTERVAL NOT NULL,
    run_time_max INTERVAL NOT NULL,
    course_id INTEGER NOT NULL REFERENCES course (id) ON UPDATE CASCADE ON DELETE CASCADE,
    tests_platforms_inserted BOOLEAN NOT NULL DEFAULT false
);
/*
--
*/
CREATE TABLE assignment_platform_relation (
    assignment_id INTEGER REFERENCES assignment(id) ON UPDATE CASCADE ON DELETE CASCADE,
    platform_id INTEGER REFERENCES platform(id) ON UPDATE CASCADE ON DELETE CASCADE,
    PRIMARY KEY(assignment_id, platform_id)
);
/*
--
*/
CREATE TYPE user_type AS ENUM('teacher', 'student');
/*
--
*/
CREATE TABLE course_user_relation (
    user_id INTEGER REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE,
    course_id INTEGER REFERENCES course(id) ON UPDATE CASCADE ON DELETE CASCADE,
    user_type user_type NOT NULL,
    PRIMARY KEY (user_id, course_id)
);
/*
--
*/
CREATE TYPE pipeline_status AS ENUM(
    'wait',
    'prepare',
    'example',
    'final',
    'done'
);
/*
--
*/
CREATE TABLE submission (
    id SERIAL PRIMARY KEY,
    source_code BYTEA NOT NULL,
    mime_type TEXT NOT NULL,
    submitted_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    platform_id INTEGER NOT NULL REFERENCES platform (id) ON UPDATE CASCADE ON DELETE CASCADE,
    user_id INTEGER NOT NULL REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE,
    assignment_id INTEGER NOT NULL REFERENCES assignment(id) ON UPDATE CASCADE ON DELETE CASCADE,
    prepare_stdout TEXT,
    prepare_stderr TEXT,
    is_prepare_success BOOLEAN NOT NULL DEFAULT false,
    pipeline_status pipeline_status NOT NULL DEFAULT 'wait',
    last_update TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    pipeline_epoch INTEGER NOT NULL DEFAULT 0
);
/*
--
*/
CREATE TYPE test_type AS ENUM('example', 'final');
/*
--
*/
CREATE TABLE test (
    id SERIAL PRIMARY KEY,
    type test_type NOT NULL,
    input TEXT NOT NULL,
    output TEXT NOT NULL,
    assignment_id INTEGER NOT NULL REFERENCES assignment(id) ON UPDATE CASCADE ON DELETE CASCADE
);
/*
--
*/
CREATE TABLE test_result (
    test_id INTEGER NOT NULL REFERENCES test(id) ON UPDATE CASCADE ON DELETE CASCADE,
    submission_id INTEGER NOT NULL REFERENCES submission(id) ON UPDATE CASCADE ON DELETE CASCADE,
    is_success BOOLEAN NOT NULL,
    stdout TEXT NOT NULL,
    stderr TEXT NOT NULL,
    pipeline_epoch INTEGER NOT NULL,
    PRIMARY KEY (test_id, submission_id)
);
/*
--
*/
CREATE TYPE email_type AS ENUM (
    'welcome',
    'reset_token',
    'password_reset_done',
    'submission_receive',
    'test_done'
);
/*
--
*/
CREATE TABLE email (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES user_account(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type email_type NOT NULL,
    extra_data JSON NOT NULL DEFAULT '{}'
);
/*
--
*/
INSERT INTO
    user_account (
        name,
        name_normalized,
        password_hash,
        email,
        is_super_user
    )
VALUES
    (
        'admin',
        'admin',
        '736372797074000f00000008000000017fc2f755c7441720b86bc2d09fe3759ea0ee33a6172cc7c31c85e7fb6520a32445b90f38d04925ce14c34a2a35a79a98ccf37305ca605b246fec15f858ac9008dc50ed828152aa9a217e3e06f19b11c1',
        'admin@admin',
        true
    );
/*
--
*/
INSERT INTO
    platform (id, name, description)
VALUES
    (1, 'Python 3 on Ubuntu 16.04 (.tar)', 'For this platform, a .tar file is expected. It should contain a main.py.
python3 will be run on this file.

Input is provided through standard input.

After extraction of the .tar file, the shell command to run your project is as follows:

```bash
#run main.py with input from standard input
python3 ./main.py 0<&0
```');

INSERT INTO
    platform (id, name, description)
VALUES
    (2, 'Python 3 on Ubuntu 16.04 (single file)', 'For this platform, a single .py file is expected. Your .py file will automatically be renamed to main.py.

Input is provided through standard input.

The shell command to run your submission is as follows:

```bash
#run main.py with input from standard input
python3 ./main.py 0<&0
```');

INSERT INTO
    platform (id, name, description)
VALUES
    (3, 'GCC on Ubuntu 16.04 (.tar)', 'For this platform, a __.tar__ file is expected. It should contain a __Makefile__ which compiles your project into a binary called __test__.

Input is provided through standard input.

After extraction of the __.tar__ file, the shell command to compile and run your project is as follows:
```sh
#compile project
make test

#run binary with input from standard input
./test <&0
```');

INSERT INTO
    platform (id, name, description)
VALUES
   (4, 'Rust with Cargo', 'For this platform, a .tar file is expected. It should contain a Cargo project.

Input is provided through standard input.

After extraction of the .tar file, the shell command to compile and run your project is as follows:

```bash
#compile project
cargo build

#run binary with input from standard input
./test <&0
```');

INSERT INTO
    platform (id, name, description)
VALUES
    (5, 'GCC on Ubuntu 16.04 (single file)', 'For this platform, a main.c file is expected. This will be compiled into a binary called __test__.

Input is provided through standard input.

The shell command to compile and run your submission is as follows:
```sh
#compile submission
gcc main.c -o test -Wall

#run binary with input from standard input
./test <&0
```');

INSERT INTO
		platform (id, name, description)
VALUES
		(6, 'G++ on Ubuntu 16.04 (single file)', 'For this platform, a main.cpp file is expected. This will be compiled into a binary called __test__.

Input is provided through standard input.

The shell command to compile and run your submission is as follows:
```sh
#compile submission
g++ main.cpp -o test -Wall -std=c++17

#run binary with input from standard input
./test <&0
```');
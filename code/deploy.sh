#!/bin/bash

RED="\033[0;31m"
YELLOW="\033[1;33m"
NO_COLOR="\033[0m"

function print_help {
    echo "Usage: deploy.sh COMMAND

Deploy script for the Code Submission System project

Options:
  -c    Path to SSL certificate
  -f    Path to JSON file with all information needed for deployment. Cannot be used with -i
  -h    Print this message
  -i    Provide deployment information manually. Cannot be used with -f
  -k    Path to SSL private key"
}

function parse_json {
    ./parse_config.py $1
    if [ $? -gt 0 ]
    then
        exit 1
    fi
}

function deploy {
    COMPOSE_PROJECT_NAME=codesubsys
    TIME_LIMIT=60

    chmod 744 ./database/scripts/*
    docker stack rm $COMPOSE_PROJECT_NAME

    # wait for `docker stack rm` to finish completely. This is not yet implemented in Docker itself
    # https://github.com/docker/cli/issues/373
    echo ""
    while [ -n "$(docker container ls --filter label=com.docker.stack.namespace=$COMPOSE_PROJECT_NAME -q)" ] && [ "$TIME_LIMIT" -ge 0 ]; do
        echo -ne "Waiting for \`docker stack rm\` to complete... ${TIME_LIMIT} seconds left.\r"
        TIME_LIMIT=$(($TIME_LIMIT-1))
        sleep 1;
    done
    while [ -n "$(docker network ls --filter label=com.docker.stack.namespace=$COMPOSE_PROJECT_NAME -q)" ] && [ "$TIME_LIMIT" -ge 0 ]; do
        echo -ne "Waiting for \`docker stack rm\` to complete... ${TIME_LIMIT} seconds left.\r"
        TIME_LIMIT=$(($TIME_LIMIT-1))
        sleep 1;
    done
    echo ""

    if [ $TIME_LIMIT -le 0 ]
    then
        echo -e "[${RED}ERROR${NO_COLOR}]: The command \`docker stack rm\` took too long to complete.

        Try restarting the Docker service if this problem persists."
        exit 1
    fi

    docker build -t ${COMPOSE_PROJECT_NAME}-cron cron/
    docker build -t ${COMPOSE_PROJECT_NAME}-database database/
    docker build -t ${COMPOSE_PROJECT_NAME}-testserver testserver/
    docker build -t ${COMPOSE_PROJECT_NAME}-web web/
    docker stack deploy --compose-file=docker-compose.yml $COMPOSE_PROJECT_NAME
}

function clean_sensitive_data {
    # cleans generated sensitive data
    rm -rf ./secrets/
    rm ./cron/send_mails/domain_name

    echo -e "[${YELLOW}WARNING${NO_COLOR}]: The files used for this setup contain sensitive data. Be sure to store them safely!"
}

function interactive_setup {
    ./generate_config.py
}

function check_args {
    FLAGS_PASSED=0
    F_FLAG_USED=0
    I_FLAG_USED=0
    K_FLAG_USED=0
    C_FLAG_USED=0

    # parse flags passed to program
    while getopts ":hf:c:k:i" FLAG; do
        FLAGS_PASSED=1
        case $FLAG in
            f)
                F_FLAG_USED=1
                if [ ${I_FLAG_USED} -eq 1 ]
                then
                    echo "-i flag has been used already. Ignoring -f"
                else
                    if [ -f ${OPTARG} ]
                    then
                        mkdir ./secrets
                        parse_json ${OPTARG}
                    else
                        echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -f does not exist!"
                        exit 1
                    fi
                fi
                ;;
            h)
                print_help
                exit 0
                ;;
            c)
                if [ -f ${OPTARG} ]
                then
                    C_FLAG_USED=1
                    cp ${OPTARG} ./secrets/cert.pem
                else
                    echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -c does not exist!"
                    exit 1
                fi
                ;;
            k)
                if [ -f ${OPTARG} ]
                then
                    K_FLAG_USED=1
                    cp ${OPTARG} ./secrets/key.pem
                else
                    echo -e "[${RED}ERROR${NO_COLOR}]: File passed to -k does not exist!"
                    exit 1
                fi
                ;;
            i)
                I_FLAG_USED=1
                if [ ${F_FLAG_USED} -eq 1 ]
                then
                    echo "-f flag has been used already. Ignoring -i"
                else
                    interactive_setup
                    mkdir ./secrets
                    parse_json ./interactive_config.json
                fi
                ;;
            \?)
                echo "Invalid option: -$OPTARG" >&2
                exit 1
                ;;
            :)
                echo "Option -$OPTARG requires an argument." >&2
                exit 1
                ;;
        esac
    done

    # no flags passed
    if [ ${FLAGS_PASSED} -eq 0 ]
    then
        print_help
        exit 1
    fi

    # if user didn't supply key/cert
    # make dummy files to prevent Docker of complaining about no secrets available
    # fill it with "." to prevent Docker of complaining about a 0byte file
    if [ ${K_FLAG_USED} -eq 0 ]
    then
        touch ./secrets/key.pem
        echo -n "." > ./secrets/key.pem
    fi
    if [ ${C_FLAG_USED} -eq 0 ]
    then
        touch ./secrets/cert.pem
        echo -n "." > ./secrets/cert.pem
    fi

    deploy
    clean_sensitive_data

    if [ ${I_FLAG_USED} -eq 1 ]
    then
        echo -e "[${YELLOW}WARNING${NO_COLOR}]: The file \`interactive_config.json\` has been created with your settings. You can use this for easier re-deployment."
    fi

    echo "Wait for application to start-up..."
    sleep 60

    exit 0
}

if [[ $(/usr/bin/id -u) -ne 0 ]]
then
    echo "Not running as root"
    exit 1
fi
check_args ${@}
#!/bin/bash

RED="\033[0;31m"
YELLOW="\033[1;33m"
NO_COLOR="\033[0m"
COMPOSE_PROJECT_NAME=codesubsys
TIME_LIMIT=60

if [[ $(/usr/bin/id -u) -ne 0 ]]
then
    echo "Not running as root"
    exit 1
fi

docker stack rm ${COMPOSE_PROJECT_NAME}
# wait for `docker stack rm` to finish completely. This is not yet implemented in Docker itself
# https://github.com/docker/cli/issues/373
echo ""
while [ -n "$(docker container ls --filter label=com.docker.stack.namespace=$COMPOSE_PROJECT_NAME -q)" ] && [ "$TIME_LIMIT" -ge 0 ]; do
    echo -ne "Waiting for \`docker stack rm\` to complete... ${TIME_LIMIT} seconds left.\r"
    TIME_LIMIT=$(($TIME_LIMIT-1))
    sleep 1;
done
while [ -n "$(docker network ls --filter label=com.docker.stack.namespace=$COMPOSE_PROJECT_NAME -q)" ] && [ "$TIME_LIMIT" -ge 0 ]; do
    echo -ne "Waiting for \`docker stack rm\` to complete... ${TIME_LIMIT} seconds left.\r"
    TIME_LIMIT=$(($TIME_LIMIT-1))
    sleep 1;
done
echo ""

if [ $TIME_LIMIT -le 0 ]
then
    echo -e "[${RED}ERROR${NO_COLOR}]: The command \`docker stack rm\` took too long to complete.

    Try restarting the Docker service if this problem persists."
    exit 1
fi

VOLUME_NAME=${COMPOSE_PROJECT_NAME}_database-volume

containers_to_kill=`docker volume rm ${VOLUME_NAME} 2>&1 | grep -o '\[.*\]' | grep -o '[a-z0-9]*'`

for container in $containers_to_kill
do
    echo "DELETING CONTAINER: $container"
    docker container rm -f $container
done

docker volume rm -f ${VOLUME_NAME}
sudo docker image rm -f ${COMPOSE_PROJECT_NAME}-database

echo ""
echo "Select the mode to load the db in"

index=0
for file in $(ls database/scripts | grep .sql);
do
    echo -n ["${index}"]" "
    echo "${file}" | grep '[a-zA-Z_0-9\-]*\.' -o
    ((index++))
done

echo ""
read -p "-: " answer

index=0
for file in $(ls database/scripts | grep .sql);
do
    if [ ${index} -eq ${answer} ];
    then
        cp database/scripts/${file} database/scripts/current.sql
        echo -n "The mode is now "
        echo "${file}" | grep '[a-zA-Z_0-9\-]*\.' -o
        exit 0
    fi
    ((index++))
done
echo "Error: invalid input"
exit 1

else exit 1
fi

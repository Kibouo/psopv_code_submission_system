#! /usr/bin/env python3

import sys
import json
import getpass
import uuid


def ask_password():
    pass_one = getpass.getpass("password: ")
    pass_two = getpass.getpass("confirm password: ")

    while pass_one != pass_two:
        print("Password do not match.")
        pass_one = getpass.getpass("password: ")
        pass_two = getpass.getpass("confirm password: ")

    return pass_one


def get_non_opt_int(text):
    answer_correct = False

    while not answer_correct:
        answer = non_optional_input(text)
        try:
            answer = int(answer)
            answer_correct = True
        except ValueError:
            print("Not a valid number.")

    return answer


def non_optional_input(text):
    answer = input(text)

    while len(answer) == 0:
        print("This field is not optional.")
        answer = input(text)

    return answer


def yes_no_input(text):
    answer = input(text)

    if len(answer) == 0:
        return 0

    possible_confirms = ['y', 'yes', 'Y', 'Yes', 'YES', '1']
    possible_denies = ['n', 'no', 'N', 'No', 'NO', '0']
    while answer not in possible_confirms and answer not in possible_denies:
        print("Not a valid option.")
        answer = input(text)

    if answer in possible_confirms:
        return 1
    if answer in possible_denies:
        return 0


def private_key_input(text):
    answer = input(text)

    if len(answer) == 0:
        return str(uuid.uuid4())
    else:
        return answer


def interactive():
    accepted = False

    while not accepted:
        dict = {}

        print("This is the interactive setup for the Code Submission System project.")
        print("[optional] fields can be skipped by pressing Enter.")
        print("")

        # cron settings
        dict['cron'] = {}
        dict['cron']['send_mails'] = {}
        dict['cron']['send_mails']['domain_name'] = non_optional_input(
            "Domain name: ")
        print("")

        # secrets
        dict['secrets'] = {}
        print("Database settings - ")
        dict['secrets']['database_name'] = non_optional_input("name: ")
        dict['secrets']['database_username'] = non_optional_input("username: ")
        dict['secrets']['database_password'] = ask_password()
        print("")

        print("Email settings - ")
        dict['secrets']['email_account'] = non_optional_input("address: ")
        dict['secrets']['email_password'] = ask_password()
        dict['secrets']['email_smtp_url'] = non_optional_input("SMTP URL: ")
        dict['secrets']['email_port'] = get_non_opt_int("SMTP port: ")
        dict['secrets']['email_use_ssl'] = int(yes_no_input("use SSL [y/N]: "))
        print("")

        print("Extra settings - ")
        dict['secrets']['cookie_secret'] = private_key_input(
            "cookie private key [optional]: ")
        dict['secrets']['ssl_password'] = input("SSL password [optional]: ")
        print("")

        if 1 == yes_no_input("Scroll back up and check your input. Everything correct? [y/N] "):
            accepted = True
        else:
            print("--------------")
            print("")

    return dict


def main():
    if len(sys.argv) == 1:
        config_json = json.dumps(interactive())

        with open('interactive_config.json', 'w') as write_file:
            write_file.write(str(config_json))
    else:
        print("Usage: generate_config.py\n\
\n\
Script which generates a JSON config file used by parse_config, interactively.")


main()
